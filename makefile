# C compiler
CC=gcc
# Compiler flags
CFLAGS=-Wall -Werror -std=c99 -O3 -c
# Linker flags
LFLAGS=-Wall

# Directory containing source files
SDIR=src
# Directory containing object files
ODIR=obj
# Directory containing test files
TDIR=test

# Files to compile
OBJECTS_MAIN=radix.o testutils.o divisor.o
OBJECTS_STRUC=struc/list.o struc/stack.o
OBJECTS_MATH=math/number.o math/numberutils.o math/numberops.o
OBJECTS_MATH_MOD=math/modularops.o math/fft.o math/ssa.o
OBJECTS_IO=io/stringutils.o io/input.o io/output.o

OBJECTS=$(patsubst %,$(ODIR)/%,$(OBJECTS_STRUC) $(OBJECTS_MATH) $(OBJECTS_MATH_MOD) $(OBJECTS_IO) $(OBJECTS_MAIN))

# Compiled filename
OUTFILE=./radix


.PHONY: all clean cleanall test


### all ########################################################################################################################

all: $(OBJECTS)
	@$(CC) $(LFLAGS) -o $(OUTFILE) $(OBJECTS)


### clean ######################################################################################################################

clean:
	@rm -f $(OBJECTS)

cleanall: clean
	@rm -f $(OUTFILE)
	@rm -fR $(ODIR)


### test #######################################################################################################################

test: TESTER=$(TDIR)/tester
test: all
	-@$(TESTER) $(OUTFILE) 0
	-@$(TESTER) $(OUTFILE) 1
	-@$(TESTER) $(OUTFILE) 4
	-@$(TESTER) $(OUTFILE) 5
	-@$(TESTER) $(OUTFILE) 6
	-@$(TESTER) $(OUTFILE) 15
	-@$(TESTER) $(OUTFILE) 16
	-@$(TESTER) $(OUTFILE) 17
	-@$(TESTER) $(OUTFILE) 20
	-@$(TESTER) $(OUTFILE) 21
	-@$(TESTER) $(OUTFILE) 24
	-@$(TESTER) $(OUTFILE) 31
	-@$(TESTER) $(OUTFILE) 32
	-@$(TESTER) $(OUTFILE) 33
	-@$(TESTER) $(OUTFILE) 47
	-@$(TESTER) $(OUTFILE) 48
	-@$(TESTER) $(OUTFILE) 49
	-@$(TESTER) $(OUTFILE) 63
	-@$(TESTER) $(OUTFILE) 64
	-@$(TESTER) $(OUTFILE) 65
	-@$(TESTER) $(OUTFILE) 79
	-@$(TESTER) $(OUTFILE) 80
	-@$(TESTER) $(OUTFILE) 81
	-@$(TESTER) $(OUTFILE) 95
	-@$(TESTER) $(OUTFILE) 96
	-@$(TESTER) $(OUTFILE) 97
	-@$(TESTER) $(OUTFILE) 111
	-@$(TESTER) $(OUTFILE) 112
	-@$(TESTER) $(OUTFILE) 113
	-@$(TESTER) $(OUTFILE) 127
	-@$(TESTER) $(OUTFILE) 128
	-@$(TESTER) $(OUTFILE) 129
	-@$(TESTER) $(OUTFILE) 143
	-@$(TESTER) $(OUTFILE) 144
	-@$(TESTER) $(OUTFILE) 145
	-@$(TESTER) $(OUTFILE) 159
	-@$(TESTER) $(OUTFILE) 160
	-@$(TESTER) $(OUTFILE) 161
	-@$(TESTER) $(OUTFILE) 175
	-@$(TESTER) $(OUTFILE) 176
	-@$(TESTER) $(OUTFILE) 177
	-@$(TESTER) $(OUTFILE) 191
	-@$(TESTER) $(OUTFILE) 192
	-@$(TESTER) $(OUTFILE) 10_000
	-@$(TESTER) $(OUTFILE) 100_000
	-@$(TESTER) $(OUTFILE) 1_000_000
	-@$(TESTER) $(OUTFILE) 10_000_000
	-@$(TESTER) $(OUTFILE) 100_000_000
#	-@$(TESTER) $(OUTFILE) 1_000_000_000


### files ######################################################################################################################


# the general rule

$(ODIR)/%.o: $(SDIR)/%.c $(SDIR)/logger.h
	@[ -d $(dir $@) ] || mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $< -o $@


# specific prerequisites extending general building rule

$(ODIR)/radix.o: $(SDIR)/math/ssa.h $(SDIR)/math/number.h $(SDIR)/math/numberops.h $(SDIR)/math/numberutils.h $(SDIR)/io/input.h $(SDIR)/io/output.h $(SDIR)/divisor.h $(SDIR)/struc/list.h $(SDIR)/struc/stack.h
$(ODIR)/testutils.o: $(SDIR)/testutils.h $(SDIR)/math/number.h $(SDIR)/math/numberops.h
$(ODIR)/divisor.o: $(SDIR)/divisor.h $(SDIR)/math/number.h

### struc ###
$(ODIR)/struc/list.o: $(SDIR)/struc/list.h
$(ODIR)/struc/stack.o: $(SDIR)/struc/stack.h

### math ###
$(ODIR)/math/number.o: $(SDIR)/math/number.h
$(ODIR)/math/numberutils.o: $(SDIR)/math/numberutils.h $(SDIR)/math/number.h
$(ODIR)/math/numberops.o: $(SDIR)/math/numberops.h $(SDIR)/math/number.h $(SDIR)/math/numberutils.h $(SDIR)/math/ssa.h

### modular math ###
$(ODIR)/math/modularops.o: $(SDIR)/math/modularops.h $(SDIR)/math/numberops.h $(SDIR)/math/numberutils.h
$(ODIR)/math/fft.o: $(SDIR)/math/fft.h $(SDIR)/math/modularops.h
$(ODIR)/math/ssa.o: $(SDIR)/math/ssa.h $(SDIR)/math/fft.h $(SDIR)/math/numberops.h $(SDIR)/math/numberutils.h $(SDIR)/struc/list.h

### io ###
$(ODIR)/io/input.o: $(SDIR)/io/input.h $(SDIR)/math/number.h $(SDIR)/struc/stack.h
$(ODIR)/io/output.o: $(SDIR)/io/output.h $(SDIR)/math/number.h $(SDIR)/math/numberutils.h $(SDIR)/io/stringutils.h
$(ODIR)/io/stringutils.o: $(SDIR)/io/stringutils.h $(SDIR)/io/input.h $(SDIR)/io/output.h $(SDIR)/math/number.h
