#ifndef TESTUTILS_H_
#define TESTUTILS_H_

#include "math/number.h"

#define sizeofdigits(I, C) sizeofmem(I, (C)*sizeof(digit))
#define sizeofchars(I, C) sizeofmem(I, (C)*sizeof(char))

char* sizeofmem(short i, index bytes);

/* helpers */
void digits2file(char*, digit*, index, index);
digit* file2digits(char*, index*, index*);

#endif /* TESTUTILS_H_ */
