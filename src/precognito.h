#ifndef PRECOGNITO_H_
#define PRECOGNITO_H_

#include <unistd.h>
#include "math/number.h"

index precognito_digits(digit radix, digit number);

#endif /* PRECOGNITO_H_ */
