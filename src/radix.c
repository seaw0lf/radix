/****************************************************************************
 Name        : curta
 Author      : seawolf
 Version     :
 Copyright   : GPL
 Description : Radix conversion / Free time C project

 Links       : https://youtu.be/ZDn_DDsBWws
               https://youtu.be/loI1Kwed8Pk

 Quotes      : The purpose of computing is insight, not numbers! - R. W. Hamming
               Go down deep enough into anything and you will find mathematics. - Dean Schlicter
               It is not enough to have a good mind. The main thing is to use it well. - Rene Descartes
               If there's no struggle, there's no progress. - Frederick Douglass
               Problems cannot be solved at the same level of awareness that created them. - Albert Einstein
               Technical skill is mastery of complexity. Creativity is mastery of simplicity. - E. C. Zeeman
               It isn't that they cannot see the solution. It is that they cannot see the problem. - G. K. Chesterton
               Nothing is impossible, the word itself says "I'm possible"! - Audrey Hepburn
               A good programmer is someone who always looks both ways before crossing a one-way street. - Doug Linder
               Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live. - Martin Golding
               If debugging is the process of removing software bugs, then programming must be the process of putting them in. - Edsger Dijkstra

 Music       : http://youtu.be/1nOVc-iCyM8
               http://youtu.be/CDM6v1XhWEg
               http://youtu.be/7gphiFVVtUI
               Cara Dillon - P Stands for Paddy
               http://youtu.be/U0Yr08UaO5A
               http://youtu.be/KWGQWu6mSWM
               http://youtu.be/4dkig4HZpRA
               http://youtu.be/V3veR_23Q9w
               http://youtu.be/i8OY9FsK3L0
               http://youtu.be/QkvcM0JadEY
               http://youtu.be/xUvdN8XwAkE
               http://youtu.be/Sv3xVOs7_No
               http://youtu.be/kZN2lQC8g5s
 ****************************************************************************/

#include <inttypes.h>
#include <stdlib.h>
#include "logger.h"
#include "testutils.h"
#include "io/input.h"
#include "io/output.h"
#include "math/numberutils.h"
#include "math/ssaops.h"

//#define DEBUG_STG1_FILE "done/10^8_stage1.txt"
//#define DEBUG_STG2_FILE "done/10^8_stage2.txt"
//#define DEBUG_STG3_FILE "done/10^8_stage3.txt"

#define	EXIT_INPUT_READ_ERROR		0x01	/* Error reading input */
#define	EXIT_CHAR_3D_EXPECTED		0x02	/* '=' expected */
#define	EXIT_CHAR_5D_EXPECTED		0x03	/* ']' expected */
#define	EXIT_CHAR_5B_EXPECTED		0x04	/* '[' expected */
#define	EXIT_CHAR_04_EXPECTED		0x05	/* EOT expected */
#define	EXIT_CHAR_DIGIT_EXPECTED	0x06	/* Digit expected */
#define	EXIT_RADIX_EXCEEDS_LIMIT	0x07	/* Radix exceeds limit */
#define	EXIT_OUTPUT_WRITE_ERROR		0x08	/* Error writing output */

void rdx_fasterIntegerOutputTarPow(digit baseExps[DIGIT_BITS], digit srcRadix, digit tarRadix, digit exponent) {
	/*** Target radix is a power of source radix ***/

	/* Example: [11100101011111110]2=16
	 *   srcRadix = 2,
	 *   tarRadix = 16 = 2^4
	 *   exponent = 4
	 *
	 * Input characters are parsed as: [1;1100,1010,1111,1110]2 => [1;C,A,F,E]16
	 *
	 * First digits (with omitted leading zeroes) are parsed specially:
	 *   "1" (only one character since LENGTH("11100101011111110") mod exponent = 17 mod 4 = 1
	 * The rest is multiple of exponent.
	 */
	output_init(tarRadix, INDEX_ONE, BUFSIZ / DIGIT_BITS);

	exps(baseExps, exponent, srcRadix);
	digit first = input_parseNumberFront(baseExps, input_unprocessed() % exponent);
	if (first) {
		/* Parse first digit (leading zeroes are omitted). */
		output_digit_(first, tarRadix, false, INDEX_ONE);
		for (index i = input_unprocessed() / exponent; i--;) {
			output_digit_(input_parseNumberFront(baseExps, exponent), tarRadix, true, INDEX_ONE);
		}
	} else {
		/* Output the zero. */
		output_digit_(DIGIT_ZERO, tarRadix, true, INDEX_ONE);
	}

	output_free();
}

void rdx_fasterIntegerOutputSrcPow(digit srcRadix, digit tarRadix, digit exponent) {
	/*** Source radix is a power of target radix ***/

	/* Example: [4ACE5]16=2
	 *   srcRadix = 16 = 2^4
	 *   tarRadix = 2
	 *   exponent = 4
	 *
	 * Input characters are parsed as: [4;A,C,E,5]16 => [100;1010,1100,1110,0101]2
	 *
	 * All characters are parsed the same way.
	 * Only the first one id written to the output with omitted leading zeroes.
	 */
	output_init(tarRadix, exponent, BUFSIZ / DIGIT_BITS);

	digitp first = parseCharFront(srcRadix);
	if (first) {
		/* Write first digit (leading zeroes are omitted). */
		output_digit_(first, tarRadix, false, exponent);
		for (index i = input_unprocessed(); i--;) {
			output_digit_(parseCharFront(srcRadix), tarRadix, true, exponent);
		}
	} else {
		/* Output the zero. */
		output_digit_(DIGIT_ZERO, tarRadix, true, INDEX_ONE);
	}

	output_free();
}

void rdx_fastIntegerInput(
		digit* internal, index internalUsed,
		digit* subresultTemp, index subresultTempUsed,
		digit* baseExpTemp, index baseExpTempUsed, index baseExpTempAlloc) {

	debugln(" Performing FII:");
	stopwatch_start(fii);

	/* Allocate memory for multiplication. */
	index mulTempAlloc = baseExpTempAlloc + ODD(baseExpTempAlloc);
	debugfln("  $ Allocating %"PRIuFAST64" digits (%s) for multiplication...", mulTempAlloc, sizeofdigits(0, mulTempAlloc));
	digit* mulTemp = digits_alloc(mulTempAlloc);

	/* Perform algorithm A1.25. */
	for (index basePower = INDEX_ONE; basePower < internalUsed; basePower <<= 1) {

		// base *= base
		curta_copy(subresultTemp, baseExpTemp, subresultTempUsed = baseExpTempUsed);
		curta_mulKT(baseExpTemp, &baseExpTempUsed, subresultTemp, subresultTemp, subresultTempUsed, mulTemp);
		//SSA//ListItem* mulTemp = ssa_mul_alloc(subresultTempUsed);
		//SSA//ssa_mul(baseExpTemp, &baseExpTempUsed, subresultTemp, subresultTemp, &subresultTempUsed, mulTemp);
		//SSA//ssa_mul_free(mulTemp);

		// Since baseExpTemp was zeroised at the beginning and b*b can't be smaller than b there is no need to zeroise here.
		//zeroise__(baseExpTemp + baseExpTempUsed, baseExpTempAlloc - baseExpTempUsed);
		for (index k = INDEX_ZERO; k + basePower < internalUsed; k += 2 * basePower) {
			// internal[i] + base * internal[i + radixExp]
			curta_mulKT(subresultTemp, &subresultTempUsed, baseExpTemp, internal + k + basePower, baseExpTempUsed, mulTemp);
			//SSA//ListItem* mulTemp = ssa_mul_alloc(baseExpTempUsed);
			//SSA//ssa_mul(subresultTemp, &subresultTempUsed, baseExpTemp, internal + k + basePower, &baseExpTempUsed, mulTemp);
			//SSA//ssa_mul_free(mulTemp);
			/* Carry is always zero. */
			curta_add(internal + k, &subresultTempUsed, internal + k, basePower, subresultTemp, subresultTempUsed);
			curta_zeroise(internal + k + subresultTempUsed, 2 * basePower - subresultTempUsed);
		}

		debugfln("  Iteration-%"PRIuFAST64" performed with len(b)=%"PRIuFAST64" and len(b*l_i)~%"PRIuFAST64, basePower, baseExpTempUsed, subresultTempUsed);
	}

	debugfln("  $ Freeing %"PRIuFAST64" digits (%s)...", mulTempAlloc, sizeofdigits(0, mulTempAlloc));
	digits_free(mulTemp);

	tracefln(" ...took %"PRIi64" s", stopwatch_stop(fii));
}

index rdx_divisor_normalize(digit* digits, index digitsUsed) {

	/* Number of bits to SHL to fulfill the normalization assumption of division algorithm */
	index shift = DIGIT_BITS - logB(digits[digitsUsed - INDEX_ONE]) - DIGIT_ONE;

	/* Shift left by shiftBits bits. */
	curta_shl(digits, &digitsUsed, digits, digitsUsed, shift);

	return shift;
}

void rdx_divisor_denormalize(digit* digits, index digitsUsed, index shift) {

	/* Shift right by shiftBits bits. */
	curta_shr(digits, &digitsUsed, digits, digitsUsed, shift);
}

void rdx_divisors(digit* baseExps[DIGIT_BITS], index baseExpsUsed[DIGIT_BITS], index used) {

	debugln(" Precomputing divisors:");
	stopwatch_start(divisors);

	/* Allocate memory for multiplication. */
	index mulTempAlloc = baseExpsUsed[INDEX_ZERO] + ODD(baseExpsUsed[INDEX_ZERO]);
	debugfln("  $ Allocating %"PRIuFAST64" digits (%s) for multiplication...", mulTempAlloc, sizeofdigits(0, mulTempAlloc));
	digit* mulTemp = digits_alloc(mulTempAlloc); // TODO if (null)

	while (used--) {

		index baseExpsAlloc = baseExpsUsed[used];

		// base *= base
		curta_mulKT(baseExps[used], &baseExpsUsed[used],
				baseExps[used + INDEX_ONE], baseExps[used + INDEX_ONE], baseExpsUsed[used + INDEX_ONE],
				mulTemp);
		//SSA//ListItem* mulTemp = ssa_mul_alloc(baseExpsUsed[used + INDEX_ONE]);
		//SSA//ssa_mul(baseExps[used], &baseExpsUsed[used],
		//SSA//		baseExps[used + INDEX_ONE], baseExps[used + INDEX_ONE], &baseExpsUsed[used + INDEX_ONE],
		//SSA//		mulTemp);
		//SSA//ssa_mul_free(mulTemp);

		curta_zeroise(baseExps[used] + baseExpsUsed[used], baseExpsAlloc - baseExpsUsed[used]);

		debugfln("  Iteration performed with len(b)=%"PRIuFAST64"/%"PRIuFAST64, baseExpsUsed[used], baseExpsAlloc);
	}

	debugfln("  $ Freeing %"PRIuFAST64" digits (%s)...", mulTempAlloc, sizeofdigits(0, mulTempAlloc));
	digits_free(mulTemp);

	tracefln(" ...took %"PRIi64" s", stopwatch_stop(divisors));
}

void rdx_divisors_inverse(
		digit* baseExpsInv[DIGIT_BITS], index baseExpsInvUsed[DIGIT_BITS],
		digit* baseExps[DIGIT_BITS], index baseExpsUsed[DIGIT_BITS], index baseExpsShift[DIGIT_BITS], index used) {

	debugln(" Precomputing inverse divisors:");
	stopwatch_start(invdivisors);

	/* Allocate memory for multiplication. */
	index tempAlloc = (3 * baseExpsUsed[INDEX_ZERO]) / 2 + 3;
	debugfln("  $ Allocating %"PRIuFAST64" digits (%s) for divisor inversion...", tempAlloc, sizeofdigits(0, tempAlloc));
	digit* temp = digits_alloc(tempAlloc); // TODO if (null)

	for (index i = INDEX_ZERO; i <= used; i++) {

		// normalize
		baseExpsShift[i] = rdx_divisor_normalize(baseExps[i], baseExpsUsed[i]);

//		// TODO nemozme zatial odkomentovat, lebo divBT_inplace pouziva inverzne prvky aj pre tie najmensie delenia... je to ok?
//		if (baseExpsUsed[i] < TRESHOLD_DIV_BARRET) {
//			baseExpsInvUsed[i] = INDEX_ZERO;
//			continue;
//		}

		ssa_divNT(baseExpsInv[i], &baseExpsInvUsed[i], baseExps[i], baseExpsUsed[i], temp);

		debugfln("  Iteration performed with len(b)=%"PRIuFAST64, baseExpsInvUsed[i]);
	}

	debugfln("  $ Freeing %"PRIuFAST64" digits (%s)...", tempAlloc, sizeofdigits(0, tempAlloc));
	digits_free(temp);

	tracefln(" ...took %"PRIi64" s", stopwatch_stop(invdivisors));
}

void rdx_fastIntegerOutput(
		digit* r, index rUsed,
		digit* temp,
		digit* baseExps[DIGIT_BITS], index baseExpsUsed[DIGIT_BITS], index baseExpsShift[DIGIT_BITS],
		digit* baseExpsInv[DIGIT_BITS], index baseExpsInvUsed[DIGIT_BITS],
		index level,
		index* k) {

	/* If we are at the bottom, r contains single digit which can be easily written to the output */
	if (baseExps[level] == NULL) {
		if (output_digit(*r) < 0) {
			//*op |= OP_WRITE;
		}
		return;
	}
	/* Zeroes can be written easily (k holds their count) */
	if (curta_z(r, rUsed)) {
		if (output_zero(*k) < 0) {
			//*op |= OP_WRITE;
		}
		return;
	}

	/* Normalize dividend/reminder */
	curta_shl(r, &rUsed, r, rUsed, baseExpsShift[level]);

	/* Quotient */
	index qAlloc = rUsed <= baseExpsUsed[level] ? INDEX_ONE : (baseExpsUsed[level] + INDEX_ONE); // TODO if null // rUsed <= baseExpsUsed[level] ? INDEX_ONE : (rUsed - baseExpsUsed[level] + INDEX_ONE)
	digit* q = digits_alloc(qAlloc);
	index qUsed = 0;
	/* Zeroise the most significant digit (since in divBT only divisor->used digits are updated */
	*(q + qAlloc - INDEX_ONE) = DIGIT_ZERO;

	/* Division */
	ssa_divBT(q, &qUsed, r, &rUsed, baseExps[level], baseExpsUsed[level], baseExpsInv[level], baseExpsInvUsed[level], temp);

	/* Denormalize dividend/remainder */
	curta_shr(r, &rUsed, r, rUsed, baseExpsShift[level]);

	*k >>= 1;

	rdx_fastIntegerOutput(q, qUsed, temp, baseExps, baseExpsUsed, baseExpsShift, baseExpsInv, baseExpsInvUsed, level + 1, k);
	digits_free(q);

	rdx_fastIntegerOutput(r, rUsed, temp, baseExps, baseExpsUsed, baseExpsShift, baseExpsInv, baseExpsInvUsed, level + 1, k);

	*k <<= 1;
}

void rdx_convert(digit srcRadix, digit tarRadix, digit baseExps[DIGIT_BITS]) {

	/* exponent > 0 iff srcRadix^exponent = tarRadix or srcRadix = tarRadix^exponent */
	digit exponent = srcRadix < tarRadix ? logD(srcRadix, tarRadix) : logD(tarRadix, srcRadix);

	debugfln("Radix %"PRIuFAST64" to %"PRIuFAST64"... ", srcRadix, tarRadix);
	debugfln(" %"PRIuFAST64" digits", input_unprocessed());
	if (exponent) {

		debugln(";-P");

		if (srcRadix < tarRadix) {
			debugln(" Target radix is a power of source radix...");
			rdx_fasterIntegerOutputTarPow(baseExps, srcRadix, tarRadix, exponent);
		} else {
			debugln(" Source radix is a power of target radix...");
			rdx_fasterIntegerOutputSrcPow(srcRadix, tarRadix, exponent);
		}

	} else {

		/* Parse number in source radix using Algorithm 1.25 FastIntegerInput */
		debugln(":-O");

		/* Rolling example:
		 * [198107041983071720131127]10=10, internal-base=10000
		 */

		/* # of Digits of source radix that can fit inTO one digit of Internal Radix. */
		/* Example: 4 */
		index d2ir = digits2ibase(srcRadix);
		debugfln("%"PRIuFAST64" source digits (%"PRIuFAST64"^%"PRIuFAST64") ~> 1 internal digit (2^%"PRId32")", d2ir, srcRadix, d2ir, DIGIT_BITS);

		/* Source radix exponentials. */
		/* Example: { 1, 10, 100 } */
		exps(baseExps, d2ir, srcRadix);

		/* Since number is not processed yet, the number of unprocessed characters
		 * is equal to input number length.
         * A1.25:
         *   m
		 */
		index inputCount = input_unprocessed();

		/* Number of digits (in internal radix) required to hold entire number.
		 * Example: 6
		 *   ... since init for algorithm will be { 1127, 2013, 0717, 1983, 0704, 1981 }
		 *   Note that this are actually four numbers!
		 *   This estimation is the # of digits required to hold input number
		 *   (before converting it into internal radix).
		 */
		index internalUsed = CEIL(inputCount, d2ir);

		/* Estimation of the internal number used digits.
		 * Example: 6+1
		 *   ... since after algorithm we will have { 1127, 2013, 0717, 1983, 0704, 1981 }
		 *   Note that this is actually one number!
		 *   Since internal radix is a power of source radix we won't see difference in size and value.
		 *   This estimation is the actual size of the number after the algorithm i.e. it is
		 *   the size of the input number in internal radix.
		 */
		index internalEstimation = logX(srcRadix) * inputCount / DIGIT_BITS + INDEX_ONE;

		/* # of iterations required by the algorithm to convert the input number into internal radix.
		 * Example: 3
		 */
		index iterations;
		/* # of digits to allocate with respect to space requirements of the multiplication algorithm.
		 * Example: 8
		 */
		index internalAlloc;
		switch (internalUsed) {
			case 0:
			case 1:
				iterations = DIGIT_ZERO;
				internalAlloc = internalUsed;
				break;
			default:
				iterations = logB(internalUsed - DIGIT_ONE) + DIGIT_ONE;
				internalAlloc = INDEX_ONE << iterations;
				break;
		}

		// TODO!!! POZOR! na to aby nam zafungovalo prve delenie pri FIO, potrebujeme mat splnene, ze internalAlloc >= dsrlen + ilen - 1
		// co by malo byt cca 2*(dsrlen - 1)...
		// Z technickych dovodov teraz dame 2*internalAlloc a ked vytvorim Cassandru, tak to budem moct dat ako MAX(internalAlloc, 2*(predikovany dsrlen - 1))
		internalAlloc <<= 1;

		debugfln(" %"PRIuFAST64" total source digits (b=%"PRIuFAST64") ~> %"PRIuFAST64" total internal digits (before FII), %"PRIuFAST64" allocated", inputCount, srcRadix, internalUsed, internalAlloc);
		debugfln(" %"PRIuFAST64" internal digits ~FII~%"PRIuFAST64"~iterations~> %"PRIuFAST64" internal digits (b=2^%"PRId32")", internalUsed, iterations, internalEstimation, DIGIT_BITS);

		// Memory Consumption Example:
		//   Let's examine the 10^9-digit number consisting of digit 35 in radix 36.
		//   This digit will occupy log(35^(10^9))/log(2^64) = 80145048 internal digits (611 MB).
		//   So we need 134217728 allocated internal digits (1024 MB).

		/* Allocate digits.
		 * Example: { ?, ?, ?, ?, ?, ?, ?, ? }
		 */
		debugfln(" $ Allocating %"PRIuFAST64" internal digits (%s)...", internalAlloc, sizeofdigits(0, internalAlloc));
		digit* internal;
#ifdef DEBUG_STG1_FILE
		internal = file2digits(DEBUG_STG1_FILE, &internalUsed, &internalAlloc);
#else
		internal = digits_alloc(internalAlloc); // TODO if (null)
		/* Parse them all.
		 * Example: { 1127, 2013, 0717, 1983, 0704, 1981, ?, ? }
		 */
		debugfln("  Parsing %"PRIuFAST64" input digits into %"PRIuFAST64" internal digits...", inputCount, internalUsed);
		stopwatch_start(parse);
		for (index i = 0; i < internalUsed; ++i) {
			internal[i] = input_parseNumberBack(baseExps, d2ir);
		}
		tracefln("  ...took %"PRIi64" s", stopwatch_stop(parse));
#endif

		/* Set rest to zero.
		 * Example: { 1127, 2013, 0717, 1983, 0704, 1981, 0, 0 }
		 */
		debugfln("  Zeroise %"PRIuFAST64" remaining internal digits...", internalAlloc - internalUsed);
		curta_zeroise(internal + internalUsed, internalAlloc - internalUsed);

		/* FastIntegerInput algorithm is required if:
		 * - we have more than one digit (if there is only one digit, the work is done) and
		 * - source radix is not a 2^1, 2^2 or 2^4 (for base 2, 4 or 16 the works is already done).
		 */
		if (internalUsed > INDEX_ONE) {

			if (srcRadix == 2 || srcRadix == 4 || srcRadix == 16) {

				debugln("FastIntegerInput not required ;-P");

			} else {

				debugln("FastIntegerInput...");

				/* Estimation of biggest source radix power used digits:
				 *   log_{2^64}({radix^{d2ir/2}}^{2^iterations}) =
				 * = log_{2^64}(radix^{(d2ir/2)*2^iterations}) =
	             * = (((d2ir/2)*2^iterations) / 64) * log_{2}(radix) =
	             * = ((d2ir * 2^iterations) / (2 * 64)) * log_{2}(radix)
	             *
	             * Example: 16+1
	             *   since the biggest source radix power is 1 0000 0000 0000 0000.
				 */
				index baseExpEstimation = ((logX(srcRadix) * internalAlloc * d2ir) / (2 * DIGIT_BITS));

				/* # of digits required to hold the power of source radix/base.
				 * Example:
				 *   1 0000, 1 0000 0000, 1 0000 0000 0000 0000
	             * A1.25:
	             *   # of digits of the biggest b value
				 */
				index baseExpTempAlloc = baseExpEstimation + DIGIT_ONE;
				index baseExpTempUsed = DIGIT_ZERO;

				/* # of digits required to hold product power-of-source-radix * digits
	             * A1.25:
	             *   # of digits of the biggest b*l_k value
				 */
				index subresultTempAlloc = 2 * baseExpTempAlloc;
				index subresultTempUsed = DIGIT_ZERO;

#ifdef DEBUG_STG2_FILE
				internal = file2digits(DEBUG_STG2_FILE, &internalUsed, &internalAlloc);
#else
				/* Allocate memory for base exponentials and subresults. */
				debugfln(" $ Allocating %"PRIuFAST64" digits for base exponential [b] (%s)...", baseExpTempAlloc, sizeofdigits(0, baseExpTempAlloc));
				debugfln(" $ Allocating %"PRIuFAST64" digits for product [b*l_i] (%s)...", subresultTempAlloc, sizeofdigits(0, subresultTempAlloc));
				digit* baseExpTemp = digits_alloc(baseExpTempAlloc + subresultTempAlloc); // TODO if (null)
				digit* subresultTemp = baseExpTemp + baseExpTempAlloc;

				/* Initial base value.
	             * A1.25:
	             *   (b, k) <- (B, m)
				 */
				curta_zeroise(baseExpTemp, baseExpTempAlloc);
				/* Square root of the computed maximum value since first thing in FII is b *= b */
				baseExpTemp[baseExpTempUsed++] = baseExps[d2ir >> 1];

				rdx_fastIntegerInput(
						internal, internalUsed,
						subresultTemp, subresultTempUsed,
						baseExpTemp, baseExpTempUsed, baseExpTempAlloc);

				debugfln(" $ Freeing %"PRIuFAST64" digits (%s)...", subresultTempAlloc, sizeofdigits(0, subresultTempAlloc));
				debugfln(" $ Freeing %"PRIuFAST64" digits (%s)...", baseExpTempAlloc, sizeofdigits(0, baseExpTempAlloc));
				digits_free(baseExpTemp);
#endif
			}
		}

		curta_msd(internal, &internalUsed);
		digits2file("internal.txt", internal, internalUsed, internalAlloc);

		/* FastIntegerOutput algorithm is required if:
		 * - we have more than one digit (if there is only one digit, the work is done) and
		 * - source radix is not a 2^1, 2^2 or 2^4 (for base 2, 4 or 16 the works is already done).
		 */
		if (tarRadix == 2 || tarRadix == 4 || tarRadix == 16) {

			debugln("FastIntegerOutput not required ;-P");
			// TODO zvolit vhodnu implementaciu...
			// pomoze rdx_fasterIntegerOutputSrcPow alebo rdx_fasterIntegerOutputTarPow ???

		} else {

			/*** PRECOMPUTE DIVISORS ***/

			/* # of Digits of source radix that can fit inTO one digit of Target Radix. */
			/* Example: 4 */
			index d2tr = digits2ibase(tarRadix);

			debugfln("%"PRIuFAST64" target digits (%"PRIuFAST64"^%"PRIuFAST64") ~> 1 internal digit (2^%"PRId32")", d2tr, tarRadix, d2tr, DIGIT_BITS);

			/* Estimation of target radix power # of iterations:
			 *   {tarRadix^d2tr}^{2^n} <= {2^DIGIT_BITS}^internalUsed
			 * i.e. target exponential can't be greater than (internal) number;
			 *      target exponential is in form base^{2^n} since in each iteration base := base^2
			 *   d2tr * 2^n * log_{2^DIGIT_BITS}(tarRadix) <= internalUsed * log_{2^DIGIT_BITS}(2^DIGIT_BITS)
			 *   d2tr * 2^n * log_2(tarRadix)/DIGIT_BITS <= internalUsed
			 *   2^n <= DIGIT_BITS * internalUsed / (d2tr * log_2(tarRadix))
			 *   n <= log_2(DIGIT_BITS * internalUsed / (d2tr * log_2(tarRadix)))
             *
             * Example: 5
             *   since the biggest target radix power is 1 0000 0000 0000 0000.
			 */
			iterations = logB(((DIGIT_BITS / d2tr) * internalUsed) / logB(tarRadix));

			exps(baseExps, d2tr, tarRadix);

			index baseExps_Used[DIGIT_BITS] = { INDEX_ZERO };

			/* Estimation of all target radix powers used digits sum:
			 *   S_0 + S_1 + ... + S_iterations
			 * = (d2tr * (2^0 + 2^1 + ... + 2^iterations) / 64) * log_{2}(radix) + iterations
			 * = (d2tr * 2^{iterations + 1} / 64) * log_{2}(radix) + iterations
             *
             * Example: ?
			 */
			index baseExps_Estimation = INDEX_ZERO;

			index j;
			for (index i = INDEX_ZERO; i < iterations; ++i) {

				j = (logX(tarRadix) * (INDEX_ONE << (iterations - (i + 1))) * d2tr) / DIGIT_BITS + INDEX_ONE;

				/* Estimation of i-th target radix power used digits:
				 *   log_{2^64}({radix^d2tr}^{2^i}) =
				 * = log_{2^64}(radix^{d2tr*2^i}) =
				 * = (d2tr * 2^i / 64) * log_{2}(radix)
				 *
				 * Example: ?
				 */
				baseExps_Estimation += baseExps_Used[i] = 2*j;
			}
			baseExps_Estimation += baseExps_Used[iterations] = 1;

			/* Allocate memory for base exponentials. */
			index baseExps_TempAlloc = baseExps_Estimation;
			debugfln(" $ Allocating %"PRIuFAST64" digits for base exponentials [b] (%s)...", baseExps_TempAlloc, sizeofdigits(0, baseExps_TempAlloc));
			digit* baseExps_Temp = digits_alloc(baseExps_TempAlloc); // TODO if (null)

			digit* baseExps_[DIGIT_BITS] = { NULL };
			baseExps_[INDEX_ZERO] = baseExps_Temp;
			for (index i = INDEX_ZERO; i < iterations; ++i) {
				baseExps_[i + INDEX_ONE] = baseExps_[i] + baseExps_Used[i];
			}
			*(baseExps_[iterations]) = tarRadix * baseExps[d2tr - INDEX_ONE];

			rdx_divisors(baseExps_, baseExps_Used, iterations);

			//~~~ log to file
			char filename[64];
			for (int i = 0; i <= iterations; ++i) {
				sprintf(filename, "internal_STACK_%03"PRIu32".txt", i);
				digits2file(filename, baseExps_[i], baseExps_Used[i], baseExps_Used[i]);
			}
			//~~~ log to file ~~~

			/*** PRECOMPUTE INVERSE DIVISORS ***/

			index baseExpsInvEstimation = INDEX_ZERO;

			index baseExpssInvUsed[DIGIT_BITS] = { INDEX_ZERO };
			for (index i = INDEX_ZERO; i <= iterations; ++i) {
				/* Since for inverse we need allocated >= dsrlength + 2 digits  */
				baseExpsInvEstimation += baseExpssInvUsed[i] = baseExps_Used[i] + 2;
			}

			/* Allocate memory for base exponential inverses. */
			index baseExpsInvTempAlloc = baseExpsInvEstimation;
			debugfln(" $ Allocating %"PRIuFAST64" digits for base exponential inverses [1/b] (%s)...", baseExpsInvTempAlloc, sizeofdigits(0, baseExpsInvTempAlloc));
			digit* baseExpsInvTemp = digits_alloc(baseExpsInvTempAlloc); // TODO if (null)

			digit* baseExpssInv[DIGIT_BITS] = { NULL };
			baseExpssInv[INDEX_ZERO] = baseExpsInvTemp;
			for (index i = INDEX_ZERO; i < iterations; ++i) {
				baseExpssInv[i + INDEX_ONE] = baseExpssInv[i] + baseExpssInvUsed[i];
			}

			index baseExps_Shift[DIGIT_BITS] = { INDEX_ZERO };

			rdx_divisors_inverse(baseExpssInv, baseExpssInvUsed, baseExps_, baseExps_Used, baseExps_Shift, iterations);

			//~~~ log to file
			for (int i = INDEX_ZERO; i <= iterations; ++i) {
				sprintf(filename, "inverse_STACK_%03"PRIu32".txt", i);
				digits2file(filename, baseExpssInv[i], baseExpssInvUsed[i], baseExpssInvUsed[i]);
			}
			//~~~ log to file ~~~

			index targetBaseExpsEstimation = 0;

			// kolko cifier bude mat vysledok
			index targetEstimation = logX(srcRadix) * inputCount / logX(tarRadix) + INDEX_ONE;

			debugfln("%"PRIuFAST64" internal digits (b=2^%"PRId32") ~FIO~%"PRIuFAST64"~iterations~> %"PRIuFAST64" output digits (b=%"PRIuFAST64")", internalEstimation, DIGIT_BITS, iterations, targetEstimation, tarRadix);

			debugln(" Performing FIO:");
			stopwatch_start(fio);

			index digits = digits2ibase(tarRadix);

			digit* temp = digits_alloc(2 * baseExpssInvUsed[INDEX_ZERO]); // TODO if (null)

			output_init(tarRadix, d2tr, BUFSIZ / DIGIT_BITS);

			rdx_fastIntegerOutput(
					internal, internalUsed,
					temp,
					baseExps_, baseExps_Used, baseExps_Shift,
					baseExpssInv, baseExpssInvUsed,
					0, &digits);

			output_free();

			digits_free(temp);

			tracefln(" ...took %"PRIi64" s", stopwatch_stop(fio));

			debugfln(" $ Freeing %"PRIuFAST64" digits (%s)...", baseExpsInvTempAlloc, sizeofdigits(0, baseExpsInvTempAlloc));
			digits_free(baseExpsInvTemp);

			debugfln(" $ Freeing %"PRIuFAST64" digits (%s)...", baseExps_TempAlloc, sizeofdigits(0, baseExps_TempAlloc));
			digits_free(baseExps_Temp);
		}

		// Oplati sa vytvorit priestor specialne sity pre cislo a nasledne delenie pricom sucasny internal
		// uvolnime? Asi ano. Preverit.

		// TEST digits2file("internal_pokus.txt", internal, internalUsed, internalAlloc);
		debugfln(" $ Freeing %"PRIuFAST64" digits (%s)...", internalAlloc, sizeofdigits(0, internalAlloc));
		digits_free(internal);
	}
}

int rdx_exit(const int status, const char* message) {
	infofln("%s", message);
	/* TODO Free output ??? */
	/* Free input */
	input_free();
	/* Terminate with specific exit code */
	return status;
}

int main(int argc, char **argv) {

	/* Initialize input */
	input_init(BUFSIZ);

	/* Load input */
	debugln("Reading input...");
	input_read();

	/* Terminate if loading was not successful */
	/* In this case input stack is empty, so current char is EOT */
	if (isCharBack(ASCII_EOT)) {
		return rdx_exit(EXIT_INPUT_READ_ERROR, "Error reading input");
	}

	/* Tolerate trailing whitespace. */
	while (isCharBack(ASCII_WHS))
		;

	/* 2 for parsing radix (since only two digit radix is supported)
	 * 5 for parsing input when tarRadix = srcRadix^k (since RADIX_MAX < 2^6, exponent < 6)
	 * 64 for parsing input (since 64 binary digits fit into DIGIT)
	 */
	digit baseExps[DIGIT_BITS] = { 1, 10 };

	/* Parse target radix */
	debug(" Parse target radix... ");
	digit tarRadix = input_parseNumberBack(baseExps, 2);
	debugfln("%"PRIuFAST64, tarRadix);

	/* Terminate if target radix exceeds limit */
	if (tarRadix < RADIX_MIN || tarRadix > RADIX_MAX) {
		return rdx_exit(EXIT_RADIX_EXCEEDS_LIMIT, "Target radix exceeds limit");
	}

	/* Next character has to be '=' (reading backwards) */
	if (!isCharBack('=')) {
		return rdx_exit(EXIT_CHAR_3D_EXPECTED, "'=' expected");
	}

	/* Parse source radix */
	debug(" Parse source radix... ");
	digit srcRadix = input_parseNumberBack(baseExps, 2);
	debugfln("%"PRIuFAST64, srcRadix);

	/* Terminate if source radix exceeds limit */
	if (srcRadix < RADIX_MIN || srcRadix > RADIX_MAX) {
		return rdx_exit(EXIT_RADIX_EXCEEDS_LIMIT, "Source radix exceeds limit");
	}

	// A very special case.
	// Only makes a copy to the output without further syntactic checks! :(
	if (srcRadix == tarRadix) {

		debugln("B-)");

		/* Print input */
		input_flush(srcRadix);
		/* Terminate with success exit code */
		return rdx_exit(EXIT_SUCCESS, "OK");
	}

	/* Next character has to be ']' (reading backwards) */
	if (!isCharBack(']')) {
		return rdx_exit(EXIT_CHAR_5D_EXPECTED, "']' expected");
	}

	/* First character has to be '[' (reading forwards) */
	if (!isCharFront('[')) {
		return rdx_exit(EXIT_CHAR_5B_EXPECTED, "'[' expected");
	}

	/* If no more character is left we have no digit to process (reading forwards) */
	if (isCharBack(ASCII_EOT)) {
		return rdx_exit(EXIT_CHAR_DIGIT_EXPECTED, "digit expected");
	}

	rdx_convert(srcRadix, tarRadix, baseExps);

	/* Next character has to be EOT (reading backwards) */
	if (!isCharBack(ASCII_EOT)) {
		return rdx_exit(EXIT_CHAR_04_EXPECTED, "EOT expected");
	}

	return rdx_exit(EXIT_SUCCESS, "OK");
}
