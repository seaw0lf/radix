#ifndef NNUMBEROPSMOD_H_
#define NNUMBEROPSMOD_H_

#include "numberops.h"

bool curta_mod_sn(const digit* source, const index slength, const index n);
void curta_mod_rdc(digit* destination, const digit* source, const index slength, const index n);

void curta_mod_add(digit* result, const digit* addend1, const digit* addend2, const index n);
void curta_mod_sub(digit* result, const digit* minuend1, const digit* minuend2, const index n);

void curta_mod_shl(digit* destination, const digit* source, index digits, const index n, digit* temp);
void curta_mod_shr(digit* destination, const digit* source, index digits, const index n, digit* temp);
void curta_mod_shrb(digit* destination, const digit* source, index bits, const index n);

#endif /* NNUMBEROPSMOD_H_ */
