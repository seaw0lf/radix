#include "numberops.h"
#include "numberutils.h"
#include "../logger.h"

#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
const digit _0 = DIGIT_ZERO;
const digit* ZERO = &_0;
#endif

/*** Basic number operations withou allocation ***/

#if LOG_LEVEL >= LEVEL_DEBUG
/* Debug level check function */
/* Checks if most significant digit is nonzero */
void isMSD(const digit* comparand, const index length) {
	if (length && comparand[length - 1] == DIGIT_ZERO) {
		debug("#");
	}
}
#endif

/* Most Significant Digit
 *
 * Input:
 *   comparand - digits of a number
 *   length - number of digits of a number
 * Output:
 *   length - number of digits of a number without leading zeroes
 */
void curta_msd(const digit* comparand, index* length) {
	comparand += *length;
	for (; *length && !*--comparand; --*length)
		;
}

/* Is Zero
 *
 * Input:
 *   comparand - digits of comparand
 *   length - number of digits of comparand
 * Output:
 *   return value - true/false iff comparand is/is not zero
 */
inline bool curta_z(const digit* comparand, const index length) {
	return length == INDEX_ZERO;
}

/* Compare (comparand1 CMP_SYMBOL comparand2  IFF  cmp(comparand1, comparand2) CMP_SYMBOL 0)
 *
 * Input:
 *   comparand 1 - digits of first comparand
 *   length1 - number of digits of first comparand
 *   comparand 2 - digits of second comparand
 *   length2 - number of digits of second comparand
 * Output:
 *   return value - -1/0/1 iff comparand1 </=/> comparand2
 */
cmpres curta_cmp(register const digit* comparand1, const index length1, register const digit* comparand2, const index length2) {
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(comparand1, length1);
	isMSD(comparand2, length2);
#endif

	/* Different number of digits simplifies comparison. */
	if (length1 < length2)
		return SGN_NEG;
	if (length1 > length2)
		return SGN_POS;

	/* length1 == length2 */

	const digit* terminator = comparand1;
	comparand1 += length1;
	comparand2 += length2;

	/* Same number of digits requires comparison from most to least significant digit. */
	while (terminator < comparand1) {
		--comparand1, --comparand2;

		/* Numbers are not equal iff they have different digits at the same position. */
		if (*comparand1 != *comparand2) {
			return (*comparand1 < *comparand2) ? SGN_NEG : SGN_POS;
		}
	}

	/* All digits at the same position of both numbers are equal. So are the numbers. */
	return SGN_EQ;
}

/* Set zero (destination := 0)
 *
 * Input:
 *   length - number of digits to set to zero
 * Output:
 *   destination - digits set to zero; allocated >= length
 */
inline void curta_zeroise(register digit* destination, register index length) {

	while (length--) {
		*(destination++) = DIGIT_ZERO;
	}
}

/* Set value (destination[ix] := value)
 *
 * Input:
 *   length - number of digits to set to value
 * Output:
 *   destination - digits set to value; allocated >= length
 */
inline void curta_fill(register digit* destination, register index length, digit value) {

	while (length--) {
		*(destination++) = value;
	}
}

/* Copy source to destination (destination := source)
 *
 * Input:
 *   source - digits to copy
 *   length - number of digits to copy
 * Output:
 *   destination - target digits; allocated >= length
 */
inline void curta_copy(register digit* destination, register const digit* source, register index length) {

	while (length--) {
		*(destination++) = *(source++);
	}
}

/* Copy source to destination backwards (destination := source)
 *
 * Input:
 *   source - digits to copy
 *   length - number of digits to copy
 * Output:
 *   destination - target digits; allocated >= length
 */
inline void curta_copyb(register digit* destination, register const digit* source, register index length) {
	destination += length;
	source += length;

	while (length--) {
		*(--destination) = *(--source);
	}
}

/* Bitwise one's complement (destination := ~source)
 *
 * Input:
 *   source - digits to bitwise complement
 *   length - number of digits to bitwise complement
 * Output:
 *   destination - bitwise complemented digits; allocated >= length
 */
inline void curta_com(register digit* destination, register const digit* source, register index length) {

	while (length--) {
		*(destination++) = ~*(source++);
	}
}

/* Shift left (destination := source << bits; bits <= digit bits)
 *
 * Input:
 *   source - digits of number to shift
 *   slength - number of digits of number to shift
 *   bits - number of bits to shift
 * Output:
 *   destination - shifted number; allocated >= slength + 1
 *   length - number of digits of shifted number
 */
void curta_shl(register digit* destination, index* length, register const digit* source, const index slength, const index bits) {
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(source, slength);
	digit* dest = destination;
#endif

	/* Target length is set to most probable number */
	*length = slength;

	/* If source number is zero, there's nothing else to do */
	if (curta_z(source, slength)) {
		return;
	}
	/* If number of bits to shift is zero... */
	if (bits == INDEX_ZERO) {
		/* ... shift means to copy digits from source to destination (but the case they're the same digits) */
		if (destination != source) {
			curta_copy(destination, source, slength);
		}
		return;
	}

	/* Number of bits that won't be carried/shifted into neighbouring digit */
	const index bitsComplement = DIGIT_BITS - bits;
	/* T101 - Arnold ;) */
	const digit* terminator = source + slength;
	/* T1000 - keeps changing all the time ;) */
	digit tmp;

	/* Carry - temporary variable for storing bits of digit carried/shifted into neighbouring digit */
	digit carry = DIGIT_ZERO;

	/** Shift by bits bits **/
	while (source < terminator) {

		tmp = (*source << bits) | carry;
		carry = *source >> bitsComplement;
		*destination = tmp;

		/* Looks better but works slower... :-/
		tmp = *source;
		*destination = (tmp << bits) | carry;
		carry = tmp >> bitsComplement;
		*/

		++source, ++destination;
	}
	if (carry) {
		*destination = carry;
		/* Resolve the position of most significant digit issue (strong usage of precondition) */
		(*length)++;
	}

#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(dest, *length);
#endif
}

/* Shift right (destination := source >> bits; bits <= digit bits)
 *
 * Input:
 *   source - digits of number to shift
 *   slength - number of digits of number to shift
 *   bits - number of bits to shift
 * Output:
 *   destination - shifted number; allocated >= slength
 *   length - number of digits of shifted number
 */
void curta_shr(register digit* destination, index* length, register const digit* source, const index slength, const index bits) {
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(source, slength);
	digit* dest = destination;
#endif

	/* Target length is set to most probable number */
	*length = slength;

	/* If source number is zero, there's nothing else to do */
	if (curta_z(source, slength)) {
		return;
	}
	/* If number of bits to shift is zero... */
	if (bits == INDEX_ZERO) {
		/* ... shift means to copy digits from source to destination (but the case they're the same digits) */
		if (destination != source) {
			curta_copy(destination, source, slength);
		}
		return;
	}

	/* Number of bits that won't be carried/shifted into neighbouring digit */
	const index bitsComplement = DIGIT_BITS - bits;
	/* Temporary variable for storing resulting shifted digit with respect of carry from neighbouring digit */
	digit tmp;

	/* Carry - temporary variable for storing bits of digit carried/shifted into neighbouring digit */
	digit carry = DIGIT_ZERO;

	/* Move pointers to the most significant digit */
	const digit* terminator = source;
	source += slength;
	destination += slength;

	/* Resolve the zero value most significant digit issue (strong usage of precondition) */
	if (*(source - INDEX_ONE) >> bits == DIGIT_ZERO) {
		(*length)--;
	}

	/* Perform the shift from most to least significant digit */
	while (terminator < source) {
		--source, --destination;

		tmp = (*source >> bits) | carry;
		carry = *source << bitsComplement;
		*destination = tmp;

		/* Looks better but works slower... :-/
		tmp = *src;
		*dst = (tmp >> bits) | carry;
		carry = tmp << bitsComplement;
		*/
	}

#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(dest, *length);
#endif
}

// #define handle_carry_add(RESULT, LENGTH, CARRY) { digit XXX = CARRY; if (XXX) *(RESULT + (*LENGTH)++) += XXX; }
inline void curta_carry_add(digit* result, index* length, const digit carry) {
	if (carry)
		*(result + (*length)++) += carry;
}

inline void curta_carry_set(digit* result, index* length, const digit carry) {
	if (carry)
		*(result + (*length)++) = carry;
}

/* Addition (result := addend1 + addend2)
 *
 * Input:
 *   addend 1 - digits of first addend
 *   length1 - number of digits of first addend
 *   addend 2 - digits of second addend
 *   length2 - number of digits of second addend
 * Output:
 *   result - sum of first and second addend (without carry); allocated >= length1 + length2
 *   length - number of digits of resulting sum (without carry)
 *   carry
 */
digit curta_add(digit* result, index* length, register const digit* addend1, const index length1, register const digit* addend2, const index length2) {
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(addend1, length1);
	isMSD(addend2, length2);
#endif

	/* Pointer to the currently processed digit of result */
	register digit* rslt = result;
	/* End of first addend */
	const digit* terminator1 = addend1 + length1;
	/* End of second addend */
	const digit* terminator2 = addend2 + length2;

	/* Carry */
	digit carry = DIGIT_ZERO;

	while (addend1 < terminator1 || addend2 < terminator2) {

#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
		add(
			addend1 < terminator1 ? *addend1 : *ZERO,
			addend2 < terminator2 ? *addend2 : *ZERO,
			carry,
			*rslt);
#else
		add(
			addend1 < terminator1 ? *addend1 : DIGIT_ZERO,
			addend2 < terminator2 ? *addend2 : DIGIT_ZERO,
			&carry,
			rslt);
#endif

		rslt++;
		addend1++;
		addend2++;
	}
	*length = rslt - result;

#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(result, *length);
#endif
	return carry;
}

/* Addition by a single digit (result := addend1 + addend2)
 *
 * Input:
 *   addend 1 - digits of first addend
 *   length1 - number of digits of second addend
 *   addend 2 - digit of second addend
 * Output:
 *   result - sum of first and second addend (without carry); allocated >= length1
 *   length - number of digits of resulting sum (without carry)
 *   carry
 */
digit curta_add1(digit* result, index* length, register const digit* addend1, const index length1, const digit addend2) {
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(addend1, length1);
#endif

	bool eqPtr = result == addend1;

	// Result of addition by zero ...
	if (addend2 == DIGIT_ZERO) {
		// ...is added value.
		*length = length1;
		if (!eqPtr) {
			curta_copy(result, addend1, length1);
		}
		return DIGIT_ZERO;
	}

	/* Pointer to the currently processed digit of result */
	register digit* rslt = result;
	/* End of second addend */
	const digit* terminator2 = addend1 + length1;

	/* Carry */
	digit carry = DIGIT_ZERO;

#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
	add(
		addend2,
		addend1 < terminator2 ? *(addend1++) : *ZERO,
		carry,
		*(rslt++));
#else
	add(
		addend2,
		addend1 < terminator2 ? *(addend1++) : DIGIT_ZERO,
		&carry,
		rslt++);
#endif

	while (carry && addend1 < terminator2) {

#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
		add(
			*ZERO,
			addend1 < terminator2 ? *addend1 : *ZERO,
			carry,
			*rslt);
#else
		add(
			DIGIT_ZERO,
			addend1 < terminator2 ? *addend1 : DIGIT_ZERO,
			&carry,
			rslt);
#endif

		rslt++;
		addend1++;
	}

	if (!eqPtr && addend1 < terminator2) {
		curta_copy(rslt, addend1, terminator2 - addend1);
	}

	*length = MAX(length1, rslt - result);

#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(result, *length);
#endif
	return carry;
}

/* Subtraction (result := minuend1 - minuend2; minuend1 >= minuend2)
 *
 * Input:
 *   minuend 1 - digits of first minuend
 *   length1 - number of digits of first minuend
 *   minuend 2 - digits of second minuend; minuend1 >= minuend2
 *   length2 - number of digits of second minuend
 * Output:
 *   result - difference of first and second minuend (without borrow); allocated >= length1
 *   length - number of digits of resulting difference (without borrow)
 *   borrow
 */
digit curta_sub(digit* result, index* length, register const digit* minuend1, const index length1, register const digit* minuend2, const index length2) {
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(minuend1, length1);
	isMSD(minuend2, length2);
#endif

	/* Pointer to the currently processed digit of result */
	register digit* rslt = result;
	/* End of first minuend */
	const digit* terminator1 = minuend1 + length1;
	/* End of second minuend */
	const digit* terminator2 = minuend2 + length2;

	/* Borrow */
	digit borrow = DIGIT_ZERO;

	while (minuend1 < terminator1 || minuend2 < terminator2) {

#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
		sub(
			minuend1 < terminator1 ? *minuend1 : *ZERO,
			minuend2 < terminator2 ? *minuend2 : *ZERO,
			borrow,
			*rslt);
#else
		sub(
			minuend1 < terminator1 ? *minuend1 : DIGIT_ZERO,
			minuend2 < terminator2 ? *minuend2 : DIGIT_ZERO,
			&borrow,
			rslt);
#endif

		rslt++;
		minuend1++;
		minuend2++;
	}
	*length = rslt - result;

	curta_msd(result, length);
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(result, *length);
#endif
	return borrow;
}

/* Subtraction by a single digit (result := minuend1 - minuend2; minuend1 >= minuend2)
 *
 * Input:
 *   minuend 1 - digits of first minuend
 *   length1 - number of digits of first minuend
 *   minuend 2 - digit of second minuend; minuend1 >= minuend2
 * Output:
 *   result - difference of first and second minuend (without borrow); allocated >= length1
 *   length - number of digits of resulting difference (without borrow)
 *   borrow
 */
digit curta_sub1(digit* result, index* length, register const digit* minuend1, const index length1, const digit minuend2) {
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(minuend1, length1);
#endif

	bool eqPtr = result == minuend1;

	// Result of addition by zero ...
	if (minuend2 == DIGIT_ZERO) {
		// ...is added value.
		*length = length1;
		if (!eqPtr) {
			curta_copy(result, minuend1, length1);
		}
		return DIGIT_ZERO;
	}

	/* Pointer to the currently processed digit of result */
	register digit* rslt = result;
	/* End of first minuend */
	const digit* terminator1 = minuend1 + length1;

	/* Borrow */
	digit borrow = DIGIT_ZERO;

#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
	sub(
		minuend1 < terminator1 ? *(minuend1++) : *ZERO,
		minuend2,
		borrow,
		*(rslt++));
#else
	sub(
		minuend1 < terminator1 ? *(minuend1++) : DIGIT_ZERO,
		minuend2,
		&borrow,
		rslt++);
#endif

	while (borrow && minuend1 < terminator1) {

#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
		sub(
			minuend1 < terminator1 ? *minuend1 : *ZERO,
			*ZERO,
			borrow,
			*rslt);
#else
		sub(
			minuend1 < terminator1 ? *minuend1 : DIGIT_ZERO,
			DIGIT_ZERO,
			&borrow,
			rslt);
#endif

		rslt++;
		minuend1++;
	}

	if (!eqPtr && minuend1 < terminator1) {
		curta_copy(rslt, minuend1, terminator1 - minuend1);
	}
	*length = length1;

	curta_msd(result, length);
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(result, *length);
#endif
	return borrow;
}

/* Addition with sign (result := addend1 +/- addend2)
 *
 * Input:
 *   addend 1 - digits of first addend
 *   length1 - number of digits of first addend
 *   sgn2 - sign of second addend
 *   addend 2 - digits of second addend
 *   length2 - number of digits of second addend
 * Output:
 *   sgn - sign of resulting sum
 *   result - sum of first and second addend; allocated >= length1 + length2 + 1 or length1 depending on sgn2
 *   length - number of digits of resulting sum
 */
digit curta_sgnadd(cmpres* sgn, digit* result, index* length, const digit* addend1, const index length1, const cmpres sgn2, const digit* addend2, const index length2) {
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(addend1, length1);
	isMSD(addend2, length2);
#endif

	if (sgn2 == SGN_EQ) {
		/* result = addend1 + 0 */
		*sgn = SGN_POS;
		*length = length1;
		if (result != addend1) {
			curta_copy(result, addend1, length1);
		}
		return DIGIT_ZERO;
	} else if (sgn2 > SGN_EQ) {
		/* result = addend1 + adden2 */
		*sgn = SGN_POS;
		return curta_add(result, length, addend1, length1, addend2, length2);
	}

	/* result = addend1 - adden2 */
	*sgn = curta_cmp(addend1, length1, addend2, length2);

	if (*sgn < SGN_EQ) {
		return curta_sub(result, length, addend2, length2, addend1, length1);
	} else if (*sgn > SGN_EQ) {
		return curta_sub(result, length, addend1, length1, addend2, length2);
	} else {
		/* length1 == length2 */
		*length = INDEX_ZERO;
		curta_zeroise(result, length1);
		return DIGIT_ZERO;
	}
}

/* Multiplication by a single digit (result := multiplicand1 * multiplicand2)
 *
 * Input:
 *   multiplicand 1 - sigle digit
 *   multiplicand 2 - digits of second multiplicand
 *   length - number of digits of second multiplicand
 * Output:
 *   result - product of digit and second multiplicand; allocated >= length + 1
 *   rlength - number of digits of resulting product
 */
void curta_mul1(digit* result, index* rlength, const digit multiplicand1, register const digit* multiplicand2, const index length) {

	// Result of multiplication by zero ...
	if (multiplicand1 == DIGIT_ZERO) {
		// ...is zero.
		*rlength = INDEX_ZERO;
		return;
	}

	register digit* rslt = result;
	const digit* terminator2 = multiplicand2 + length;

	/* Carry */
	digit carry = DIGIT_ZERO;

	while (multiplicand2 < terminator2) {
		/* Iterate over multiplicand2 */

		/* Multiply */
#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
		mul(
			multiplicand1,
			*multiplicand2,
			*ZERO,
			carry,
			*rslt);
#else
		mul(
			multiplicand1,
			*multiplicand2,
			DIGIT_ZERO,
			&carry,
			rslt);
#endif

		rslt++;
		multiplicand2++;
	}
	if (carry) {
		*(rslt++) = carry;
	}
	*rlength = rslt - result;

#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(result, *rlength);
#endif
}

/* Basecase Multiplication (result := multiplicand1 * multiplicand2)
 *
 * Input:
 *   multiplicand 1 - digits of first multiplicand
 *   multiplicand 2 - digits of second multiplicand
 *   length - number of digits of first / second multiplicand (same length expected)
 * Output:
 *   result - product of first and second multiplicand; allocated >= 2 * length
 *   rlength - number of digits of resulting product
 *
 * Algorithm:
 *   Algorithm M (Multiplication of nonnegative integers) by Knuth in The Art of Computer Programming, Volume 2
 */
static void curta_mulBC(digit* result, index* rlength, register const digit* multiplicand1, register const digit* multiplicand2, const index length) {

	const digit* mul2 = multiplicand2;
	const digit* res = result;
	register digit* rslt = result;

	const digit* terminator1 = multiplicand1 + length;
	const digit* terminator2 = multiplicand2 + length;

	/* Carry */
	digit carry;

	/* Initialize result */
	curta_zeroise(result, length << 1);

	while (multiplicand1 < terminator1) {
		/* Iterate over multiplicand1 */
		carry = DIGIT_ZERO;
		rslt = result;

		for (multiplicand2 = mul2; multiplicand2 < terminator2; multiplicand2++, rslt++) {
			/* Iterate over multiplicand2 */

			/* Multiply */
#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
			mul(
				*multiplicand1,
				*multiplicand2,
				*rslt,
				carry,
				*rslt);
#else
			mul(
				*multiplicand1,
				*multiplicand2,
				*rslt,
				&carry,
				rslt);
#endif
		}
		if (carry) {
			*(rslt++) = carry;
		}

		multiplicand1++;
		result++;
	}
	*rlength = rslt - res;

	curta_msd(res, rlength);
#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(res, *rlength);
#endif
}

/* Karatsuba's Multiplication (result := multiplicand1 * multiplicand2)
 *
 * Input:
 *   multiplicand 1 - digits of first multiplicand
 *   multiplicand 2 - digits of second multiplicand
 *   mlength - number of digits of first / second multiplicand (same length expected)
 *   temp - additional temporary space; allocated >= mlength + (mlength % 2)
 * Output:
 *   result - product of first and second multiplicand; allocated >= 2 * mlength
 *   rlength - number of digits of resulting product
 *
 * Algorithm:
 *   by Thome in Karatsuba multiplication with temporary space of size <= n
 *   http://www.loria.fr/~thome/files/kara.pdf
 */
void curta_mulKT(digit* result, index* length, const digit* multiplicand1, const digit* multiplicand2, const index mlength, digit* temp) {

	/* Cut off */
	if (mlength <= TRESHOLD_MUL_KARATSUBA) {
		curta_mulBC(result, length, multiplicand1, multiplicand2, mlength);
		return;
	}

	index p = mlength >> 1;
	index q = mlength - p;
	index p2 = p * 2;
	index px, qx;

	/* Prerequisites: */
	/* result has at least 2*length allocated digits */
	/* temp has at least 2*q = length + (length % 2) = length + ODD(length) <= length + 1 allocated digits */

	index rlength;
	index tlength;
	cmpres rsgn = SGN_POS;
	cmpres tsgn = SGN_NEG;

	/* Step 1 / result := alpha = m1_lo - m1_hi (+/-) */
	px = p; curta_msd(multiplicand1, &px);
	qx = q; curta_msd(multiplicand1 + p, &qx);
	curta_carry_add(result, &tlength, curta_sgnadd(/*r*/&rsgn, result, &tlength, /*a*/multiplicand1, px, /*b*/SGN_NEG, multiplicand1 + p, qx));
	curta_zeroise(result + tlength, q - tlength);

	/* Step 2 / result+q := beta = m2_hi - m2_lo (+/-) */
	px = p; curta_msd(multiplicand2, &px);
	qx = q; curta_msd(multiplicand2 + p, &qx);
	curta_carry_add(result + q, &tlength, curta_sgnadd(/*r*/&tsgn, result + q, &tlength, /*a*/multiplicand2 + p, qx, /*b*/SGN_NEG, multiplicand2, px));
	curta_zeroise(result + q + tlength, q - tlength);

	/* Step 3 / (sgn)temp := alpha * beta = result * result+q (+/-) */
	rsgn *= tsgn;
	curta_mulKT(temp, &tlength, result, result + q, q, result + 2 * q);

	/* Step 4 / result+2p := m1_hi * m2_hi (+) */
	curta_mulKT(result + p2, &rlength, multiplicand1 + p, multiplicand2 + p, q, result);

	/* Step 5 / (sgn)temp += result+2p = m1_hi * m2_hi (+/-) */
	curta_carry_add(temp, &tlength, curta_sgnadd(/*r*/&rsgn, temp, &tlength, /*a*/result + p2, rlength, /*b*/rsgn, temp, tlength));

	/* Step 6+7 / result+p += temp = (alpha * beta) + (m1_hi * m2_hi) (+/-) */
	/* Step 6 / copy */
	curta_zeroise(result + p, p);
	/* Step 7 / add */
	if (rlength) { // first p digits is set to zero, so we add up iff result + p2 is non-zero
		rlength += p;
	}
	curta_carry_add(result + p, &rlength, curta_sgnadd(/*r*/&rsgn, result + p, &rlength, /*a*/result + p, rlength, /*b*/rsgn, temp, tlength));

	/* Step 8 / temp := m1_lo * m2_lo (+) */
	digit backup = DIGIT_ZERO;
	if (ODD(p)) {
	  backup = *(result + p);
	}
	curta_mulKT(temp, &tlength, multiplicand1, multiplicand2, p, result);
	if (ODD(p)) {
	  *(result + p) = backup;
	}

	/* Step 11 */
	curta_carry_add(result + p, &rlength, curta_sgnadd(/*r*/&tsgn, result + p, &rlength, /*a*/temp, tlength, /*b*/rsgn, result + p, rlength));

	/* Step 9+10 / result += temp = (m1_lo * m2_lo) / m1_lo * m2_lo >= 0, so copy + add should suffice */
	/* Step 9 / copy */
	curta_copy(result, temp, p);
	/* Step 10 / add */
	curta_carry_add(result + p, &px, curta_add(/*r*/result + p, &px, /*a*/result + p, rlength, /*b*/temp + p, (tlength <= p) ? INDEX_ZERO : (tlength - p)));
	if (rlength) {
		rlength += p;
	}
	rlength = MAX(rlength, px ? p + px : tlength);

	*length = rlength;

#if LOG_LEVEL >= LEVEL_DEBUG
	isMSD(result, *length);
#endif
}

/* Basecase Inplace Division (quotient := dividendreminder / divisor, dividendreminder := dividendreminder % divisor)
 *
 * Input:
 *   dividendreminder - dividend
 *   ddrlength - number of digits of dividend
 *   divisor - digits of divisor; > 0; most significant digit of divisor >= 2^DIGIT_BITS/2 i.e. divisor is normalized
 *   dsrlength - number of digits of divisor
 *   temp - additional temporary space; allocated >= dsrlength + 1
 * Output:
 *   dividendreminder - reminder
 *   ddrlength - number of digits of reminder
 *   quotient - quotient; allocated >= ddrlength - dsrlength + 1
 *   qlength - number of digits of quotient
 *
 * Algorithm:
 *   Algorithm 1.6 (BasecaseDivRem) by Brent & Zimmermann in Modern Computer Arithmetic
 *   http://www.loria.fr/~zimmerma/mca/mca-cup-0.5.9.pdf
 *   Algorithm D (Division of nonnegative integers) by Knuth in The Art of Computer Programing, volume 2
 */
void curta_divBC(digit* quotient, index* qlength, digit* dividendreminder, index* ddrlength, digit* divisor, const index dsrlength, digit* temp) {

	/* Prerequisites: */
	/* quotient has at least ddrlength - dsrlength + 1 allocated digits */
	/* temp has at least dsrlength + 1 allocated digits */

	// temp + tmplength
	index tmplength;

	/* tmplength is required in step 2 */
	/* so in step 1 it can be used for different purpose as a delta in length of dividend and divisor */
	tmplength = *ddrlength - dsrlength;

	/* Quotient's computed digit (starting from most significant) */
	quotient += tmplength;

	/* Most significat digit of dividend reminder */
	digit* ai = dividendreminder + *ddrlength;
	/* Most significant digit of divisor */
	digit* bn = divisor + dsrlength - INDEX_ONE;
	/* First dsrlength digits of dividend reminder */
	digit* reminderI = ai - (*ddrlength = dsrlength);

	/* Step 1 */
	if (curta_cmp(dividendreminder + tmplength, dsrlength, divisor, dsrlength) >= SGN_EQ) {
		curta_sub(dividendreminder + tmplength, ddrlength, dividendreminder + tmplength, dsrlength, divisor, dsrlength);
		*quotient = DIGIT_ONE;
		*qlength = tmplength + INDEX_ONE;
	} else {
		*quotient = DIGIT_ZERO;
		*qlength = tmplength;
	}

#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
	digit r;
#else
	digit c, r;
#endif

	/* Step 2 */
	while (dividendreminder < reminderI) {

		/* q := current quotient digit to calculate */
		--quotient;

		/* Dividend reminder limb for calculating current quotient */
		--reminderI;
		if (*ddrlength || *reminderI) {
			++(*ddrlength);
		}
		/* Most significant digit of dividend reminder */
		--ai;


		/*** Quotient guess ***/

		if (*ai < *bn) {
			/* Step 3 */
#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86
			divrem(*ai, *(ai - INDEX_ONE), *bn, *quotient, r);
#else
			divrem(*ai, *(ai - INDEX_ONE), *bn, &c, quotient, &r);
#endif
		} else {
			/* Step 4 */
			*quotient = DIGIT_MAX;
		}


		/*** Quotient correction (2x max.) ***/
		/*** Alternative implementation without negative dividend reminder limb ***/

		/* Step 5a */
		/* qDivident := q_i * divident */
		curta_mul1(temp, &tmplength, *quotient, divisor, dsrlength);
		/* Step 6 */
		/* tmpDividend < (qDivident << i) */
		while (curta_cmp(reminderI, *ddrlength, temp, tmplength) < SGN_EQ) {
			/* Step 7 */
			/* q_i := q_i - 1 */
			--(*quotient);
			/* Step 8 */
			/* qDivident := qDivident - divisor */
			curta_sub(temp, &tmplength, temp, tmplength, divisor, dsrlength);
		}
		/* Step 5b */
		/* tmpDividend := tmpDividend - (qDivisor << i) */
		curta_sub(reminderI, ddrlength, reminderI, *ddrlength, temp, tmplength);
	}
}
