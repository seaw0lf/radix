#include <inttypes.h>
#include <stdlib.h>
#include "number.h"
#include "../logger.h"

/***** Number management *****/

/* Allocate digits */
inline digit* digits_alloc(const index length) {
	return malloc(length * sizeof(digit));
}

/* Free digits */
inline void digits_free(digit* theDigits) {

	/* If we have nothing, we are done */
	if (theDigits == NULL)
		return;

	free(theDigits);
}

/* Print digits */
void digits_print(const digit* theDigits, const index length) {

	/* If we have nothing, we are done */
	if (theDigits == NULL) {
		trace("NULL |");
		return;
	}

	/* Print length digits */
	index i;
	for (i = INDEX_ZERO; i < length; ++i) {
		if (i && i % 50 == 0) {
			traceln("&");
		}
		tracef("%"PRIuFAST64" ", theDigits[i]);
	}
}
