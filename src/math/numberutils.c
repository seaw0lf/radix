#include <unistd.h>
#include <stdlib.h>
#include "numberutils.h"

/* Elementary operations with digits */


/** Logarithms **/

/* Tables for logB function */
#define INDEX_LOGB 6 // log_2(log_2(2^DIGIT_BITS)) = log_2(DIGIT_BITS)
static const digit logB_b[INDEX_LOGB] = { 0x2, 0xC, 0xF0, 0xFF00, 0xFFFF0000, 0xFFFFFFFF00000000, };
static const digit logB_s[INDEX_LOGB] = { 1, 2, 4, 8, 16, 32, };

/* Tables for log2 function (required for radix log, first 36 results are sufficient)
 *
 * Precomputed data using bc:
 *   scale=32; for (i = 0; i < 37; i++) { print "*", i, "* ", l(i)/l(2), ",\n" };
 */
static const double log2_fp[37] = {
	/*00*/	0,
	/*01*/	0,
	/*02*/	1,
	/*03*/	1.58496250072115618145373894394782,
	/*04*/	2,
	/*05*/	2.32192809488736234787031942948940,
	/*06*/	2.58496250072115618145373894394783,
	/*07*/	2.80735492205760410744196931723184,
	/*08*/	3,
	/*09*/	3.16992500144231236290747788789566,
	/*10*/	3.32192809488736234787031942948941,
	/*11*/	3.45943161863729725619936304672581,
	/*12*/	3.58496250072115618145373894394783,
	/*13*/	3.70043971814109216039681265425671,
	/*14*/	3.80735492205760410744196931723185,
	/*15*/	3.90689059560851852932405837343723,
	/*16*/	4,
	/*17*/	4.08746284125033940825406601081043,
	/*18*/	4.16992500144231236290747788789566,
	/*19*/	4.24792751344358549379351942290686,
	/*20*/	4.32192809488736234787031942948943,
	/*21*/	4.39231742277876028889570826117968,
	/*22*/	4.45943161863729725619936304672582,
	/*23*/	4.52356195605701287229414824416270,
	/*24*/	4.58496250072115618145373894394785,
	/*25*/	4.64385618977472469574063885897881,
	/*26*/	4.70043971814109216039681265425673,
	/*27*/	4.75488750216346854436121683184348,
	/*28*/	4.80735492205760410744196931723187,
	/*29*/	4.85798099512757212071977332462802,
	/*30*/	4.90689059560851852932405837343723,
	/*31*/	4.95419631038687520880612359917559,
	/*32*/	5,
	/*33*/	5.04439411935845343765310199067365,
	/*34*/	5.08746284125033940825406601081044,
	/*35*/	5.12928301694496645531228874672125,
	/*36*/	5.16992500144231236290747788789567,
};

/* Logarithm to the base 2
 *
 * Usage:
 *   for estimating # of digits in given radix
 * Complexity:
 *   constant
 */
inline double logX(index radix) {
	return log2_fp[radix];
}

/* Binary logarithm
 *
 * Algorithm:
 *   http://graphics.stanford.edu/~seander/bithacks.html#IntegerLog
 */
inline digit logB(digit number) {
	register digit result = DIGIT_ZERO;

	register index i = INDEX_LOGB;
	while (i--) {
		if (number & logB_b[i]) {
			number >>= logB_s[i];
			result |= logB_s[i];
		}
	}
	return result;
}

/* Returns e iff number := base^e, otherwise zero is returned */
inline digit logD(register digit base, register digit number) {

	register digit exponent;
	for (exponent = DIGIT_ONE; number > base && number % base == DIGIT_ZERO; number /= base, ++exponent)
		;

	return number == base ? exponent : DIGIT_ZERO;
}

/* Returns exponent. Base contains remainder. */
inline digit logREDUCE(const digit base, digit* number) {

	digit exponent;
	for (exponent = DIGIT_ZERO; *number > base; ++exponent)
		*number /= base;

	if (*number == base) {
		*number = DIGIT_ZERO;
		exponent++;
	}
	return exponent;
}


/** Exponentiation **/

/*
 * base 02-02  64=2^6
 *      03-04  32=2^5
 *      05-16  16=2^4
 *      17-36   8=2^3
 */
/* Returns number of digits in base that can fit into internal data type digit */
/* Returns e iff a_0*base^0 + a_1*base^1 + ... + a_{e-1}*base^{e-1} <= maximum value storable into data type digit */
inline digit digits2ibase(digit base) {
	return DIGIT_ONE << logB(DIGIT_BITS / logX(base));
}

/* Returns { base^0, base^1, ..., base^{power - 1}} */
inline void exps(digit baseExps[], const digit power, const digit base) {

	/* srcBase^0 */
	index i = INDEX_ZERO;
	baseExps[i] = DIGIT_ONE;

	/* srcBase^i, for i in <1, power - 1> */
	while (++i < power) {
		baseExps[i] = baseExps[i - 1] * base;
	}
}

/* Returns base^power */
inline digit power(digit base, digit power) {

	digit result;
	for (result = base; --power; result *= base)
		;

	return result;
}

#if CORE_IMPL == CORE_ASM_x64 || CORE_IMPL == CORE_ASM_x86

/** Defined as macro in header **/

#else

#define DIGIT_HALF_MAX (DIGIT_MAX>>(DIGIT_BITS>>1))

#define SETHI(X) (((digit) X)<<32)
#define HI(X) ((X)>>32)
#define LO(X) ((X) & DIGIT_HALF_MAX)
#define HILO(X,Y) (SETHI(X)+(Y))

/** Addition **/

inline void add(const digit addend1, const digit addend2, register digit* carry, digit* result) {

	/* Temporary variable for storing subresults */
	digit tmp;

	/* A' := B + CARRY */
	tmp = addend2 + *carry;
	*carry = (tmp < *carry);

	/* X := A + A' */
	*result = addend1 + tmp;
	if (*result < tmp) {
		(*carry)++;
	}
}

/** Subtraction **/

inline void sub(const digit minuend1, const digit minuend2, register digit* borrow, digit* result) {

	/* Temporary variable for storing subresults */
	digit tmp;

	/* B' := B + BORROW */
	tmp = minuend2 + *borrow;
	*borrow = (tmp < *borrow);

	/* X := A - B' */
	tmp = minuend1 - tmp;
	if (tmp > minuend1) {
		(*borrow)++;
	}
	*result = tmp;
}

/** Multiplication **/

inline void mul(const digit multiplicand1, const digit multiplicand2, const digit subresult, digit* carry, digit* result) {

	/* Temporary variables for storing subresults */
	//register digit c11, c00, a0;
	//digit a1, b1, b0;
	register digit c11, c00;

	/* Multiply */
	if (multiplicand1 && multiplicand2) {

		/* Non-zero multiplier */
		register digit a1 = HI(multiplicand1);
		register digit a0 = LO(multiplicand1);
		register digit b1 = HI(multiplicand2);
		register digit b0 = LO(multiplicand2);

		c11 = a1 * b1;
		c00 = a0 * b0;


		/* reuse a0 as tmp1 */ a0 = a0 * b1;
		/* reuse a1 as tmp0 */ a1 = (a1 * b0) + a0;
		if (a1 < a0) {
			c11 += SETHI(DIGIT_ONE);
		}
		c11 += HI(a1);

		a1 = SETHI(a1);
		c00 += a1;
		if (c00 < a1) {
			c11++;
		}

	} else {

		/* Zero multiplier optimization */
		c00 = DIGIT_ZERO;
		c11 = DIGIT_ZERO;
	}

	/* X := X + CARRY */
	c00 += *carry;
	if (c00 < *carry) {
		c11++;
	}

	/* X := X + w_ij */
	c00 += subresult;
	if (c00 < subresult) {
		c11++;
	}

	/* Result */
	*result = c00;
	/* Carry */
	*carry = c11;
}

/** Division **/

/*
 * Input:
 *   dividend a (three half digits)
 *      a12 - full digit (two half digits a1 and a2)
 *      a3 - half digit
 *   dividend b (two half digits)
 *      b1, b2 - half digits
 *      b - full digit (two half digits b1 and b2)
 * Output:
 *   q(uotient) - as return value
 *   r(eminder) - as parameter by reference
 *
 * Algorithm:
 *   Algorithm 2 (divThreeHalvesByTwo) by Burnikel & Ziegler in Fast Recursive Division
 *   http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.47.565&rep=rep1&type=pdf
 */
inline static digit divide3By2HalfDigits(const digit a12, const digit a3, const digit b, const digit b1, const digit b2, digit* r) {

	/* Estimation of the quotient. */
	register digit q = a12 / b1;
	if (q > DIGIT_HALF_MAX)
		q = DIGIT_HALF_MAX;

	register digit c = a12 - (q * b1);
	register digit d = q * b2;

	if (c > DIGIT_HALF_MAX) {
		/*
		 Since c is greater than a halfdigit HILO(c, a3) would be greater than a digit.
		 Since q and b2 are halfdigits their product (d = q * b2) will never be greater than a digit.
		 So it holds that c >= d and therefore is no need to decrease the quotient.
		 */

		/*
		 Since HILO(c, a3) would be greater than a digit,
		 operation (HILO(c, a3) - d) must be performed more carefully.
		 */
		c -= HI(d);
		c = SETHI(c);
		c += a3;
		c -= LO(d);

		*r = c;
		return q;
	}

	/* c fits into one halfdigit so HILO(c, a3) fits into one digit. */
	c = HILO(c, a3);

	/* While the quotient is too big... */
	while (c < d) {
		/* ... decrease it by one... */
		q--;

		/* ... decrease d by divisor b. */
		if (d >= b) {
			/* If d - b would by nonnegative, it's straightforward. */
			d -= b;
		} else {
			/*
			 If d - b would by negative, d (= d - b) would by negative.
			 c is nonnegative hence it holds that c > d and so no further decrement of the quotient is needed.
			 Operations d = d - b; r = c - d; turn into operations: r = c + (b - d);
			 */
			*r = c + (b - d);
			return q;
		}
	}

	*r = c - d;
	return q;
}

/*
 * Input:
 *   dividend a (two full digits)
 *      a12 - full digit
 *      a34 - full digit
 *   dividend b (full digit)
 *      b - full digit
 * Output:
 *   c(arry)    - parameter by reference
 *   q(uotient) - parameter by reference
 *   r(eminder) - parameter by reference
 *
 * Algorithm:
 *   Algorithm 1 (divTwoDigitsByOne) by Burnikel & Ziegler in Fast Recursive Division
 *   http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.47.565&rep=rep1&type=pdf
 */
inline void divrem(const digit a12, const digit a34, const digit b, digit* c, digit* q, digit* r) {

	digit aa12;
	if (a12 >= b) {
		aa12 = a12 - b;
		*c = DIGIT_ONE;
	} else {
		aa12 = a12;
		*c = DIGIT_ZERO;
	}

	if (aa12 == DIGIT_ZERO) {
		if (a34 == DIGIT_ZERO) {
			/* If dividend is zero (both his digits are zero) quotient and reminder must be zero. */
			*q = *r = DIGIT_ZERO;
		} else {
			/*
			 If dividend is one digit number (most significant digit is zero)
			 quotient and reminder can be easily calculated using C's operands division and modulo
			 */
			*q = a34 / b;
			*r = a34 % b;
		}
		return;
	}

	digit a3 = HI(a34);
	digit a4 = LO(a34);
	digit b1 = HI(b);
	digit b2 = LO(b);

	digit r12;
	digit q1 = divide3By2HalfDigits(aa12, a3, b, b1, b2, &r12);

	digit s12;
	digit q2 = divide3By2HalfDigits(r12, a4, b, b1, b2, &s12);

	/* Set carry.
	 *c = HI(q1);
	 */
	/* Set quotient. */
	*q = HILO(q1, q2);
	/* Set reminder. */
	*r = s12;
}

#endif
