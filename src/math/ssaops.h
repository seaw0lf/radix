#ifndef SSAOPS_H_
#define SSAOPS_H_

#include "ssa.h"
#include "numberutils.h"


void ssa_div(digit* quotient, index* qlength, digit* dividendreminder, index* ddrlength, digit* divisor, const index dsrlength, digit* temp);

void ssa_divBT(digit* quotient, index* qlength, digit* dividendreminder, index* ddrlength, digit* divisor, const index dsrlength, const digit* inverse, const index ilength, digit* temp);
void ssa_divNT(digit* inverse, index* ilength, digit* divisor, const index dsrlength, digit* temp);

#endif /* SSAOPS_H_ */
