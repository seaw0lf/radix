#include <inttypes.h>
#include <stdlib.h>
#include <stdbool.h>
#include "ssa.h"
#include "fft.h"
#include "numberops.h"
#include "numberutils.h"
#include "../logger.h"

#define SSA_ROCHE true

typedef struct {
	/* SSA depth */
	index depth;

	/* k */
	index k;
	/* # of limbs (2^k) */
	index K;
	/* # of digits in one limb */
	index M;
	/* 2M */
	index M2;
	/* total # of original digits (padded with zeroes) */
	index MK;

	/* # of digits of a weighted limb entering SSA's FFT */
	index digits; // TODO premenovat napr na "d"
	/* total # of digits entering SSA's FFT */
	index Kdigits; // TODO premenovat na "Kd"

	/* Weight */
	index theta;
	/* represents modulo 2^N + 1 */
	index N;
	/* digits of 2^N + 1 */
	digit* Ndigits;

#ifdef SSA_ROCHE
	/* Temporary space for SSA's FFT using Roche trick */
	digit* temp_; // size = K * digits = K * (N + 1)
	digit* temp_mul; // size = 2 * digits = 2 * (N + 1)
#else
	/* Temporary space for SSA's FFT */
	digit* temp;
#endif

} SSAMem;

// Allocated space has to be >= m + (m % 2) for Karatsuba's multiplication.
// Since this this multiplication can be used for input of maximum size of TRESHOLD_MUL_SCHONHAGE_STRASSEN, m = TRESHOLD_MUL_SCHONHAGE_STRASSEN.
static digit defaultMem[TRESHOLD_MUL_SCHONHAGE_STRASSEN + ODD(TRESHOLD_MUL_SCHONHAGE_STRASSEN)];


/* Allocate SSA shared memory block */
inline static SSAMem* allocMemBlock(index nlength) {

	/* Allocate memory block holder */
	SSAMem* mem = malloc(sizeof(SSAMem));

	/* Return nothing on error */
	if (mem == NULL)
		return NULL;

	/* Initialize members */
#ifdef SSA_ROCHE
		mem->temp_ = NULL;
		mem->temp_mul = NULL;
#else
		mem->temp = NULL;
#endif

		if (nlength) {

			mem->Ndigits = digits_alloc(nlength);

			/* On error... */
			if (mem->Ndigits == NULL) {
				/* ... destroy shared memory block */
				free(mem);
				/* ... return nothing */
				return NULL;
			}
		} else {

			mem->Ndigits = NULL;
		}

	return mem;
}

/* Free SSA shared memory block */
inline static void freeMemBlock(SSAMem* mem) {

	/* If we have nothing, we are done */
	if (mem == NULL)
		return;

	/* Free memory block */
	digits_free(mem->Ndigits);
#ifdef SSA_ROCHE
	digits_free(mem->temp_);
	digits_free(mem->temp_mul);
#else
	digits_free(mem->temp);
#endif
	free(mem);
}


/* Cut off with some fine tuning */
inline static bool useSSA(index length) {
//#ifdef SSA_ROCHE
//	return length >= 64; //length >= 15;
//#endif

	/* Fine tuning */
	if (length < 7698)
		return false;
	if (length < 7936)
		return true;
	if (length < 12320)
		return false;
	if (length < 15872)
		return true;
	if (length < 22670)
		return false;
	if (length < 32256)
		return true;

	/* Cut off */
	if (length < TRESHOLD_MUL_SCHONHAGE_STRASSEN)
		return false;
	return true;
}

/* Schonhage-Strassen Algorithm Memory Allocation & Coefficient Chooser
 *
 * Input:
 *   length - number of digits of multiplicand entering the SSA
 * Output:
 *   mem - allocated memory & appropriate coefficients for the SSA of given multiplicand length
 *
 * Algorithm:
 *   Algorithm 2.4 (FFTMulMod) & Comments by Brent & Zimmermann in Modern Computer Arithmetic
 *   http://www.loria.fr/~zimmerma/mca/mca-cup-0.5.9.pdf
 */
inline static SSAMem* allocSharedMem(const index length, const index depth) {

	if (!useSSA(length)) {
		return NULL;
	}

	/* k, M estimation */

	/* Compute number of length's bits */
	index k = logB(length) + 1;
	/* Divide in two halves */
	k = CEIL(k, 2);
	/* The lower part is to be the # of numbers to by transformed using FFT since it is required to be a power of two */
	index K = 1 << k; // K := 2^k

	/* The higher part is to be the # of digits in one number to be transformed using FFT */
	/* CEIL(length / K) */
	index M = (length >> k) + (length % K > 0);

	tracefln("%"PRIuFAST64" (K) limbs of %"PRIuFAST64" (M) digits can hold %"PRIuFAST64" (N) digit number", K, M, M * K);

	/* Step 3 */

	/* N = 2*length / K + k, where N must be a multiple of K */
	index N = 2 * M + k;
	tracefln("n' = 2n/K+k = 2*%"PRIuFAST64"/%"PRIuFAST64"+%"PRIuFAST64" = %"PRIuFAST64, M * K, K, k, N);

	/* theta := ceil(N / K) */
	index theta = CEIL(N, K);
	/* N := K * theta */
	N = K * theta;

	tracefln("n' = %"PRIuFAST64", theta = 2^%"PRIuFAST64, N, theta);

	/* Number of digits required to hold number modulo 2^N + 1
	 * Additional digit is required for the value 2^N */
	index digits = N + INDEX_ONE;

	/* Allocate required memory */
	SSAMem* mem = allocMemBlock(digits);

	/* Return nothing on error */
	if (mem == NULL) {
		return NULL;
	}

	mem->depth = depth;
	mem->k = k;
	mem->K = K;
	mem->M = M;
	mem->M2 = M << 1;
	mem->MK = K * M;
	mem->N = N;
	mem->theta = theta;
	mem->digits = digits;
	mem->Kdigits = K * digits;
#ifdef SSA_ROCHE
	// Using this alternative we save (K + 1) * (N + 1) words
	mem->temp_ = digits_alloc(mem->Kdigits); // pre koeficienty a_i, b_i resp. c_i
	mem->temp_mul = digits_alloc(2 * mem->digits); // pre sucin a_i * b_i
#else
	mem->temp = digits_alloc((2 * K + 3) * mem->digits);
#endif

	// TODO doriesit stav ked temp sa nepodari naalokovat

	/* Number 2^n + 1 is required only if input length wasn't doubled */
	if (mem->depth) {
		curta_zeroise(mem->Ndigits, mem->N);
		*mem->Ndigits = DIGIT_ONE;
		*(mem->Ndigits + mem->N) = DIGIT_ONE;
	}

	errorf("#%"PRIuFAST64" (k=%"PRIuFAST64",m=%"PRIuFAST64") -> %"PRIuFAST64" + %"PRIuFAST64"\n", length, K, M, mem->Kdigits, 2 * mem->digits);

	return mem;
}

/* SSA Input Inflate Mechanism (Step 1, 2, 4 and 5 out of the box)
 *
 * Input:
 *   source - digits to be inflated
 *   slength - length of digits to be inflated
 *   theta - the weight
 *   K - # of limbs
 *   M - # of digits in one limb
 *   N - # of digits in one inflated limb
 * Output:
 *   destination - inflated digits
 */
void inflate____simple(digit* destination, const digit* source, const index* slength, index theta, index K, index M, index N) {
	/* We use N + 1 in this function (the number of digits in a limb) */
	N++;

	/* Terminator is reduced by M since we want easily zeroise the rest of destination */
	const digit* terminator = source + *slength - M;
	digit* dst = destination;

	/* a_j := a_j * theta^j mod (2^N + 1) actually means to insert fixed size of zeroes between a_j and a_(j + 1)
	 * The only hard thing is to deal with the modulo.
	 *
	 * But since length is chosen to be twice as long as the input (see allocSharedMem)
	 * because we want to have the product not affected by modulo it holds that a_j = 0, for j in <K/2, K - 1>.
	 *
	 * Now the question if a_j * theta^j <= 2^N is reduced to question if a_(K/2 - 1) * theta^(K/2 - 1) <= 2^N.
	 *   The answer is positive because
	 *     a_(K/2 - 1) * theta^(K/2 - 1) <= 2^M * 2^((N/K) * (K/2 - 1)) <= 2^N iff M + (N/K) * (K/2 - 1) <= N iff M*K <= N*K/2 + N
	 *   and from Step 3 we know that 2*M*K/K + k <= N, so
	 *     M*K <= N*K/2 - k*K/2
	 *   and from this we can definitely say that M*K <= N*K/2 - k*K/2 <= N*K/2 <= N*K/2 + N
     *
	 * Therefore we can omit the modulo.
	 */
	while (source < terminator) {

		curta_copy(destination, source, M);
		destination += M;
		curta_zeroise(destination, N - M + theta);
		destination += N - M + theta;

		source += M;
	}
	/* Do the rest in one turn */
	index j = terminator + M - source;
	curta_copy(destination, source, j);
	destination += j;
	curta_zeroise(destination, (dst + K * N) - destination);
}

/* SSA Input Inflate Mechanism (Step 1, 2, 4 and 5)
 *
 * Input:
 *   source - digits to be inflated
 *   slength - length of digits to be inflated
 *   theta - the weight
 *   K - # of limbs
 *   M - # of digits in one limb
 *   N - # of digits in one inflated limb
 * Output:
 *   destination - inflated digits
 */
void inflate____(digit* destination, const digit* source, index slength, SSAMem* stemp) {

	/* Terminator is reduced by M since we want easily zeroise the rest of destination */
	digit* dst = destination;

	index ix;

	index th = 0;
	for (index k = stemp->K; k--; source += stemp->M, destination += stemp->digits, th += stemp->theta) {

		/* Skopirujeme M cifier */
		curta_copy(destination + th, source, ix = k ? MIN(stemp->M, slength) : slength);
		slength -= ix;
		/* theta^j */
		curta_zeroise(destination, th);
		ix += th;

		if (ix <= stemp->N) {
			curta_zeroise(destination + ix, stemp->digits - ix);
		} else {
			curta_mod_rdc(destination, destination, ix, stemp->N);
		}
	}
	/* Do the rest in one turn */
	curta_zeroise(destination, (dst + stemp->Kdigits) - destination);
}

void eval____(digit* destination, const index dsizeof, const digit* source, const index slength, const index ssizeof, index scount, const index omega) {

	const digit* sTerminator = source + slength;
	index dlength;

	while (scount--) {

		dlength = MIN(source + ssizeof, sTerminator) - source;

		curta_carry_add(destination, &dlength, curta_add(destination, &dlength, destination, ssizeof, source, dlength));

		source += ssizeof;
		destination += omega;
	}

	// TODO netreba rdc na koniec?
}

/*
 *
 * Input:
 *   destination - number of digits of multiplicand entering the SSA
 *   dsizeof - ; value >= 2
 *   source - number of digits of multiplicand entering the SSA
 *   ssizeof - ; value >= 1
 *   scount -
 *   fcount - number of subsequences to fold (not number of elements in a subsequence); fcount exactly divides scount
 *   omega - represents (2^DIGIT_BITS)^i
 *   temp - temporary space for folding; allocated >= 2 * dsizeof
 */
void fold____(digit* destination, const index dsizeof, const digit* source, const index slength, const index ssizeof, index scount, index fcount, const index omega, digit* temp) {

	/* Example (powers of omega are ommited due to readibility):
	 *
	 * ssizeof = 1; scount = 16, fcount = 2
	 * source sequence = (0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610)
	 * folded sequence = (0, 1, 1, 2, 3, 5, 8, 13)
	 *                 + (21, 34, 55, 89, 144, 233, 377, 610)
	 *                 = (0 + 21, 1 + 34, 1 + 55, 2 + 89, 3 + 144, 5 + 233, 8 + 377, 13 + 610)
	 *                 = (21, 35, 56, 91, 147, 238, 385, 623)
	 *
	 * ssizeof = 1; scount = 16, fcount = 4
	 * source sequence = (0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610)
	 * folded sequence = (0, 1, 1, 2)
	 *                 + (3, 5, 8, 13)
	 *                 + (21, 34, 55, 89)
	 *                 + (144, 233, 377, 610)
	 *                 = (0 + 3 + 21 + 144, 1 + 5 + 34 + 233, 1 + 8 + 55 + 377, 2 + 13 + 89 + 610)
	 *                 = (168, 273, 441, 714)
	 */

	/* Since we require number count of each sequence to fold, we recompute fcount variable */
	fcount = scount / fcount;

	/* Example: scount = 16, fcount = 4
	 *
	 * 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, XX, XX, ...
	 * ^           ^                                                 ^
	 * source      fsTerminator                                      sTerminator
	 */
	const digit* sTerminator = source + ssizeof * scount;
	const digit* fsTerminator = source + ssizeof * fcount;

	const digit* fTerminator = source + slength;
	index ssizeofMAX;

	index thetaMOD = INDEX_ZERO;
	const index N = dsizeof - INDEX_ONE;
	const index N2 = N << 1;

	/* Clean temporary space */
	digit* shtemp = temp + dsizeof;

	const digit* src = source;
	index tht = thetaMOD;

	while (src < fsTerminator) {

		while (source < sTerminator) {

			if (source < fTerminator) {

				// TODO optimalizacia: ak ssizeof + thetaMOD <= N, tak staci skopirovat do temp/destination 0^{N+1-ssizeof-thetaMOD}||source_limb||0^{thetaMOD}
				ssizeofMAX = MIN(source + ssizeof, fTerminator) - source; // source + ssizeof <= fTerminator ? ssizeof : (fTerminator < source ? INDEX_ZERO : (fTerminator - source));
				curta_copy(temp, source, ssizeofMAX);
				curta_zeroise(temp + ssizeofMAX, dsizeof - ssizeofMAX);
				if (thetaMOD < N) {
					curta_mod_shl(temp, temp, thetaMOD, N, shtemp);
				} else {
					curta_mod_shr(temp, temp, N2 - thetaMOD, N, shtemp);
				}
				// TODO optimalizacia: nebude rychlejsie vytiahnut pred while aby sa if nerobil v kazdej iteracii? copy je vzdy len raz na zaciatku
				if (source == src) {
					curta_copy(destination, temp, dsizeof);
				} else {
					// TODO optimalizacia: tu staci obycajny add_... na konci len rdc_ treba raz spravit
					curta_mod_add(destination, destination, temp, N);
				}

			} else {
				if (source == src) {
					curta_zeroise(destination, dsizeof);
				}
			}

			source += ssizeof * fcount;

			thetaMOD += omega * fcount;
			thetaMOD %= N2;
		}

		source = (src += ssizeof);
		destination += dsizeof;

		thetaMOD = (tht += omega);
		thetaMOD %= N2;
	}
}

#ifdef SSA_ROCHE
void mulSSA__correction(digit* cj, index j,  cmpres* rsgn, digit* result, index* rlength,  SSAMem* stemp) {

	// Step 11
	curta_mod_shr(cj, cj, stemp->theta * (j - 1), stemp->N, /*tmp*/stemp->temp_mul);
	curta_mod_shrb(cj, cj, stemp->k, stemp->N);

	// Step 12-13
	index tlength = stemp->digits;
	curta_msd(cj, &tlength);

	index ix = tlength <= stemp->M2 ? INDEX_ZERO : (tlength - stemp->M2);

	/* c_j >= (j + 1) * 2^{2*M} */
	if (curta_cmp(cj + stemp->M2, ix, &j, INDEX_ONE) >= SGN_EQ) {
		/* c_j := c_j - tmp1 = c_j - (2^n' + 1) */
		curta_carry_add(cj, &tlength, curta_sgnadd(rsgn, cj, &tlength, cj, tlength, SGN_NEG, stemp->Ndigits, stemp->digits));
	} else {
		*rsgn = SGN_POS;
	}

	if (cj == result) {
		*rlength = tlength;
		return;
	}

	// Step 14
	//handle_carry_add(result, rlength, sgnadd__(rsgn, result, rlength, result, *rlength, *rsgn, cj, tlength));

	// We want numbers in stemp->temp_ (corrected vector c)
	//   AAAAAAAA BBBBBBBB CCCCCCCC DDDDDDDD
	// combine to (M = 3)
	//   000000000DDDDDDDD
	//   000000CCCCCCCC
	//   000BBBBBBBB
	//   AAAAAAAA
	//
	// When combinig i.e.
	//   000BBBBBBBB
	//   AAAAAAAA
	// we have to consider addition and subtraction separately.

	ix = tlength <= stemp->M ? INDEX_ZERO : (tlength - stemp->M);

	if (*rsgn == SGN_POS) {
		// Addition:
		// 1. Add AAAAA with BBBBBBBB
		curta_carry_add(result + stemp->M, rlength, curta_add(result + stemp->M, rlength, result + stemp->M, *rlength, cj + stemp->M, ix));
		// 2. Copy AAA
		curta_copy(result, cj, stemp->M);
		if (*rlength) {
			*rlength += stemp->M;
		} else {
			*rlength += MIN(tlength, stemp->M);
		}
	}

	if (*rsgn == SGN_NEG) {
		// Subtraction:
		// 1. Subtract AAAAA from BBBBBBBB
		curta_sub(result + stemp->M, rlength, result + stemp->M, *rlength, cj + stemp->M, ix);
		// 2. Negate AAA and add 1, subtract 1 from result in step 1.
		ix = stemp->M;
		curta_msd(cj, &ix);
		if (!curta_z(cj, ix)) {
			curta_sub1(result + stemp->M, rlength, result + stemp->M, *rlength, DIGIT_ONE);
			curta_com(result, cj, stemp->M);
			curta_carry_add(result, &ix, curta_add1(result, &ix, result, ix, DIGIT_ONE));
		}
		if (*rlength) {
			*rlength += stemp->M;
		} else {
			*rlength += MIN(ix, stemp->M);
		}
	}
}
#endif

void ssa_mul(digit* result, index* length, const digit* multiplicand1, const digit* multiplicand2, const index* mlength, ListItem* temp);

#ifdef SSA_ROCHE
void mulSSA__Roche(digit* result, index* length, const digit* multiplicand1, const digit* multiplicand2, const index* mlength, ListItem* temp) {

	SSAMem* stemp = temp->data;

	digit *temp_, *temp_half;
	index ix;
	index khalf = stemp->k;
	index Khalf = stemp->K;

	//test: op |= fold____(temp_, 2/*stemp->digits*/, multiplicand1, 1/*stemp->M*/, 16/*Khalf*/, 4, 3 * stemp->theta, stemp->temp_mul);
	if (stemp->depth) {

		index i = INDEX_ONE;
		while (khalf--) {
	    	Khalf >>= 1;

	    	temp_ = stemp->temp_;
			temp_half = temp_ + stemp->digits * Khalf;

			fold____(temp_, stemp->digits, multiplicand1, *mlength, stemp->M, stemp->K, 2 * i, stemp->theta + 2 * i * stemp->theta, stemp->temp_mul);
			fold____(temp_half, stemp->digits, multiplicand2, *mlength, stemp->M, stemp->K, 2 * i, stemp->theta + 2 * i * stemp->theta, stemp->temp_mul);

			fFFT(temp_, khalf, stemp->digits, 2 * stemp->theta * 2 * i, stemp->N, /*tmp*/stemp->temp_mul);
			fFFT(temp_half, khalf, stemp->digits, 2 * stemp->theta * 2 * i, stemp->N, /*tmp*/stemp->temp_mul);

			for (index j = Khalf; j--; temp_ += stemp->digits, temp_half += stemp->digits) {
				/* Multiply */
				ssa_mul(/*r*/stemp->temp_mul, &ix, /*m1*/temp_, /*m2*/temp_half, &stemp->digits, /*tmp*/temp->next);
				/* Reduce */
				curta_mod_rdc(temp_half, stemp->temp_mul, ix, stemp->N);
			}

			i <<= 1;
	    }

		curta_zeroise(stemp->temp_mul, stemp->digits << 1);
		eval____(stemp->temp_mul, stemp->digits, multiplicand1, *mlength, stemp->M, stemp->K, stemp->theta);
		curta_mod_rdc(stemp->temp_, stemp->temp_mul, stemp->digits << 1, stemp->N);
		curta_zeroise(stemp->temp_mul, stemp->digits << 1);
		eval____(stemp->temp_mul, stemp->digits, multiplicand2, *mlength, stemp->M, stemp->K, stemp->theta);
		curta_mod_rdc(result, stemp->temp_mul, stemp->digits << 1, stemp->N);

		/* Multiply */
		ssa_mul(/*r*/stemp->temp_mul, &ix, /*m1*/stemp->temp_, /*m2*/result, &stemp->digits, /*tmp*/temp->next);
		/* Reduce */
		curta_mod_rdc(stemp->temp_, stemp->temp_mul, ix, stemp->N);
	} else {

		index i = INDEX_ONE;
		while (khalf--) {
	    	Khalf >>= 1;

	    	temp_ = stemp->temp_;
			temp_half = temp_ + stemp->digits * Khalf;

			fold____(temp_, stemp->digits, multiplicand1, *mlength, stemp->M, stemp->K >> 1, i, stemp->theta + 2 * i * stemp->theta, stemp->temp_mul);
			fold____(temp_half, stemp->digits, multiplicand2, *mlength, stemp->M, stemp->K >> 1, i, stemp->theta + 2 * i * stemp->theta, stemp->temp_mul);

			fFFT(temp_, khalf, stemp->digits, 2 * stemp->theta * 2 * i, stemp->N, /*tmp*/stemp->temp_mul);
			fFFT(temp_half, khalf, stemp->digits, 2 * stemp->theta * 2 * i, stemp->N, /*tmp*/stemp->temp_mul);

			for (index j = Khalf; j--; temp_ += stemp->digits, temp_half += stemp->digits) {
				/* Multiply */
				ssa_mul(/*r*/stemp->temp_mul, &ix, /*m1*/temp_, /*m2*/temp_half, &stemp->digits, /*tmp*/temp->next);
				/* Reduce */
				curta_mod_rdc(temp_half, stemp->temp_mul, ix, stemp->N);
			}

			i <<= 1;
	    }

		curta_zeroise(stemp->temp_, stemp->digits);
		eval____(stemp->temp_, stemp->digits, multiplicand1, *mlength, stemp->M, stemp->K >> 1, stemp->theta);
		curta_zeroise(result, stemp->digits);
		eval____(result, stemp->digits, multiplicand2, *mlength, stemp->M, stemp->K >> 1, stemp->theta);

		/* Multiply */
		ssa_mul(/*r*/stemp->temp_mul, &ix, /*m1*/stemp->temp_, /*m2*/result, &stemp->digits, /*tmp*/temp->next);
		/* Reduce */
		curta_mod_rdc(stemp->temp_, stemp->temp_mul, ix, stemp->N);
	}

	bFFT(stemp->temp_, stemp->k, stemp->digits, 2 * stemp->theta, stemp->N, /*tmp*/stemp->temp_mul);

	if (stemp->depth) {
		traceln("\nStep 10-14:");

		*length = INDEX_ZERO;

		/* Since tmp1 has K * (n' + 1) allocated digits. We require K*M + (n' + 1 - M) digits (since we put n' + 1 digits per each M digits). From Step 3 we have n' >= 2*M + k so it's OK. */

		cmpres tsgn;

		/* working...*
		zeroise__(result, stemp->MK + (stemp->digits - stemp->K));

		temp_ = stemp->temp_ + stemp->Kdigits;
		temp_half = result + stemp->MK;

		tsgn = SGN_EQ;

		for (index j = stemp->K; j; j--) {
			temp_ -= stemp->digits;
			temp_half -= stemp->M;

			mulSSA__correction(temp_, j, &tsgn, temp_half, length, stemp);
		}

		if (*length > stemp->MK) {
			rdc___(temp_half, temp_half, *length, stemp->MK);
			*length = stemp->MK + INDEX_ONE;
			msd__(temp_half, length);
		} else {
			zeroise__(result + *length, (stemp->MK - *length) + INDEX_ONE);
		}
		op |= copy__(result, temp_half, *length);
		**/

		/**/
		temp_ = stemp->temp_ + stemp->Kdigits;
		temp_half = stemp->temp_ + stemp->Kdigits - stemp->digits + stemp->M;

		tsgn = SGN_EQ;

		for (index j = stemp->K; j; j--) {
			temp_ -= stemp->digits;
			temp_half -= stemp->M;

			mulSSA__correction(temp_, j, &tsgn, temp_half, length, stemp);
		}

		if (*length > stemp->MK) {
			curta_mod_rdc(result, temp_half, *length, stemp->MK);
			*length = stemp->MK + INDEX_ONE;
			curta_msd(result, length);
		} else {
			curta_copy(result, temp_half, *length);
			curta_zeroise(result + *length, (stemp->MK - *length) + INDEX_ONE);
		}
		/**/

	} else {

		traceln("\nStep 10:");
    	temp_ = stemp->temp_;
		for (index j = INDEX_ZERO; j < stemp->K; j++, temp_ += stemp->digits) {
			curta_mod_shr(temp_, temp_, stemp->theta * j, stemp->N, /*tmp*/stemp->temp_mul);
			curta_shr(temp_, &ix, temp_, stemp->digits, stemp->k);
		}

		traceln("\nStep 12, 13:");
		/* These steps are not actually required (see Observation). */

		traceln("\nStep 14:");
    	temp_ = stemp->temp_;
    	temp_half = result;
    	/* working *
		op |= zeroise__(temp_half, *length = stemp->MK);
		for (index j = stemp->K; j--; temp_ += stemp->digits, temp_half += stemp->M) {
			op |= add__(temp_half, &ix, temp_half, stemp->digits, temp_, stemp->digits);
		}
		msd__(result, length);
		**/

		/* new */
    	*length = INDEX_ZERO;
		Khalf = stemp->K - ((*mlength << 1) / stemp->M);
		for (index j = stemp->K, khalf = INDEX_ZERO; j--; temp_ += stemp->digits, temp_half += stemp->M, khalf = (khalf < stemp->M) ? INDEX_ZERO : (khalf - stemp->M)) {

			/* c_j */
			ix = stemp->digits;
			curta_msd(temp_, &ix);

			/* Initialize result digits iff result is shorter than c_j */
			if (khalf < ix) {
				curta_zeroise(temp_half + khalf, ix - khalf);  // TODO tu by sa mala pocistit este dalsia cifra... ale iba ked add__ skonci s carry co treraz este nevieme!!! treba upravit filozofiu funkcie add__!
			}

			curta_carry_set(temp_half, &khalf, curta_add(temp_half, &khalf, temp_half, khalf, temp_, ix));
			// TODO pozor!!! ked sucet ma carry, tak treba pocistit dalsiu cifru... teraz by to zafungovalo nekorektne!!!

			if ((Khalf < j) && (khalf < stemp->M)) {
				curta_zeroise(temp_half + khalf, stemp->M - khalf);
			}

			/* Update result length */
			if (khalf) {
				*length = (stemp->K - j - INDEX_ONE) * stemp->M + khalf;
			}
		}
		/**/
	}

	if (*length < *mlength) { /* TODO moze byt takto??? */
		curta_zeroise(result + *length, *mlength - *length);
	}
}
#else
void mulSSA__Zimmermann(digit* result, index* length, const digit* multiplicand1, const digit* multiplicand2, const index* mlength, ListItem* temp) {

	index ix;

	SSAMem* stemp = temp->data;

	/* Temp. memory organization (2K + 3):
	 * stemp := K*digits - for multiplicand1 (tmp1) for c in step 7&8
	 *        + 2*digits - as additional temporary space (tmp3)
	 *        + K*digits - for multiplicand2 (tmp2)
	 *        + 1*digits - additional temporary space (tmp4) for computing b_{K-1} in step 4 & 5
	 */
	digit* tmp1 = stemp->temp; // A; size: K * stemp->digits
	digit* tmp3 = tmp1 + stemp->Kdigits; // TEMP; size: 2 * stemp->digits
	digit* tmp2 = tmp3 + 2 * stemp->digits; // B; size: K * stemp->digits

	/* On tmp4...
	 *
	 * digit* tmp4 = 1*digits = N + 1 digits
	 *
	 * Since theta^j*a_j is biggest for j=K-1 and
	 *   theta^{K-1} * a_{K-1} < theta^{K} * a_{K-1} <= theta^{K} * 2^M = (2^{n'/K})^K * 2^M = 2^{n' + M}
	 * the required space theta^j*a_j isn't bigger than 2^{n' + M}.
	 *
	 * From step 3 and input follows
	 *   n' >= 2n/K + k = 2MK/K + k = 2M + k
	 * so M <= n'/2 - k/2
	 * and therefore
	 *   n' + M <= n' + n'/2 - k/2 <= n' + n'/2
	 *
	 * So for performing a_j := theta^j*a_j we need in worst case cca N + N/2 digits.
	 * We can say that during performing step 4 & 5 we can rewrite cca N/2 digits of a_{j+1} because of the temp. memory organization.
	 * But since we are computing from j=0 to j=K-1 it's no big deal because memory for a_{j+1} is in time of computing a_j unused.
	 *
	 * The only case there is extra space needed for N/2 is for computing b_{K-1}. Therefore we also allocate tmp4 at that peculiar place.
	 */

	traceln("\nStep 3:");

	traceln("\nStep 1, 2:");
	/* Incorporated into inflate mechanism */

	traceln("\nStep 4, 5:");
	/* Incorporated into inflate mechanism */

	if (stemp->depth) {
		inflate____(tmp1, multiplicand1, *mlength, stemp);
		inflate____(tmp2, multiplicand2, *mlength, stemp);
	} else {
		inflate____simple(tmp1, multiplicand1, mlength, stemp->theta, stemp->K, stemp->M, stemp->N);
		inflate____simple(tmp2, multiplicand2, mlength, stemp->theta, stemp->K, stemp->M, stemp->N);
	}

	traceln("\nStep 6:");
	fFFT(tmp1, stemp->k, stemp->digits, 2 * stemp->theta, stemp->N, /*tmp*/tmp3);
	fFFT(tmp2, stemp->k, stemp->digits, 2 * stemp->theta, stemp->N, /*tmp*/tmp3);

	traceln("\nStep 7, 8:");
	/* Temp. memory organization:
	 * stemp := K digits FFT'ed multiplicand1 (tmp1)
	 *        + 2 digits additional temporary space (tmp3)
	 *        + K digits FFT'ed multiplicand2 (tmp2) */
	for (index j = stemp->K; j--; tmp1 += stemp->digits, tmp2 += stemp->digits, tmp3 += stemp->digits) {
		/* Multiply */
		ssa_mul(/*r*/tmp3, &ix, /*m1*/tmp1, /*m2*/tmp2, &stemp->digits, /*tmp*/temp->next);
//		if (ix < stemp->digits) {
//			op |= zeroise__(tmp3 + ix, stemp->digits - ix);
//		}
		/* Reduce */
		curta_mod_rdc(tmp3, tmp3, ix, stemp->N);
	}

	traceln("\nStep 9:");
	/* itemp := K digits FFT'ed multiplicand1 (tmp1) + K digits FFT'ed convolution of multiplicand1 and multiplicand2 (tmp3) + 2 digits additional temporary space */
	tmp3 = stemp->temp + stemp->Kdigits; // FFT(A)*FFT(B)
	tmp2 = tmp3 + stemp->Kdigits; // TEMP

	bFFT(tmp3, stemp->k, stemp->digits, 2 * stemp->theta, stemp->N, /*tmp*/tmp2);

	if (stemp->depth) {

		traceln("\nStep 10-14:");
		*length = 0;
		/* Since tmp1 has K * (n' + 1) allocated digits. We require K*M + (n' + 1 - M) digits (since we put n' + 1 digits per each M digits). From Step 3 we have n' >= 2*M + k so it's OK. */
		tmp1 = stemp->temp;
		curta_zeroise(tmp1, stemp->MK + (stemp->digits - stemp->K));
		tmp1 += stemp->MK;

		index tlength;
		cmpres tsgn;

		tmp3 += stemp->Kdigits;
		for (index j = stemp->K; j; j--) {
			tmp3 -= stemp->digits;

			// Step 11
			curta_mod_shr(tmp3, tmp3, stemp->theta * (j - 1), stemp->N, /*tmp*/tmp2);
			curta_mod_shrb(tmp3, tmp3, stemp->k, stemp->N);

			// Step 12-13
			tlength = stemp->digits;
			curta_msd(tmp3, &tlength);

			ix = tlength <= stemp->M2 ? INDEX_ZERO : (tlength - stemp->M2);

			/* c_j >= (j + 1) * 2^{2*M} */
			if (curta_cmp(tmp3 + stemp->M2, ix, &j, INDEX_ONE) >= SGN_EQ) {
				/* c_j := c_j - tmp1 = c_j - (2^n' + 1) */
				curta_carry_add(tmp3, &tlength, curta_sgnadd(&tsgn, tmp3, &tlength, tmp3, tlength, SGN_NEG, stemp->Ndigits, stemp->digits));
			} else {
				tsgn = SGN_POS;
			}

			// Step 14
			if (*length) {
				*length += stemp->M;
			}
			tmp1 -= stemp->M;
			curta_carry_add(tmp1, length, curta_sgnadd(&tsgn, tmp1, length, tmp1, *length, tsgn, tmp3, tlength));
		}

		if (*length > stemp->MK) {
			curta_mod_rdc(tmp1, tmp1, *length, stemp->MK);
			*length = stemp->MK + INDEX_ONE;
			curta_msd(tmp1, length);
		} else {
			curta_zeroise(result + *length, (stemp->MK - *length) + INDEX_ONE);
		}
		curta_copy(result, tmp1, *length);

	} else {

		traceln("\nStep 10:");
		for (index j = INDEX_ZERO; j < stemp->K; j++, tmp3 += stemp->digits) {
			curta_mod_shr(tmp3, tmp3, stemp->theta * j, stemp->N, /*tmp*/tmp2);
			curta_shr(tmp3, &ix, tmp3, stemp->digits, stemp->k);
		}

		traceln("\nStep 12, 13:");
		/* These steps are not actually required (see Observation). */

		traceln("\nStep 14:");
		tmp1 = tmp2 = stemp->temp;
		curta_zeroise(tmp2, *length = stemp->K * stemp->M + stemp->digits);
		tmp3 = tmp1 + stemp->K * stemp->digits;
		for (index j = stemp->K; j--; tmp3 += stemp->digits, tmp2 += stemp->M) {
			curta_carry_add(tmp2, &ix, curta_add(tmp2, &ix, tmp2, stemp->digits, tmp3, stemp->digits));
		}

		curta_msd(tmp1, length);
		curta_copy(result, tmp1, *length);
	}

	if (*length < *mlength) { /* TODO moze byt takto??? */
		curta_zeroise(result + *length, *mlength - *length);
	}
}
#endif

/* Schonhage-Strassen Multiplication (result := multiplicand1 * multiplicand2)
 *
 * Input:
 *   multiplicand 1 - digits of first multiplicand
 *   multiplicand 2 - digits of second multiplicand
 *   mlength - number of digits of first / second multiplicand (same length expected)
 *   temp - recursion plan & reusable temporary space
 * Output:
 *   result - product of first and second multiplicand; allocated >= 2 * mlength
 *   rlength - number of digits of resulting product
 *
 * Algorithm:
 *   Algorithm 2.4 (FFTMulMod) by Brent & Zimmermann in Modern Computer Arithmetic
 *   http://www.loria.fr/~zimmerma/mca/mca-cup-0.5.9.pdf
 *   A GMP-based Implementation of Schönhage-Strassen's Large Integer Multiplication Algorithm by Gaudry, Kruppa & Zimmermann
 *   http://www.loria.fr/~gaudry/publis/issac07.pdf
 */
void ssa_mul(digit* result, index* length, const digit* multiplicand1, const digit* multiplicand2, const index* mlength, ListItem* temp) {

	if (temp == NULL) {
		curta_mulKT(result, length, multiplicand1, multiplicand2, *mlength, /*tmp*/defaultMem);
		if (*length < *mlength) { /* TODO moze byt takto??? */
			curta_zeroise(result + *length, *mlength - *length);
		}
		return;
	}

	/* Observation:
	 * When n is required to be (or already is >=) 2*n, that is the case we won't to have A*B reduced by 2^n + 1, we can:
	 * 1) omit reduction by 2^n' + 1 in steps 4 & 5 which turns the task into inserting n' - M + theta zeroes between each a_i and a_{i+1},
	 * 2) omit steps 12 & 13.
	 *
	 * *) We have to bare in mind that in this case a_j = 0 and b_j = 0 for j from K/2 to K - 1.
	 *
	 * Add 1)
	 *  For j from K/2 to K - 1 holds theta^j * a_j = theta^j * 0 < 2^n' + 1.
	 *  For j from 0 to K/2 - 1 holds theta^j * a_j < theta^j * 2^M < theta^{K/2} * 2^M = 2^{(n'/K)*(K/2) + M} = 2^{n'/2 + M} <= 2^n' < 2^n' + 1.
	 *    Here we have to show that n'/2 + M <= n' which is equivalent to n' >= 2M. And this holds since n' is chosen so that n' >= 2M + k >= 2M.
	 *
	 * Add 2)
	 *  We have to show that 0 <= c_j < 2^n' + 1.
	 *
	 *  By the Theorem 2.3 and its proof we can write c_j as:
	 *    c_j = SUM_{l,m=0;l+m=j}^{K-1}{a_l*b_m} - SUM_{l,m=0;l+m=K+j}^{K-1}{a_l*b_m}
	 *
	 *  Using *) we can simplify c_j to:
	 *    c_j = SUM_{l,m=0;l+m=j}^{K-1}{a_l*b_m}
	 *  since at least one of a_l or b_m is zero for l+m=K+j. Now it is proven that c_j >= 0.
	 *
	 *  Using *) we can simplify c_j to:
	 *    c_j = SUM_{l,m=0;l+m=j}^{K/2-1}{a_l*b_m}
	 *  so for c_j holds
	 *    c_j < (j + 1) * 2^{2*M}
	 *  since a_l < 2^M, b_m < 2^M and SUM contains
	 *   - j + 1 terms for j from 0 to K/2 - 1,
	 *   - K - (j + 1) terms for j from K/2 to K - 1.
	 *
	 *  Now (j + 1) * 2^{2*M} <= K * 2^{2*M} = 2^k * 2^{2*M} = 2^{2*M + k} <= 2^n' < 2^n' + 1.
	 *
	 * ***
	 * Note:
	 * Since digit can hold only first 2^M, where M = DIGIT_BITS, so for i from 0 to K - 1
	 * holds that a_i < 2^M and b_i < 2^M (or in other words, digit (2^64)^n would by considered
	 * as n+1 digit, not as n digit number), therefore the proof above is sufficient.
	 *
	 * But if we want to have our observation sound with hypothesis of theorem 2.3 we have
	 * to consider situation where A or B equal to 2^n. In this case we would have to consider
	 * a_{K/2} = 1 or b_{K/2} = 1 in *). Specially, when a_{K/2} = 1 and b_{K/2} = 1, then
	 * all other coefficients a_j and b_j would be equal to zero, we would have to do correction
	 * of c_0 since for j from 1 to K-1 will simplify to
	 *    c_j = SUM_{l,m=0;l+m=j}^{K-1}{a_l*b_m}
	 * but
	 *    c_0 = a_0*b_0 - 1
	 * since a_{K/2} = 1 and b_{K/2} = 1.
	 *
	 * ***
	 * Summary:
	 * Since we work with DIGIT_BITS bits per digit, the special case where mulitiplicands
	 * equal to 2^n, we
	 * downside) will use cca twice as much memory as algorithm in MCA does :(
	 * advantage) do not have to deal with step modular reduction in step 5, step 12 and 13.
	 */

#ifdef SSA_ROCHE
	/*** Roche approach - Zimmermann approach with Roche flavor ***/
	mulSSA__Roche(result, length, multiplicand1, multiplicand2, mlength, temp);
#else
	/*** Zimmermann approach ***/
	mulSSA__Zimmermann(result, length, multiplicand1, multiplicand2, mlength, temp);
#endif
}

ListItem* ssa_mul_alloc(const index length) {

	/** Planing & memory allocation **/

	/* Pointer to first number list node */
	ListItem* temp = NULL;
	/* Pointer to last number list node */
	ListItem* lastNode = NULL;

	/* Double the input length since multiplication of L digit numbers can be 2L digits long */
	index len = length << 1;
	index dpt = INDEX_ZERO;
	while (len) {

		/* Create shared memory block */
		SSAMem* newMem = allocSharedMem(len, dpt);
		/* We don't add defaultMem to temp list since it will be freed */
		if (newMem == NULL) {
			break;
		}

		/* Create list item... */
		ListItem* newListItem = allocListItem(newMem, NULL);
		/* On error... */
		if (newListItem == NULL) {
			/* ... free new shared memory block */
			freeMemBlock(newMem);
			/* ... free list of shared memory blocks */
			ssa_mul_free(temp);
			return NULL;
		}

		if (lastNode) {
			lastNode->next = newListItem;
		} else {
			temp = newListItem;
		}
		lastNode = newListItem;

		len = newMem->N;
		dpt++;
	}

	return temp;
}

void ssa_mul_free(ListItem* temp) {
	freeList(temp, (void *) &freeMemBlock);
}
