#include "modularops.h"
#include "numberutils.h"

/* N := DIGIT_BITS * n so 2^N := (2^DIGIT_BITS)^n */

/* Is SemiNormalized Modulo 2^N + 1
 *
 * Number residue mod 2^N + 1 is considered to be seminormalized
 * if it is in form A = (a_{n+1}, a_n, ..., a_0) where a_{n+1} is carry and its greatest value can be one
 * or in other words if 0 <= A < 2*2^N.
 *
 * Why seminormalized instead of normalized?
 * Because it can be always performed in constant time.
 *
 * Input:
 *   source - digits of source
 *   slength - number of digits of source
 *   n - mod 2^N + 1
 * Output:
 *   return value - true/false iff source is seminormalized
 *
 * Idea:
 *   by Kruppa in Speeding up Integer Multiplication and Factorization
 *   tel.archives-ouvertes.fr/docs/00/47/72/10/PS/thesis.ps
 */
inline bool curta_mod_sn(const digit* source, const index slength, const index n) {
	return slength < n + 1 || (slength == n + 1 && *(source + n) <= DIGIT_ONE);
}

/* Seminormalized addition modulo 2^N + 1 (SNMresult := SNMaddend1 + SNMaddend2)
 *
 * Input:
 *   addend 1 - digits of first seminormalized addend of length n + 1
 *   addend 2 - digits of second seminormalized addend of length n + 1
 *   n - mod 2^N + 1
 * Output:
 *   result - seminormalized sum of first and second seminormalized addend modulo 2^N + 1; allocated >= N + 1
 *
 * Algorithm:
 *   by Kruppa in Speeding up Integer Multiplication and Factorization
 *   tel.archives-ouvertes.fr/docs/00/47/72/10/PS/thesis.ps
 */
void curta_mod_add(digit* result, register const digit* addend1, register const digit* addend2, const index n) {

	/* Idea:
	 *   addend1 := a_n * 2^N + a_rest; where 0 <= a_rest < 2^N; addend1 is SN by assumption, so 0 <= a_n <= 1
	 *   addend2 := b_n * 2^N + b_rest; where 0 <= b_rest < 2^N; addend2 is SN by assumption, so 0 <= b_n <= 1
	 *   result  := r_n * 2^N + r_rest; where 0 <= r_rest < 2^N
	 * then
	 *   addend1 + addend2 = a_n * 2^N + a_rest + b_n * 2^N + b_rest = (a_n * 2^N + b_n * 2^N) + (a_rest + b_rest)
	 * but
	 *   0 <= a_rest + b_rest < 2 * 2^N; so it can be written in form rr_n * 2^N + rr_rest where 0 <= rr_rest < 2^N and 0 <= rr_n <= 1
	 * so
	 *   addend1 + addend2 = (a_n * 2^N + b_n * 2^N) + (rr_n * 2^N + rr_rest) = (a_n + b_n + rr_n) * 2^N + rr_rest
	 *
	 * Now, since assumption of seminormalization holds, a_n, b_n, r_n can't be greater than one.
	 * Since we want addend1 + addend2 to be seminormalized and 0 <= c := a_n + b_n + rr_n <= 3 we have to
	 * reduce addend1 + addend2 by 2^N + 1 so that 0 <= r_n <= 1 to be the result.
	 * Since 0 <= c <= 3, the reduction has to be done 3 times at most.
	 * But is has to be done carefully because we want the result to be nonnegative (and seminormalized)!
	 *
	 * One reduction by 2^N + 1 means reduction of c by one and reduction of rr_rest by one.
	 *
	 * So the ideal case is to reduce addend1 + addend2 by 2^N + 1 c-times.
	 * We have to bare in mind that the number after reduction has to be nonnegative (and seminormalized).
	 *
	 * Since we require result to be only seminormalized we can achieve it this way:
	 * A. if rr_rest_0 (least significant digit of rr_rest) >= c we can addend1 + addend2 boldly reduce by c * (2^N + 1);
	 *    in the worst case addend1 + addend2 - c * (2^N + 1) would by zero which satisfies seminormalization requirement;
	 *    so we set r_n := c - c = 0 and reduce rr_rest by c;
	 * B. if rr_rest_0 < c we could receive a negative result after reducing by c * (2^N + 1) so for sake of simplicity
	 *    in this case we reduce addend1 + addend2 only by (c - 1) * (2^N + 1);
	 *    so we set r_n := c - (c - 1) = 1 and reduce rr_rest by (c - 1);
	 *      Let's discuss a delicate case here.
	 *      Since this is the case where rr_rest_0 < c it could easily be the special case where rr_rest < (c - 1).
	 *      In this case reducing rr_rest by (c - 1) would propagate borrow but only to r_n since r_n was set in this
	 *      case to 1 (which would be borrowed).
	 */

	index rlength;

	/* Sum addends (except carries) together using classic (non modular) addition */
	/* c := a_n + b_n + rr_n */
	digit c = *(addend1 + n) + *(addend2 + n) + curta_add(/*r*/result, &rlength, /*a*/addend1, n, /*b*/addend2, n);

	/* r_n := 0, if rr_rest_0 >= c; (A.)
	 *        1, if rr_rest_0 < c; (B.)
	 *
	 * c := c - r_n = c - 0, if rr_rest_0 >= c; (A.)
	 *      c - r_n = c - 1, if rr_rest_0 < c; (B.)
	 */
	*(result + n) = (*result < c);

	/* result := r_n * 2^N + rr_rest - c */
	rlength = n + 1;
	/* No borrow expected since (r_n < 2 < DIGIT_MAX) */
	curta_sub1(/*r*/result, &rlength, /*r*/result, rlength, /*c*/c - *(result + n));
}

/* Seminormalized subtraction modulo 2^N + 1 (SNMresult := SNMminuend1 + SNMminuend2)
 *
 * Input:
 *   minuend 1 - digits of first seminormalized minuend of length n + 1
 *   minuend 2 - digits of second seminormalized minuend of length n + 1
 *   n - mod 2^N + 1
 * Output:
 *   result - seminormalized difference of first and second seminormalized minuend modulo 2^N + 1; allocated >= N + 1
 *
 * Algorithm:
 *   by Kruppa in Speeding up Integer Multiplication and Factorization
 *   tel.archives-ouvertes.fr/docs/00/47/72/10/PS/thesis.ps
 */
void curta_mod_sub(digit* result, register const digit* minuend1, register const digit* minuend2, const index n) {

	/* Idea:
	 *   minuend1 := a_n * 2^N + a_rest; where 0 <= a_rest < 2^N; minuend1 is SN by assumption, so 0 <= a_n <= 1
	 *   minuend2 := b_n * 2^N + b_rest; where 0 <= b_rest < 2^N; minuend2 is SN by assumption, so 0 <= b_n <= 1
	 *   result   := r_n * 2^N + r_rest; where 0 <= r_rest < 2^N
	 * then
	 *   minuend1 - minuend2 = a_n * 2^N + a_rest - (b_n * 2^N + b_rest) = (a_n * 2^N - b_n * 2^N) + (a_rest - b_rest)
	 * but
	 *   -2^N < a_rest - b_rest < 2^N; so it can be written in form -rr_n * 2^N + rr_rest where 0 <= rr_rest < 2^N and 0 <= rr_n <= 1
	 *   (rr_n is borrow... borrow nothing or borrow one; it is more readable to have 0 <= rr_n <= 1, so -1 <= -rr_n <= 0)
	 * so
	 *   minuend1 - minuend2 = (a_n * 2^N - b_n * 2^N) + (-rr_n * 2^N + rr_rest) = (a_n - b_n - rr_n) * 2^N + rr_rest
	 *
	 * Now, -2 <= c := a_n + b_n + rr_n <= 1 so we have to gain minuend1 - minuend2 by 2^N + 1 so that 0 <= r_n <= 1 to be the result.
	 * Since -2 <= c <= 1, the gain has to be done 2 times at most.
	 * But is has to be done carefully because we want the result to be nonnegative and seminormalized!
	 *
	 * One gain by 2^N + 1 means gain of c by one and gain of rr_rest by one.
	 *
	 * We have to bare in mind that the number after reduction has to be nonnegative and seminormalized.
	 *
	 * Since we require result to be only seminormalized we can achieve it this way:
	 * A. if c == 1 there is nothing to be done;
	 *    so we set r_n := c and 0 <= rr_rest < 2^N holds;
	 * B. if -2 <= c < 1 the result can be negative so for sake of simplicity (case c == 0 could be treated as special)
	 *    in this case we gain minuend1 - minuend2 by (-c) * (2^N + 1);
	 *    so we set r_n := c + (-c) = 0 and gain rr_rest by (-c);
	 *      Let's discuss a delicate case here.
	 *      We could easily have rr_rest + (-c) >= 2^N.
	 *      In this case gaining rr_rest by (-c) would propagate carry but only to r_n since r_n was set in this
	 *      case to 0 (which would be carried up to 1).
	 */

	/* rr_n */
	index rlength;

	/* Subtract minuends (except carries) using classic (non modular) subtraction */
	/* Since we want a_rest - b_rest to be in form -rr_n * 2^N + rr_rest we interpret overflow during subtraction as rr_n = 1 */
	/* c := a_n - b_n - rr_n */
	digit c = *(minuend1 + n) - *(minuend2 + n) - curta_sub(/*r*/result, &rlength, /*a*/minuend1, n, /*b*/minuend2, n);

	/* r_n := 1, if c == 1; (B.)
	 *        0, if c < 1; (A.)
	 *
	 * c := r_n - c = 1 - 1 = 0, if c == 1; (B.)
	 *      r_n - c = 0 - c = -c, if c < 1; (A.)
	 */
	*(result + n) = (c == 1);

	/* result := r_n * 2^N + rr_rest + c */
	rlength = n + 1;
	/* No carry expected since (r_n < 2 < DIGIT_MAX) */
	curta_add1(/*r*/result, &rlength, /*r*/result, rlength, /*c*/*(result + n) - c);
}

/* Seminormalized shift left digits/multiplication by 2^digits modulo 2^N + 1 (SNMdestination := SNMsource << digits)
 *
 * Input:
 *   source - digits of seminormalized number to shift of length n + 1; 0 <= slength <= N + 1
 *   digits - number of digits to shift; 0 <= digits <= N
 *   temp - temporary space; allocated >= N + 1
 * Output:
 *   destination - seminormalized shifted number; allocated >= N + 1
 *
 * Idea:
 *   by Kruppa in Speeding up Integer Multiplication and Factorization
 *   tel.archives-ouvertes.fr/docs/00/47/72/10/PS/thesis.ps
 */
void curta_mod_shl(digit* destination, const digit* source, const index digits, const index n, digit* temp) {

	/* If number of digits to shift is zero... */
	if (digits == INDEX_ZERO) {
		/* ... shift means to copy digits from source to destination (but the case they're the same digits) */
		if (destination != source) {
			curta_copy(destination, source, n + 1);
		}
		return;
	}

	/* Let 0 <= i <= N. TODO tu by stacilo 0 <= i < N... dala by sa implementacia kusok zjednodusit, pricom SHL(N) := SHR(N)
	 * First, consider 0 <= X < 2^N:
	 *   X * 2^i = X[N-1:N-i] * 2^N + X[N-i-1:0] * 2^i = X[N-i-1:0] * 2^i - X[N-1:N-i] mod 2^N + 1
	 *
	 * If X[N-i-1:0] * 2^i < X[N-1:N-i] borrow will be propagated, in worst case to the (N+1)-th digit.
	 *
	 * This case can be easily solved by adding 1 to the difference (which is no longer than N digits) since:
	 *   X[N-i-1:0] * 2^i - X[N-1:N-i] = 2^N + 1 + X[N-i-1:0] * 2^i - X[N-1:N-i] = (2^N + X[N-i-1:0] * 2^i - X[N-1:N-i]) + 1 mod 2^N + 1
	 * 2^N is for the borrow propagated by difference X[N-i-1:0] * 2^i - X[N-1:N-i] so (2^N + X[N-i-1:0] * 2^i - X[N-1:N-i])
	 * is no longer than N digits. So it suffices to add one to the difference.
	 *
	 * Second, X is seminormalized, so 0 <= X[N] <= 1
	 * There's no problem with previous computation if X[N] := 1 and instead of X[N-1:N-i] we can work with X[N:N-i];
	 * since X[N-1:N-i] is subtracted as lower part the X[N] won't stick out; except of shifting by N.
	 * In that case it suffices to add X[N] to the difference, since
	 *   X * 2^2N = X * (2^N)^2 = X * (-1)^2 = X mod 2^N + 1.
	 */

	/* Number of bits that won't be carried/shifted into neighboring digit */
	const index digitsComplement = n - digits;

	/* temp := X[N:N-i] (but no more than N digits) */
	index tlength = MIN(n, digits + 1);
	curta_copy(temp, source + digitsComplement, tlength);

	/* destination := X[N-i-1:0] * 2^i; constructed as N+1 digit number */
	curta_copyb(destination + digits, source, digitsComplement);
	curta_zeroise(destination, digits);
	*(destination + n) = DIGIT_ZERO;

	/* Since source is seminormalized and shift by N means shift carry to 2N-th digit, we have to add up 0 <= carry <= 1 to the result */
	digit add = digits == n ? *(source + n) : DIGIT_ZERO;

	/* destination := destination - temp = X[N-i-1:0] * 2^i - X[N:N-i] */
	/* If borrow is propagated to the (N+1)-th digit, we have to add up 1 to the result */
	add += curta_sub(/*destination*/destination, &tlength, /*destination*/destination, n, /*temp*/temp, tlength);

	/* Perform addition */
	if (add) {
		curta_carry_add(destination, &tlength, curta_add1(/*destination*/destination, &tlength, /*destination*/destination, tlength, /*add*/add));
	}
}

/* Seminormalized shift right digits/division by 2^digits modulo 2^N + 1 (SNMdestination := SNMsource >> digits)
 *
 * Input:
 *   source - digits of seminormalized number to shift
 *   slength - number of digits of seminormalized number to shift; 0 <= slength <= N + 1
 *   digits - number of digits to shift; 0 <= digits <= N
 *   temp - temporary space; allocated >= N + 1
 * Output:
 *   destination - seminormalized shifted number; allocated >= N + 1
 *   length - number of digits of seminormalized shifted number
 */
void curta_mod_shr(digit* destination, const digit* source, const index digits, const index n, digit* temp) {

	/* If number of digits to shift is zero... */
	if (digits == INDEX_ZERO) {
		/* ... shift means to copy digits from source to destination (but the case they're the same digits) */
		if (destination != source) {
			curta_copy(destination, source, n + 1);
		}
		return;
	}

	/* Let 0 <= X <= 2*2^N (seminormalized), 0 <= i <= N:
	 *   X * 2^{-i} = X * 1 * 2^{-i} = X * (-1)^2 * 2^{-i} = X * (2^N)^2 * 2^{-i} = X * 2^{2N - i} =
	 *   = X[N:i] * 2^2N + X[i-1:0] * 2^{2N - i} = X[N:i] * 2^2N + X[i-1:0] * 2^{(N - i) + N} =
	 *   = X[N:i] * (2^N)^2 + X[i-1:0] * 2^{N - i} * 2^N = X[N:i] * (-1)^2 + X[i-1:0] * 2^{N - i} * (-1) =
	 *   = X[N:i] - X[i-1:0] * 2^{N - i} mod 2^N + 1
	 *
	 * If X[N:N-i] < X[i-1:0] borrow will be propagated, in worst case to the (N+1)-th digit.
	 *
	 * This case can be easily solved by adding 1 to the difference (which is no longer than N digits) since:
	 *   X[N:i] - X[i-1:0] * 2^{N - i} = 2^N + 1 + X[N:i] - X[i-1:0] * 2^{N - i} = (2^N + X[N:i] - X[i-1:0] * 2^{N - i}) + 1 mod 2^N + 1
	 * 2^N is for the borrow propagated by difference X[N:i] - X[i-1:0] * 2^{N - i} so (2^N + X[N:i] - X[i-1:0] * 2^{N - i})
	 * is no longer than N digits. So it suffices to add one to the difference (which may carry one to (N+1)-th digit).
	 * Result is clearly seminormalized.
	 */

	/* Number of bits that won't be carried/shifted into neighboring digit */
	const index digitsComplement = n - digits;

	/* temp := X[i-1:0] */
	curta_copy(temp, source, digits);

	/* destination := X[N:i]; constructed as N+1 digit number */
	index hlength = digitsComplement + 1;
	curta_copy(destination, source + digits, hlength);
	curta_zeroise(destination + hlength, digits);

	/* destination := destination - temp * 2^{N - i} = X[N:i] - X[i-1:0] * 2^{N - i} */
	if (curta_sub(/*destination*/destination + digitsComplement, &hlength, /*destination*/destination + digitsComplement, digits, /*temp*/temp, digits)) {
		/* If borrow is propagated to the (N+1)-th digit, we have to add up 1 to the result of N digits */
		curta_carry_add(destination, &hlength, curta_add1(/*destination*/destination, &hlength, /*destination*/destination, n, /*1*/DIGIT_ONE));
	}
}

/* Seminormalized backwards shift right digits/division by 2^digits modulo 2^N + 1 (SNMdestination := SNMsource >> digits)
 *
 * Input:
 *   source - digits of seminormalized number to shift
 *   slength - number of digits of seminormalized number to shift; 0 <= slength <= N + 1
 *   digits - number of digits to shift; 0 <= digits < DIGIT_BITS
 *   temp - temporary space; allocated >= N + 1
 * Output:
 *   destination - seminormalized shifted number; allocated >= N + 1
 *   length - number of digits of seminormalized shifted number
 */
void curta_mod_shrb(digit* destination, const digit* source, const index bits, const index n) {

	/* If number of digits to shift is zero... */
	if (bits == INDEX_ZERO) {
		/* ... shift means to copy digits from source to destination (but the case they're the same digits) */
		if (destination != source) {
			curta_copy(destination, source, n + 1);
		}
		return;
	}

	/* Let 0 <= X <= 2*2^N (seminormalized), 0 <= i <= N:
	 *   X * 2^{-i} = X * 1 * 2^{-i} = X * (-1)^2 * 2^{-i} = X * (2^N)^2 * 2^{-i} = X * 2^{2N - i} =
	 *   = X[N:i] * 2^2N + X[i-1:0] * 2^{2N - i} = X[N:i] * 2^2N + X[i-1:0] * 2^{(N - i) + N} =
	 *   = X[N:i] * (2^N)^2 + X[i-1:0] * 2^{N - i} * 2^N = X[N:i] * (-1)^2 + X[i-1:0] * 2^{N - i} * (-1) =
	 *   = X[N:i] - X[i-1:0] * 2^{N - i} mod 2^N + 1
	 *
	 * If X[N:N-i] < X[i-1:0] borrow will be propagated, in worst case to the (N+1)-th digit.
	 *
	 * This case can be easily solved by adding 1 to the difference (which is no longer than N digits) since:
	 *   X[N:i] - X[i-1:0] * 2^{N - i} = 2^N + 1 + X[N:i] - X[i-1:0] * 2^{N - i} = (2^N + X[N:i] - X[i-1:0] * 2^{N - i}) + 1 mod 2^N + 1
	 * 2^N is for the borrow propagated by difference X[N:i] - X[i-1:0] * 2^{N - i} so (2^N + X[N:i] - X[i-1:0] * 2^{N - i})
	 * is no longer than N digits. So it suffices to add one to the difference (which may carry one to (N+1)-th digit).
	 * Result is clearly seminormalized.
	 */

	digit out = *source << (DIGIT_BITS - bits);

	index hlength;
	curta_shr(destination, &hlength, source, n + 1, bits);

	if (out) {

		/* destination := destination - temp * 2^{N - i} = X[N:i] - X[i-1:0] * 2^{N - i} */
		if (curta_sub1(/*destination*/destination + n - INDEX_ONE, &hlength, /*destination*/destination + n - INDEX_ONE, /*out*/INDEX_ONE, out)) {
			/* If borrow is propagated to the (N+1)-th digit, we have to add up 1 to the result of N digits */
			curta_carry_add(destination, &hlength, curta_add1(/*destination*/destination, &hlength, /*destination*/destination, n, /*1*/DIGIT_ONE));
		}
	}
}

/* Reduce modulo 2^N + 1
 *
 * Input:
 *   source - digits of number to seminormalize; 0 <= source < 2^2N * (2^N - 1)
 *   slength - number of digits of number to seminormalize; 0 <= slength <= 3N
 * Output:
 *   destination - seminormalized number; allocated >= N + 1
 *   length - number of digits of seminormalized number
 */
void curta_mod_rdc(digit* destination, const digit* source, const index slength, const index n) {
	/* Let
	 *   source := x_2 * 2^2N + x_1 * 2^N + x_0; where 0 <= x_0, x_1 < 2^N, 0 <= x_2 < 2^N - 1 (since 0 <= source < 2^2N * (2^N - 1))
	 *
	 * Now
	 *   destination = x_2 * (-1)^2 + x_1 * (-1) + x_0 = x_0 - x_1 + x_2 mod 2^N + 1
	 *
	 * If x_0 >= x_1, then
	 *   0 <= x_0 - x_1 < 2^N
	 * If x_0 < x_1, then
	 *   -2^N < x_0 - x_1 < 0, so 1 < 2^N + 1 + x_0 - x_1 < 2^N + 1
	 * So
	 *   0 <= x_0 - x_1 mod  2^N + 1 <= 2^N < 2^N + 1
	 *
	 * Destination is seminormalized since
	 *   0 <= (x_0 - x_1) + (x_2) mod  2^N + 1 < (2^N + 1) + (2^N - 1) = 2*2^N
	 */

	/* If source already is seminormalized... */
	if (curta_mod_sn(source, slength, n)) {
		/* ... seminormalize means to copy digits from source to destination (but the case they're the same digits) */
		if (destination != source) {
			curta_copy(destination, source, slength);
		}
		curta_zeroise(destination + slength, n + INDEX_ONE - slength);
		return;
	}

	index dlength;

	/* destination := x_0 - x_1 */
	digit borrow = curta_sub(/*destination*/destination, &dlength, /*x_0*/source, n, /*x_1*/source + n, slength < 2 * n ? slength - n : n);
	*(destination + n) = DIGIT_ZERO;

	/* If borrow is propagated to the (N+1)-th digit, we have to add up 1 to the result of N digits */
	if (borrow) {
		curta_carry_add(destination, &dlength, curta_add1(/*destination*/destination, &dlength, /*destination*/destination, dlength, /*1*/borrow));
	}

	/* destination := x_0 - x_1 + x_2 */
	if (slength > 2 * n) {
		curta_carry_add(destination, &dlength, curta_add(/*destination*/destination, &dlength, /*destination*/destination, dlength, /*x_2*/source + 2 * n, slength < 3 * n ? slength - 2 * n : n));
	}
}
