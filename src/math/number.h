#ifndef NUMBER_H_
#define NUMBER_H_

#include <stdint.h>

/* Parsed digit representation */
typedef int_fast16_t digitp;
/* Result of comparation */
typedef int_fast16_t cmpres;

/* Digit representation */
typedef uint_fast64_t digit;
/* Index representation */
typedef uint_fast64_t index;

#define INDEX_BITS 64
#define DIGIT_BITS 64
#define DIGIT_MIN 0
#define DIGIT_MAX UINT_FAST64_MAX

#define CORE_C 1				/* C impl. */
#define CORE_ASM_x86 32			/* ASM impl. using x86 instruction set */
#define CORE_ASM_x64 64			/* ASM impl. using x86_64 instruction set */

/* CORE_IMPL */
#ifndef __WORDSIZE
#define CORE_IMPL CORE_C
#elif __WORDSIZE == 64
#define CORE_IMPL CORE_ASM_x64
#elif __WORDSIZE == 32
#define CORE_IMPL CORE_ASM_x86
#else
#define CORE_IMPL CORE_C
#endif

#define DIGIT_ZERO 0
#define DIGIT_ONE 1
#define INDEX_ZERO 0
#define INDEX_ONE 1
#define SGN_POS 1
#define SGN_EQ 0
#define SGN_NEG -1

/* Instantiate */
digit* digits_alloc(const index length);
/* Dispose */
void digits_free(digit* theDigits);
/* Print */
void digits_print(const digit* theDigits, const index length);

#endif /* NUMBER_H_ */
