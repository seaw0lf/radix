#include "ssaops.h"

/* Recursive Inplace Division (quotient := dividendreminder / divisor, dividendreminder := dividendreminder % divisor)
 *
 * Input:
 *   dividendreminder - dividend
 *   ddrlength - number of digits of dividend
 *   divisor - digits of divisor; > 0; most significant digit of divisor >= 2^DIGIT_BITS/2 i.e. divisor is normalized
 *   dsrlength - number of digits of divisor
 *   temp - additional temporary space; allocated >= dsrlength + 1
 * Output:
 *   dividendreminder - reminder
 *   ddrlength - number of digits of reminder
 *   quotient - quotient; allocated >= ddrlength - dsrlength + 1
 *   qlength - number of digits of quotient
 *
 * Algorithm:
 *   Algorithm 1.8 (RecursiveDivRem) by Brent & Zimmermann in Modern Computer Arithmetic
 *   http://www.loria.fr/~zimmerma/mca/mca-cup-0.5.9.pdf
 */
static void ssa_divRC(digit* quotient, index* qlength, digit* dividendreminder, index* ddrlength, digit* divisor, const index dsrlength, digit* temp) {

	/* Prerequisites: */
	/* quotient has at least ddrlength - dsrlength + 1 allocated digits */
	/* temp has at least dsrlength + 1 allocated digits and is used for multiplicating divisor by quotient guess */

	/* Step 2 */
	/* klength = FLOOR(m) >= 1 */
	index klength = (*ddrlength - dsrlength) >> 1;

	index dsrklength = dsrlength - klength;

	digit tmpdigit1 = DIGIT_ZERO;
	digit tmpdigit2 = DIGIT_ZERO;

	/*** Quotient limb 1 guess ***/

	/* Step 3 */
	*ddrlength -= 2 * klength;
	/* Prerequisites are fulfilled:
	 * quotient + klength ... (ddrlength - 2*klength) - (dsrlength - klength) + 1 = (ddrlength - dsrlength + 1) - klength
	 * temp ... dsrlength - klength < dsrlength + 1 */
	ssa_div(/*q*/quotient + klength, qlength, /*a*/dividendreminder + 2 * klength, /*b*/ddrlength, divisor + klength, dsrklength, /*t*/temp);

	/* qlength := length(Q_1)*/
	/* ddrlength := 2*k + length(R_1) */
	*ddrlength += 2 * klength;


	/*** Quotient limb 1 correction (2x max.) ***/
	/*** Alternative implementation without negative dividend reminder limb ***/
	cmpres tmpsgn = SGN_POS;
	index tmplength;

	/* Step 4a */
	/* temp := Q_1 * B_0 */
	/* SPECIALITA... pretoze k je dolna cela cast, tak sa moze stat,
	 * ze quotient je az o dve "cifry" vacsi ako k (ak by k bola horna cela cast, tak by
	 * quotient bol vacsi iba o jednu "cifru" a vedeli by sme pouzit fintu ako nizsie).
	 * Odlozime obe cifry, nastavime na nulu a vykoname nasobenie.
	 * Po pouziti (nasobenia) vratime do povodneho stavu - co si kto rozvinie, to si aj zvinie ;). */
	/* tmpdigit1 := backup1; tmpdigit2 := backup2; */
	if (*qlength > klength) {
		tmpdigit1 = *(divisor + klength); *(divisor + klength) = DIGIT_ZERO;
		tmpdigit2 = *(divisor + klength + INDEX_ONE); *(divisor + klength + INDEX_ONE) = DIGIT_ZERO;
	}
	/* Q_1 is m - k + 1 long, B_0 is k long; so Q_1 * B_0 is (m - k + 1) + (k) = m + 1 long */
	ListItem* ssamem = ssa_mul_alloc(*qlength);
	ssa_mul(temp, &tmplength, quotient + klength, divisor, qlength, ssamem);
	ssa_mul_free(ssamem);
	if (*qlength > klength) {
		*(divisor + klength + INDEX_ONE) = tmpdigit2;
		*(divisor + klength) = tmpdigit1;
	}

	/* Step 5 */
	/* dividendreminder < (temp << i) */
	*ddrlength -= klength;
	while (tmpsgn == SGN_POS && curta_cmp(dividendreminder + klength, *ddrlength, temp, tmplength) < SGN_EQ) {
		/* Q_1 := Q_1 - 1 */
		curta_sub1(quotient + klength, qlength, quotient + klength, *qlength, DIGIT_ONE);
		/* temp := temp - divisor */
		curta_carry_add(temp, &tmplength, curta_sgnadd(&tmpsgn, temp, &tmplength, temp, tmplength, SGN_NEG, divisor, dsrlength));
	}
	/* Step 4b */
	/* dividendreminder := dividendreminder - (temp << i) */
	tmpsgn *= SGN_NEG;
	curta_carry_add(dividendreminder + klength, ddrlength, curta_sgnadd(&tmpsgn, dividendreminder + klength, ddrlength, dividendreminder + klength, *ddrlength, tmpsgn, temp, tmplength));

	/* ddrlength := 2*k + length(R_1) */
	*ddrlength += klength;


	/*** Quotient limb 0 guess ***/

	/* Step 6 */
	/* Prerequisites are fulfilled:
	 * quotient ... (dsrlength) - (dsrlength - klength) + 1 = klength + 1
	 * temp ... dsrlength - klength < dsrlength + 1 */
	/* tmpdigit1 := backup; tmpdigit2 := qcarry; */
	tmpdigit1 = *(quotient + klength);
	*ddrlength -= klength;
	/* Pozn: na dlzku quotienta zneuzijeme tmplength... lebo qlength je prakticky taka aka ma byt... uz len k pricitame*/
	ssa_div(/*q*/quotient, &tmplength, /*a*/dividendreminder + klength, /*b*/ddrlength, divisor + klength, dsrklength, /*t*/temp);
	/* Put Q_0 together with Q_1 */
	/* Remember division carry. */
	tmpdigit2 = *(quotient + klength);
	/* Repair first digit of Q_1 destroyed by division carry Q_0. */
	*(quotient + klength) = tmpdigit1;
	/* If there was a carry add it to Q_1. */
	if (tmpdigit2) {
		curta_carry_add(quotient + klength, qlength, curta_add1(quotient + klength, qlength, quotient + klength, *qlength, DIGIT_ONE));
	}
	*qlength += klength;

	/* ddrlength := k + length(R_1) */
	*ddrlength += klength;


	/*** Quotient limb 0 correction (2x max.) ***/
	/*** Alternative implementation without negative dividend reminder limb ***/

	/* Step 7a */
	/* temp := Q_0 * B_0 */
	/* Q_0 is k + 1 long, B_0 is k long; so Q_0 * B_0 is (k + 1) + (k) = 2*k + 1 long */
	ssamem = ssa_mul_alloc(klength);
	ssa_mul(temp, &tmplength, quotient, divisor, &klength, ssamem);
	ssa_mul_free(ssamem);
	/* SPECIALITA: pretoze k je horna cela cast, tak sa moze stat,
	 * ze quotient je o jednu "cifru" vacsi ako k. Naviac by to mala byt vzdy 0 alebo 1.
	 * V pripade nuly je to ok... ale v pripade 1 by to este chcelo priratat B_0 raz. */
	if (tmpdigit2) {
		tmplength -= klength;
		curta_carry_add(temp + klength, &tmplength, curta_add(temp + klength, &tmplength, temp + klength, tmplength, divisor, klength));
		tmplength += klength;
	}

	/* Step 8 */
	/* dividendreminder < (temp << 0) */
	while (tmpsgn == SGN_POS && curta_cmp(dividendreminder, *ddrlength, temp, tmplength) < SGN_EQ) {
		/* Q_0 := Q_0 - 1 */
		curta_sub1(quotient, qlength, quotient, *qlength, DIGIT_ONE);
		/* temp := temp - divisor */
		curta_carry_add(temp, &tmplength, curta_sgnadd(&tmpsgn, temp, &tmplength, temp, tmplength, SGN_NEG, divisor, dsrlength));
	}
	/* Step 7b */
	/* dividendreminder := dividendreminder - (temp << 0) */
	tmpsgn *= SGN_NEG;
	curta_carry_add(dividendreminder, ddrlength, curta_sgnadd(&tmpsgn, dividendreminder, ddrlength, dividendreminder, *ddrlength, tmpsgn, temp, tmplength));
}

/* Unbalanced Inplace Division (quotient := dividendreminder / divisor, dividendreminder := dividendreminder % divisor)
 *
 * Input:
 *   dividendreminder - dividend
 *   ddrlength - number of digits of dividend; ddrlength > 2 * dsrlength
 *   divisor - digits of divisor; > 0; most significant digit of divisor >= 2^DIGIT_BITS/2 i.e. divisor is normalized
 *   dsrlength - number of digits of divisor
 *   temp - additional temporary space; allocated >= dsrlength + 1
 * Output:
 *   dividendreminder - reminder
 *   ddrlength - number of digits of reminder
 *   quotient - quotient; allocated >= ddrlength - dsrlength + 1
 *   qlength - number of digits of quotient
 *
 * Algorithm:
 *   Algorithm 1.9 (UnbalancedDivRem) by Brent & Zimmermann in Modern Computer Arithmetic
 *   http://www.loria.fr/~zimmerma/mca/mca-cup-0.5.9.pdf
 */
static void ssa_divUB(digit* quotient, index* qlength, digit* dividendreminder, index* ddrlength, digit* divisor, const index dsrlength, digit* temp) {

	index m = *ddrlength - dsrlength;

	/* Quotient significant part pointer */
	quotient += m;
	/* Initialize quotient's most significant digit (since we always want initialized backup value) to zero.
	 * Other quotient digits aren't required to be set to zero since division performs only write operations with quotient. */
	*quotient = DIGIT_ZERO;

	/* Pointer to quotient's digit to be backuped (since division writes one digit to quotient in addition) */
	digit* quotientBackup = quotient;
	/* Number of digits of quotient limb backuped digit as the least significant one */
	index qblength = INDEX_ZERO;

	/* Space for backup */
	digit backup;

	/* Dividend significant part pointer */
	dividendreminder += m;
	/* Temporary DividendReminder Length */
	index drlength = dsrlength;

	for (index step = dsrlength; m; m -= step) {

		/* Step size correction */
		if (m < dsrlength) {
			step = m;
		}

		quotient -= step;
		dividendreminder -= step;
		if (drlength) {
			drlength += step;
		} else {
			drlength = step;
			curta_msd(dividendreminder, &drlength);
		}

		backup = *quotientBackup;

		/* DivRem */
		ssa_div(/*q*/quotient, qlength, /*a*/dividendreminder, &drlength, /*b*/divisor, dsrlength, /*t*/temp);

		if (*qlength > step) {
			qblength = MAX(qblength, *qlength - step);
		} else {
			curta_zeroise(quotient + *qlength, step - *qlength);
		}

		/* quotientBackup points to 0 or 1 */
		if (*quotientBackup) {
			curta_carry_add(quotientBackup, &qblength, curta_add1(quotientBackup, &qblength, quotientBackup, qblength, backup));
		} else {
			*quotientBackup = backup;
			qblength = MAX(qblength, backup ? INDEX_ONE : INDEX_ZERO);
		}
		quotientBackup -= step;
		if (qblength) {
			qblength += step;
		} else {
			qblength = *qlength;
		}
	}
	*qlength = qblength;
	*ddrlength = drlength;
}

/* Inplace Division (quotient := dividendreminder / divisor, dividendreminder := dividendreminder % divisor)
 *
 * Input:
 *   dividendreminder - dividend
 *   ddrlength - number of digits of dividend; ddrlength > 2 * dsrlength
 *   divisor - digits of divisor; > 0; most significant digit of divisor >= 2^DIGIT_BITS/2 i.e. divisor is normalized
 *   dsrlength - number of digits of divisor
 *   temp - additional temporary space; allocated >= dsrlength + 1
 * Output:
 *   dividendreminder - reminder
 *   ddrlength - number of digits of reminder
 *   quotient - quotient; allocated >= ddrlength - dsrlength + 1
 *   qlength - number of digits of quotient
 */
void ssa_div(digit* quotient, index* qlength, digit* dividendreminder, index* ddrlength, digit* divisor, const index dsrlength, digit* temp) {

	/** If dividend is smaller than divisor, Q := 0, R := dividend **/
	if (*ddrlength < dsrlength || (*ddrlength == dsrlength && curta_cmp(dividendreminder, *ddrlength, divisor, dsrlength) < SGN_EQ)) {
		*quotient = DIGIT_ZERO;
		*qlength = INDEX_ZERO;
		return;
	}

	/** If dividend and divisor have same length and dividend is greater or equal than divisor, Q := 1, R := dividend - divisor **/
	if (*ddrlength == dsrlength) {
		*quotient = DIGIT_ONE;
		*qlength = INDEX_ONE;
		curta_sub(dividendreminder, ddrlength, dividendreminder, *ddrlength, divisor, dsrlength);
		return;
	}

	index dlength = *ddrlength - dsrlength;

	/** Cut off - If dividend and divisor are not very diverse in length, use basecase division. **/
	if (dlength <= TRESHOLD_DIV_RECURSIVE) {
		curta_divBC(/*q*/quotient, qlength, /*a*/dividendreminder, ddrlength, /*b*/divisor, dsrlength, /*t*/temp);
		return;
	}

	/** If dividend and divisor are diverse in length... **/
	if (dlength <= dsrlength) {
		ssa_divRC(/*q*/quotient, qlength, /*a*/dividendreminder, ddrlength, /*b*/divisor, dsrlength, /*t*/temp);
	} else {
		ssa_divUB(/*q*/quotient, qlength, /*a*/dividendreminder, ddrlength, /*b*/divisor, dsrlength, /*t*/temp);
	}
}

// Allocated space has to be >= m + (m % 2) for Karatsuba's multiplication.
// Since this this multiplication can be used for input of maximum size of TRESHOLD_MUL_SCHONHAGE_STRASSEN, m = TRESHOLD_MUL_SCHONHAGE_STRASSEN.
static digit mem_divNT[INDEX_ONE + (TRESHOLD_DIV_NEWTON << 1)];

/* Newton's Inplace Division
 * Special version for computing inverse divisor for Barrett's Divison
 *
 * Input:
 *   divisor - digits of divisor; > 0; allocated >= 2 * h; most significant digit of divisor >= 2^DIGIT_BITS/2 i.e. divisor is normalized
 *   dsrlength - number of digits of divisor (h + l)
 *   temp - additional temporary space; allocated >= (3 * h + 1) >= 2 * TRESHOLD_DIV_NEWTON
 *   divtemp - additional temporary space for division; allocated >= 3 * (h - l) >= TRESHOLD_DIV_NEWTON + 1
 * Output:
 *   inverse - digits of divisor inverse; allocated >= dsrlength + 2
 *   ilength - number of digits of divisor inverse
 *
 * It is required that h > l; therefore
 *   l := floor(dsrlength - 1 / 2)
 *   h := dsrlength - l
 *
 * Observe that h can be expressed as floor(dsrlength / 2) + 1
 * Observe that 2*h can be expressed as (dsrlength + 1) + ODD(dsrlength + 1)
 *
 * TODO zaktualizovat popis...
 * Since division is special (i.e. beta^{2*dsrlength} / divisor) the dividend isn't required.
 * Value beta^{2*dsrlength} is intented to be initialized in temp.
 * In fact, temp is used for special dividend beta^{2*dsrlength} and later on for T and U.
 * It is required to be allocated >= (2 * dsrlength + 2) + 1.
 *   2 * dsrlength + 2 for beta^{2*dsrlength}, T and U
 *   1 for carry when computing U
 *
 * Idea:
 *   http://courses.csail.mit.edu/6.006/spring11/rec/rec23.pdf
 *   http://www.csd.uwo.ca/~moreno/CS424/Lectures/FastDivisionAndGcd.ps.gz
 * Algorithm:
 *   Algorithm (mpn_invert2) [recursive version] by Zimmermann in Asymptotically Fast Division for GMP
 *   http://www.loria.fr/~zimmerma/papers/invert.pdf
 *   Algorithm 4.2 (Newton inversion, again) [linear version] by Hasselstrom in Fast Division of Large Integers
 *   http://www.treskal.com/kalle/exjobb/original-report.pdf
 */
void ssa_divNT(digit* inverse, index* ilength, digit* divisor, const index dsrlength, digit* temp) {

	/* Prologue:
	 * This method is intended to be used only for computing inverse divisor i.e. beta^2n / A, where beta^n/2 <= A < beta^n.
	 *
	 * Idea of Newton's method is finding root of function f by approximating the function f by its tangent:
	 *   x_{i+1} = x_{i} + x_{i} * f(x_{i}) / f'(x_{i})
	 *
	 * By choosing f(x) = a - 1/x the Newton's method will find 0 = f(x) = 1/x - a and that is 1/a by:
	 *   x_{i+1} = x_{i} + x_{i} * (1 - x_{i} * a)
	 *
	 * And when x_{i} > 0 then
	 *   0 <= 1/a - x_{i+1} <= x_{i}^2 * (1/a - x_{i})^2 / theta^3
	 * for some theta between x_{i} and 1/a.
	 *
	 * We want to compute beta^2n/A in "integer world".
	 * That means to compute beta^n/A to at least n fraction digits (beta^2n/A = beta^n * beta^n/A) in "floating point world" and multiply by beta^n/A.
	 *
	 * The implementation idea is to perform Newton's method in "integer world".
	 *
	 * Now assume that X_n has n digits and l = (n - 1) / 2, h = n - l; then:
	 *   X_n := X_h * beta^h + X_l
	 *
	 * Now let's
	 *   a := A * beta^{-n}
	 *   x_{i+1} := X_n * beta^{-n}
	 *   x_{i} := X_h * beta^{-h}
	 * where a, x_{i}, x_{i+1} are floating point numbers between 0 and 1 and
	 *       A, X_n are integers of n digits, X_h is integer of h digits
	 *
	 * So
	 *   X_n * beta^{-n} = X_h * beta^{-h} + X_h * beta^{-h} * (1 - X_h * beta^{-h} * A * beta^{-n})
	 * fits the Newton's method.
	 *
	 * Now multiplying the last equation by beta^{n} is a huge step towards moving to the "integer world":
	 *   X_n = X_h * beta^{n-h} + X_h * beta^{-h} * beta^{n} * (1 - X_h * beta^{-h} * A * beta^{-n}) =
	 *       = X_h * beta^{l} + X_h * beta^{-h} * (beta^{n} - X_h * beta^{-h} * A) =
	 *       = X_h * beta^{l} + X_h * (beta^{n+h} - X_h * A) * beta^{-2*h}
	 */

	/* Memory organization:
	 *
	 * Let n := dsrlength, l := (n - 1) / 2, h := n - l;
     *
	 * For T are n + h digits required.
	 * For U are 2 * h digits required.
	 *
	 * From the algorithm we have that h digits can be shared between T a U.
	 * Dividends allocated space for digits (>= 2*n + 2) is enough to store T and U since n + 2*h <= 2*n + 2 (considering odd and even n separately).
	 *
	 * Since dividends value beta^{2*h} is required only for the bottom case
	 * it is not required to be initialized at all (initialization will be part of the bottom case).
	 */

	/* Example:
	 *   beta := 10
	 *   dividend := CXXXXXXXXXXXX (2*5 + 2 digits + 1 extra digit) (value could be initialized to 10000000000 but it's not required)
	 *   divisor := 54321 (5 digits)
	 *   inverse := XXXXXX (5 + 1 digits)
	 *
	 * Step 1:
	 *   l := 2, h := 3
	 *
	 * Step 2 (recursion):
	 *   suppose 3 is treshold for using faster/simpler inverse algorithm like divRC
	 *   before:
	 *     dividend := XX1000000XXXX
	 *     divisor limb is 543
	 *   after:
	 *     dividend := XXXXXX337XXXX
	 *     inverse := 1841XX
	 *
	 * Step 3:
	 *     dividend := XXXX100004961
	 *
	 * Step 4:
	 *     dividend := XXXX099950640
	 *     inverse := 1840XX
	 *
	 * Step 5:
	 *     dividend := XXXX099049359
	 *     dividend := XXXX099049360 (1 + 2*h (7) digits not required anymore, n (5) digits hold beta^{n+h} - X_h * A)
	 *     th := 0 (n+1-th digit)
	 *
	 * Step 6:
	 *     dividend := 090712049360 (1 + 2*h (7) digits hold X_h * (beta^{n+h} - X_h * A)/beta^{l} , n (5) digits hold beta^{n+h} - X_h * A)
	 *     dividend :=   0907120493
	 *     cy := 0 (2*h+n-th digit) ... nothing added since th = 0
	 *
	 * Step 7:
	 *     inverse := 184090
	 */
	index tlength;

	/* Step 0 */
	if (dsrlength <= TRESHOLD_DIV_NEWTON) {

		/* Initialize dividend */
		curta_zeroise(mem_divNT, tlength = dsrlength << 1);
		*(mem_divNT + tlength++) = DIGIT_ONE;

		/* Divide */
		/* Sice temp has 2*dsrlength + 1 digits and divisor dsrlength,
		 * 2*dsrlength + 1 - dsrlength + 1 = dsrlength + 2 digits are
		 * required for inverse */
		ssa_div(/*q*/inverse, ilength, /*a*/mem_divNT, &tlength, /*b*/divisor, dsrlength, /*t*/temp);
		return;
	}

	/* Step 1 */
	index l = (dsrlength - INDEX_ONE) >> 1;
	index h = dsrlength - l;

	/* Step 2 */
	ssa_divNT(/*i*/inverse + l, ilength, /*b*/divisor + l, h, /*t*/temp);

	// MEMREQ optimalizacia - vsetky mulSSA_ pouzivaju rovnaku dlzku h cifier, ... mozme teda pouzit rovnaky plan, vsakze?

	/* Step 3 */
	/* mulSSA_(temp, &tlength, inverse + l, divisor, &dsrlength); begin */
	/* But this multipilication of dsrlength digits can be broken up into two multiplications of size h (where dsrlength = h + l) */
	index d = h - l;

	ListItem* ssamem = ssa_mul_alloc(h);
	ssa_mul(/*r*/temp, &tlength, /*a*/inverse + l, /*b*/divisor, &h, ssamem);
	curta_zeroise(temp + tlength, 2 * h - tlength);

	/* Dirty Magic * The price for saving some memory */
	curta_copy(mem_divNT, divisor + dsrlength, d);
	curta_copy(mem_divNT + d, inverse + l, d);
	curta_copy(mem_divNT + 2 * d, temp + dsrlength, d);

	curta_zeroise(divisor + dsrlength, d);

	curta_copy(inverse, temp + h, l);

	ssa_mul(/*r*/temp + h, &tlength, /*a*/inverse + l, /*b*/divisor + h, &h, ssamem); // MEMREQ kvoli tomuto musi byt divisor.alloc >= 2*h
	curta_zeroise(temp + h + tlength, 2 * h - tlength);

	curta_copy(divisor + dsrlength, mem_divNT,  d);
	curta_copy(inverse + l, mem_divNT + 2 * d, d);

	curta_carry_add(temp + h, &tlength, curta_add(/*r*/temp + h, &tlength, /*a*/temp + h, tlength, /*b*/inverse, h));
	for (digit i = *(inverse + dsrlength); i; --i) {
		curta_carry_add(temp + h, &tlength, curta_add(/*r*/temp + h, &tlength, /*a*/temp + h, tlength, /*b*/divisor, dsrlength));
	}
	tlength += h;
	curta_copy(inverse + l, mem_divNT + d, d);
	/* mulSSA_(temp, &tlength, inverse + l, divisor, &dsrlength); end */

	/* The resulting product from step 3 isn't expected to be longer than dsrlength + h
	 * but if it's shorter, then we need to pad it wit zeroes */
	if (tlength <= dsrlength + h) {
		curta_zeroise(temp + dsrlength + h, (dsrlength + h + INDEX_ONE) - tlength);
	}

	/* Step 4 */
	while (*(temp + dsrlength + h)) {
		curta_sub(/*r*/temp, &tlength, /*a*/temp, tlength, /*b*/divisor, dsrlength);
		curta_sub1(/*r*/inverse + l, ilength, /*a*/inverse + l, /*b*/*ilength, DIGIT_ONE);
	}

	/* Step 5 */
	curta_com(temp, temp, dsrlength + INDEX_ONE);
	digit th = *(temp + dsrlength) + curta_add1(/*r*/temp, &tlength, /*a*/temp, dsrlength, /*b*/DIGIT_ONE);

	/* Step 6 */
	curta_copy(temp, temp + l, h);
	ssa_mul(/*r*/temp + h, &tlength, /*a*/inverse + l, /*b*/temp, &h, ssamem);
	ssa_mul_free(ssamem);

	tlength -= h;
	digit cy = curta_add(/*r*/temp + 2*h, &tlength, /*a*/temp + 2*h, tlength, /*b*/temp, h);
	if (th) {
		cy += DIGIT_ONE + curta_add(/*r*/temp + 2*h, &tlength, /*a*/temp + 2*h, tlength, /*b*/inverse + l, h);
	}

	/* Step 7 */
	curta_copy(inverse, temp + 3 * h - l, l);
	curta_add1(/*r*/inverse + l, ilength, /*a*/inverse + l, *ilength, /*b*/cy);
	*ilength += l;
}

/* Barrett's Inplace Division (quotient := dividendreminder / divisor, dividendreminder := dividendreminder % divisor)
 *
 * Input:
 *   dividendreminder - dividend
 *   ddrlength - number of digits of dividend; ddrlength <= 2*dsrlength; allocated >= dsrlength + ilength - 1
 *   divisor - digits of divisor; > 0; most significant digit of divisor >= 2^DIGIT_BITS/2 i.e. divisor is normalized
 *   dsrlength - number of digits of divisor
 *   inverse - digits of divisor inverse; > 0
 *   ilength - number of digits of divisor inverse
 *   temp - additional temporary space; allocated >= 2 * ilength
 * Output:
 *   dividendreminder - reminder
 *   ddrlength - number of digits of reminder
 *   quotient - quotient; allocated >= dsrlength
 *   qlength - number of digits of quotient
 *
 * Idea:
 *   http://www.springerreference.com/docs/html/chapterdbid/317497.html
 *   http://alexandre.venelli.free.fr/pdf/slides_cardis11.pdf
 * Algorithm:
 *   Algorithm 2.5 (BarrettDivRem) by Brent & Zimmermann in Modern Computer Arithmetic
 *   http://www.loria.fr/~zimmerma/mca/mca-cup-0.5.9.pdf
 * Performance improvement to study:
 *   http://www.acsel-lab.com/arithmetic/arith18/papers/ARITH18_Hasenplaugh.pdf
 *   https://www.lirmm.fr/arith18/slides/hasenplaugh-FastModularReduction.ppt
 */
void ssa_divBT(digit* quotient, index* qlength, digit* dividendreminder, index* ddrlength, digit* divisor, const index dsrlength, const digit* inverse, const index ilength, digit* temp) {

	/** If dividend is smaller than divisor, Q := 0, R := dividend **/
	if (*ddrlength < dsrlength || (*ddrlength == dsrlength && curta_cmp(dividendreminder, *ddrlength, divisor, dsrlength) < SGN_EQ)) {
		*quotient = DIGIT_ZERO;
		*qlength = INDEX_ZERO;
		return;
	}

	/** If dividend and divisor have same length and dividend is greater or equal than divisor, Q := 1, R := dividend - divisor **/
	if (*ddrlength == dsrlength) {
		*quotient = DIGIT_ONE;
		*qlength = INDEX_ONE;
		curta_sub(dividendreminder, ddrlength, dividendreminder, *ddrlength, divisor, dsrlength);
		return;
	}

	index tlength;

	index ix = dsrlength - INDEX_ONE;

	/* Step 2 */
	/* temp := I * A_1 */
	ListItem* ssamem = ssa_mul_alloc(MAX(dsrlength, ilength));
	ssa_mul(temp, &tlength, dividendreminder + ix, inverse, &ilength, ssamem);
	/* Q := FLOOR(temp / beta) = FLOOR(I * A_1 / beta) */
	ix = dsrlength + INDEX_ONE;
	if (tlength > ix) {
		curta_copy(quotient, temp + ix, *qlength = tlength - ix);
	} else {
		*qlength = INDEX_ZERO;
	}
	if (dsrlength > *qlength) {
		curta_zeroise(quotient + *qlength, dsrlength - *qlength);
	}

	/* Step 3 */
	/* temp := Q * B */
	ssa_mul(temp, &tlength, quotient, divisor, &dsrlength, ssamem);
	ssa_mul_free(ssamem);
	/* A - temp = A - Q * B */
	curta_sub(dividendreminder, ddrlength, dividendreminder, *ddrlength, temp, tlength);

	/* Step 4 */
	while (curta_cmp(dividendreminder, *ddrlength, divisor, dsrlength) >= SGN_EQ) {
		/* Step 5 */
		/* Q := Q + 1 */
		curta_carry_add(quotient, qlength, curta_add1(quotient, qlength, quotient, *qlength, DIGIT_ONE));
		/* R := R - B */
		curta_sub(dividendreminder, ddrlength, dividendreminder, *ddrlength, divisor, dsrlength);
	}
}
