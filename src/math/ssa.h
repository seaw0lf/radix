#ifndef SSA_H_
#define SSA_H_

#include "numberops.h"
#include "../struc/list.h"


ListItem* ssa_mul_alloc(const index length);
void ssa_mul(digit* result, index* length, const digit* multiplicand1, const digit* multiplicand2, const index* mlength, ListItem* temp);
void ssa_mul_free(ListItem* temp);

#endif /* SSA_H_ */
