#ifndef NUMBERUTILS_H_
#define NUMBERUTILS_H_

#include "number.h"

#define MAX(A,B) ((A)<(B)?(B):(A))
#define MIN(A,B) ((A)<(B)?(A):(B))
#define ODD(A) ((A) & 1)
#define CEIL(DIVIDEND,DIVISOR) ((DIVIDEND) / (DIVISOR)) + ((DIVIDEND) % (DIVISOR) != 0)

double logX(index radix);
digit logB(digit number);
digit logD(digit base, digit number);
digit logREDUCE(const digit base, digit* number);

index digits2ibase(const digit base);
void exps(digit baseExps[], const digit power, const digit base);
digit power(digit base, digit power);

#if CORE_IMPL == CORE_ASM_x64

/* http://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html
 * http://en.wikipedia.org/wiki/Inline_assembler
 * http://www.cs.virginia.edu/~clc5q/gcc-inline-asm.pdf
 * http://www.advancedlinuxprogramming.com/alp-folder/alp-ch09-inline-asm.pdf
 * http://developer.amd.com/resources/documentation-articles/developer-guides-manuals/#manuals
 */

#define add(addend1, addend2, carry, result)		\
	__asm__ __volatile__(	/* rax := addend1 */	\
		"add %1,%%rax;"		/* rax += carry */		\
		"mov $0,%1;"		/* carry := 0 */		\
		"adc $0,%1;"		/* carry += CF */		\
		"add %3,%%rax;"		/* rax += addend2 */	\
		"adc $0,%1;"		/* carry += CF */		\
		:"=a"(result),		/* result := rax */		\
		 "=g"(carry)								\
		:"a"(addend1),								\
		 "g"(addend2),								\
		 "1"(carry)									\
		:"cc"										\
	);

#define sub(minuend1, minuend2, borrow, result)		\
	__asm__ __volatile__(	/* rax := minuend1 */	\
		"sub %1,%%rax;"		/* rax -= borrow */		\
		"mov $0,%1;"		/* borrow := 0 */		\
		"adc $0,%1;"		/* borrow += CF */		\
		"sub %3,%%rax;"		/* rax -= minuend2 */	\
		"adc $0,%1;"		/* borrow += CF */		\
		:"=a"(result),		/* result := rax */		\
		 "=g"(borrow)								\
		:"a"(minuend1),								\
		 "g"(minuend2),								\
		 "1"(borrow)								\
		:"cc"										\
	);

#define mul(multiplicand1, multiplicand2, subresult, carry, result)	\
	__asm__ __volatile__(	/* rax := multiplicand1 */				\
		"mulq %3;"			/* rax *= multiplicand2 */				\
		"add %1,%%rax;"		/* rax += carry */						\
		"mov %%rdx,%1;"		/* carry := rdx */						\
		"adc $0,%1;"		/* carry += CF */						\
		"add %5,%%rax;"		/* carry += subresult */				\
		"adc $0,%1;"		/* carry += CF */						\
		:"=a"(result),		/* result := rax */						\
		 "=g"(carry)												\
		:"a"(multiplicand1),										\
		 "g"(multiplicand2),										\
		 "1"(carry),												\
		 "g"(subresult)												\
		:"rdx","cc"													\
	);

#define divrem(dividendHI, dividenLO, divisor, quotient, reminder)			\
	__asm__ __volatile__(	/* rax := dividendHI; rdx := dividenLO; */		\
		"divq %4;"			/* rdx:rax /= divisor */						\
		:"=a"(quotient),	/* quotient := rax */							\
		 "=d"(reminder)		/* reminder := rdx */							\
		:"a"(dividenLO),													\
		 "1"(dividendHI),													\
		 "g"(divisor)														\
		:"cc"																\
	);

#elif CORE_IMPL == CORE_ASM_x86

#define add(addend1, addend2, carry, result)		\
	__asm__ __volatile__(							\
		"mov $-1,%%eax;"	/* eax := 0xFFFFFFFF */	\
		"add %4,%%eax;"		/* eax := eax + carry */\
		"mov %2,%%eax;"		/* eax := addend1_lo */	\
		"adc %3,%%eax;"		/* eax += addend2_lo */	\
		"mov %%eax,%0;"		/* result_lo := eax */	\
		"mov 4%2,%%eax;"	/* eax := addend1_hi */	\
		"adc 4%3,%%eax;"	/* eax += addend2_hi */	\
		"mov %%eax,4%0;"	/* result_hi := eax */	\
		"movw $0,%w1;"		/* carry := 0 */		\
		"adc $0,%1;"		/* carry += CF */		\
		:"=m"(result),								\
		 "=g"(carry)								\
		:"m"(addend1),								\
		 "m"(addend2),								\
		 "1"(carry)									\
		:"cc","eax"									\
	);

#define sub(minuend1, minuend2, borrow, result)			\
	__asm__ __volatile__(								\
		"mov $0,%%eax;"		/* eax := 0 */				\
		"sub %4,%%eax;"		/* eax := eax - borrow */	\
		"mov %2,%%eax;"		/* eax := minuend1_lo */	\
		"sbb %3,%%eax;"		/* eax -= minuend2_lo */	\
		"mov %%eax,%0;"		/* result_lo := eax */		\
		"mov 4%2,%%eax;"	/* eax := minuend1_hi */	\
		"sbb 4%3,%%eax;"	/* eax += minuend2_hi */	\
		"mov %%eax,4%0;"	/* result_hi := eax */		\
		"movw $0,%w1;"		/* carry := 0 */			\
		"adc $0,%1;"		/* carry += CF */			\
		:"=m"(result),									\
		 "=g"(borrow)									\
		:"m"(minuend1),									\
		 "m"(minuend2),									\
		 "1"(borrow)									\
		:"cc","eax"										\
	);

#define mul(multiplicand1, multiplicand2, subresult, carry, result)	\
	__asm__ __volatile__(											\
																	\
		/* temp_reg := edx:eax */									\
		/* cr_rs_reg := carry_lo:result_hi */						\
																	\
		"mov %2,%%eax;"												\
		"mull %3;"			/* temp_reg := m1_lo * m2_lo */			\
																	\
		"add %4,%%eax;"												\
		"adc 4%4,%%edx;"	/* temp_reg += carry */					\
																	\
		"movl $0,%1;"												\
		"movl $0,4%1;"												\
		"adc $0,%1;"		/* carry += CF */						\
																	\
		"add %5,%%eax;"												\
		"adc 4%5,%%edx;"	/* temp_reg += subresult */				\
		"adc $0,%1;"		/* carry += CF */						\
																	\
		"mov %%eax,%0;"												\
		"mov %%edx,4%0;"	/* result := temp_reg */				\
																	\
		"mov 4%2,%%eax;"											\
		"mull 4%3;"			/* temp_reg := m1_hi * m2_hi */			\
																	\
		"add %%eax,%1;"												\
		"adc %%edx,4%1;"	/* carry += temp_reg */					\
																	\
		"mov %2,%%eax;"												\
		"mull 4%3;"			/* temp_reg := m1_lo * m2_hi */			\
		"add %%eax,4%0;"											\
		"adc %%edx,%1;"		/* cr_rs += temp_reg */					\
		"adc $0,4%1;"		/* carry_hi += CF */					\
																	\
		"mov 4%2,%%eax;"											\
		"mull %3;"			/* temp_reg := m1_hi * m2_lo */			\
		"add %%eax,4%0;"											\
		"adc %%edx,%1;"		/* cr_rs += temp_reg */					\
		"adc $0,4%1;"		/* carry_hi += CF */					\
																	\
		:"=m"(result),												\
		 "=g"(carry)												\
		:"m"(multiplicand1),										\
		 "m"(multiplicand2),										\
		 "1"(carry),												\
		 "m"(subresult)												\
		:"cc","eax","edx"											\
	);

#define divrem(dividendHI, dividenLO, divisor, quotient, reminder)						\
	__asm__ __volatile__(																\
																						\
		/* temp_reg := edx:eax */														\
		/* temp := reminder */															\
		/* reminder is required only for storing at the end of the alg. so it			\
		 * will be used as temp space (no extra registers :( ) */						\
																						\
		"mov %2,%%eax;"																	\
		"mov 4%2,%%edx;"	/* temp_reg := a1:a2 */										\
		"mov %%eax,4%1;"	/* temp := a2:?? */											\
																						\
		"divl 4%4;"			/* eax := temp_reg / b1 */									\
		"mov %%eax,4%0;"	/* quotient_hi := eax */									\
		"mull 4%4;"			/* temp_reg := quotient_hi * b1 */							\
																						\
		"sub %%eax,4%1;"	/* temp := c:?? */											\
		"mov 4%3,%%eax;"																\
		"mov %%eax,%1;"		/* temp (C) := c:a3 */										\
																						\
		"mov 4%0,%%eax;"	/* quotient_hi := eax */									\
		"mull %4;"			/* temp_reg (D) := quotient_hi * b2 */						\
																						\
		"sub %%eax,%1;"																	\
		"sbb %%edx,4%1;"	/* temp (R) := temp (C) - temp_reg (D) */					\
		"mov %1,%%eax;"																	\
		"mov 4%1,%%edx;"	/* temp_reg (R) := temp */									\
																						\
		"jnc okHI;"			/* jump if temp (R) >= 0 i.e. R remained positive */		\
																						\
							/* temp (R) < 0 i.e. R changed to negative */				\
		"decl 4%0;"			/* quotient_hi-- */											\
		"add %4,%%eax;"																	\
		"adc 4%4,%%edx;"	/* temp_reg (R) += divisor */								\
		"jc okHI;"			/* jump if temp (R) >= 0 i.e. R changed to positive */		\
																						\
		"decl 4%0;"			/* quotient_hi-- */											\
		"add %4,%%eax;"																	\
		"adc 4%4,%%edx;"	/* temp_reg (R) += divisor */								\
																						\
		"okHI:"																			\
							/* temp_reg := r1:r2 */										\
		"mov %%eax,4%1;"	/* temp := r2:?? */											\
																						\
		"divl 4%4;"			/* eax := temp_reg / b1 */									\
		"mov %%eax,%0;"		/* quotient_hi := eax */									\
		"mull 4%4;"			/* temp_reg := quotient_hi * b1 */							\
																						\
		"sub %%eax,4%1;"	/* temp := c:?? */											\
		"mov %3,%%eax;"																	\
		"mov %%eax,%1;"		/* temp (C) := c:a4 */										\
																						\
		"mov %0,%%eax;"		/* quotient_lo := eax */									\
		"mull %4;"			/* temp_reg (D) := quotient_hi * b2 */						\
																						\
		"sub %%eax,%1;"																	\
		"sbb %%edx,4%1;"	/* temp (R) := temp (C) - temp_reg (D) */					\
		"mov %1,%%eax;"																	\
		"mov 4%1,%%edx;"	/* temp_reg (R) := temp */									\
																						\
		"jnc okLO;"			/* jump if temp (R) >= 0 i.e. R remained positive */		\
																						\
							/* temp (R) < 0 i.e. R changed to negative */				\
		"mov %1,%%eax;"																	\
		"mov 4%1,%%edx;"	/* temp_reg (R) := temp */									\
																						\
		"decl %0;"			/* quotient_hi-- */											\
		"add %4,%%eax;"																	\
		"adc 4%4,%%edx;"	/* temp_reg (R) += divisor */								\
		"jc okLO;"			/* jump if temp (R) >= 0 i.e. R changed to positive */		\
																						\
		"decl %0;"			/* quotient_hi-- */											\
		"add %4,%%eax;"																	\
		"adc 4%4,%%edx;"	/* temp_reg (R) += divisor */								\
																						\
		"okLO:"																			\
		"mov %%eax,%1;"																	\
		"mov %%edx,4%1;"	/* reminder := temp */										\
																						\
		:"=m"(quotient),																\
		 "=m"(reminder)																	\
		:"m"(dividendHI),																\
		 "m"(dividenLO),																\
		 "m"(divisor)																	\
		:"cc","eax","edx"               												\
	);

#else

void add(const digit addend1, const digit addend2, digit* carry, digit* result);
void sub(const digit minuend1, const digit minuend2, digit* borrow, digit* result);
void mul(const digit multiplicand1, const digit multiplicand2, const digit subresult, digit* carry, digit* result);
void divrem(const digit a12, const digit a34, const digit b, digit* c, digit* q, digit* r);

#endif

#endif /* NUMBERUTILS_H_ */
