#ifndef FFT_H_
#define FFT_H_

#include "modularops.h"

void fFFT(digit* F, index k, index step, index omega, index N, digit* temp);
void bFFT(digit* F, index k, index step, index omega, index N, digit* temp);

#endif /* FFT_H_ */
