#ifndef NUMBEROPS_H_
#define NUMBEROPS_H_

#include <stdbool.h>
#include "number.h"

// TODO TRESHOLD -> THRESHOLD
#define TRESHOLD_MUL_KARATSUBA 20
#define TRESHOLD_MUL_SCHONHAGE_STRASSEN 37128
#define TRESHOLD_DIV_RECURSIVE 64
#define TRESHOLD_DIV_NEWTON 8192
#define TRESHOLD_DIV_BARRET 10 // TODO ma taketo zmysel?

void curta_msd(const digit* comparand, index* length);

bool curta_z(const digit* comparand, const index length);
cmpres curta_cmp(const digit* comparand1, const index length1, const digit* comparand2, const index length2);

void curta_zeroise(digit* result, index length);
void curta_fill(digit* destination, index length, digit value);
void curta_copy(digit* destination, const digit* source, index length);
void curta_copyb(digit* destination, const digit* source, index length);
void curta_com(digit* destination, const digit* source, index length);

void curta_shl(digit* destination, index* length, const digit* source, const index slength, const index bits);
void curta_shr(digit* destination, index* length, const digit* source, const index slength, const index bits);

void curta_carry_add(digit* result, index* length, const digit carry);
void curta_carry_set(digit* result, index* length, const digit carry);

digit curta_add(digit* result, index* length, const digit* addend1, const index length1, const digit* addend2, const index length2);
digit curta_add1(digit* result, index* length, register const digit* addend1, const index length1, const digit addend2);
digit curta_sub(digit* result, index* length, register const digit* minuend1, const index length1, register const digit* minuend2, const index length2);
digit curta_sub1(digit* result, index* length, register const digit* minuend1, const index length1, const digit minuend2);
digit curta_sgnadd(cmpres* sgn, digit* result, index* length, const digit* addend1, const index length1, const cmpres sgn2, const digit* addend2, const index length2);

void curta_mul1(digit* result, index* rlength, const digit multiplicand1, const digit* multiplicand2, const index length);
void curta_mulKT(digit* result, index* length, const digit* multiplicand1, const digit* multiplicand2, const index mlength, digit* temp);

void curta_divBC(digit* quotient, index* qlength, digit* dividendreminder, index* ddrlength, digit* divisor, const index dsrlength, digit* temp);

#endif /* NUMBEROPS_H_ */
