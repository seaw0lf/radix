#include "fft.h"

#define BITREV_TABLE_SIZE 7

static const index bitrevTable[BITREV_TABLE_SIZE][1 << (BITREV_TABLE_SIZE - 1)] = {
		/* K = 0 */{ 0 },
		/* K = 1 */{ 0, 1 },
		/* K = 2 */{ 0, 2, 1, 3 },
		/* K = 3 */{ 0, 4, 2, 6, 1, 5, 3, 7 },
		/* K = 4 */{ 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 },
		/* K = 5 */{ 0, 16, 8, 24, 4, 20, 12, 28, 2, 18, 10, 26, 6, 22, 14, 30, 1, 17, 9, 25, 5, 21, 13, 29, 3, 19, 11, 27, 7, 23, 15, 31 },
		/* K = 6 */{ 0, 32, 16, 48, 8, 40, 24, 56, 4, 36, 20, 52, 12, 44, 28, 60, 2, 34, 18, 50, 10, 42, 26, 58, 6, 38, 22, 54, 14, 46, 30, 62, 1, 33, 17, 49, 9, 41, 25, 57, 5, 37, 21, 53, 13, 45, 29, 61, 3, 35, 19, 51, 11, 43, 27, 59, 7, 39, 23, 55, 15, 47, 31, 63 },
	};

/* Bit reversal algorithm
 *
 * Algorithm:
 *   http://graphics.stanford.edu/~seander/bithacks.html#ReverseParallel
 */
static inline index bitrev(register index j, index K) {

	/* Basecase bit reversal algorithm replaced by bitrev table */

	if (K < BITREV_TABLE_SIZE) {

		/*
		index r = 0;
		while (K--) {
			r <<= 1;
			r |= (j & 1);
			j >>= 1;
		}
		return r;
		*/
		return bitrevTable[K][j];
	}

	/* Parallel bit reversal algorithm */

	K = INDEX_BITS - K;

	// center the number
	j <<= (K >> 1);

	// swap odd and even bits
	j = ((j >>  1) & 0x5555555555555555) | ((j & 0x5555555555555555) <<  1);
	// swap consecutive pairs
	j = ((j >>  2) & 0x3333333333333333) | ((j & 0x3333333333333333) <<  2);
	// swap nibbles ...
	j = ((j >>  4) & 0x0F0F0F0F0F0F0F0F) | ((j & 0x0F0F0F0F0F0F0F0F) <<  4);
	// swap bytes
	j = ((j >>  8) & 0x00FF00FF00FF00FF) | ((j & 0x00FF00FF00FF00FF) <<  8);
	// swap 2-byte long pairs
	j = ((j >> 16) & 0x0000FFFF0000FFFF) | ((j & 0x0000FFFF0000FFFF) << 16);
	// swap 4-byte long pairs
	j = ( j >> 32                      ) | ( j                       << 32);

	// align right (and deal with odd input value K)
	j >>= ((K >> 1) + (K & 1));

	return j;
}

/* Butterfly operation
 *
 * omega - represents PRU 2^omega
 * N - represents number 2^N + 1
 * temp - temporary space; allocated >= 2 * (N + 1)
 */
static inline void bfy__inplace(digit* A, digit* B, index omega, index N, digit* temp) {

	// temp = 2^omega * *B;
	if (omega < N) {
		/* Multiplication by 2^k is shift left by k modulo 2^N + 1 (for 0 <= k < N) */
		curta_mod_shl(temp, B, omega, N, temp + N + INDEX_ONE);
	} else {
		/* Multiplication by 2^k can be reduced to shift right by 2^N - k modulo 2^N + 1 (N <= k < 2*N) */
		curta_mod_shr(temp, B, 2 * N - omega, N, temp + N + INDEX_ONE);
	}

	// *B = *A - temp
	curta_mod_sub(B, A, temp, N);
	// *A = *A + temp
	curta_mod_add(A, A, temp, N);
}

/* Forward Fast Fourier Transformation; Decimation In Time; In place; Bit Reversed Output (with normal order input)
 *
 * N - represents number 2^N + 1
 * omega - represents PRU 2^omega in ring 2^N + 1
 * F - array of K numbers of N+1 digits
 * digits - N+1 digits
 * k, K - K := 2^k; represents # of numbers in array F
 * temp - temporary space; allocated >= 2 * (N + 1)
 *
 * Algorithm:
 *   Algorithm 2.2 (ForwardFFT) by Brent & Zimmermann in Modern Computer Arithmetic
 *   http://www.loria.fr/~zimmerma/mca/mca-cup-0.5.9.pdf
 * Performance improvement to study:
 *   http://www.usna.edu/Users/cs/roche/research/phd/ch4-lsmul.pdf (Chapter 4.3)
 *   http://issac2009.kias.re.kr/Roche.pdf
 */
void fFFT(digit* F, index k, index digits, index omega, index N, digit* temp) {
	switch (k) {

		case 0:
			break;

		case 1:
			bfy__inplace(F, F + digits, INDEX_ZERO, N, temp);
			break;

		default:
			k--;

			digit* G = F + digits;

			digits <<= INDEX_ONE;

			fFFT(F, k, digits, 2 * omega, N, temp);
			fFFT(G, k, digits, 2 * omega, N, temp);

			index K = INDEX_ONE << k;
			for (index j = INDEX_ZERO; j < K; ++j, F += digits, G += digits) {
				bfy__inplace(F, G, omega * bitrev(j, k), N, temp);
			}
			break;
	}
}

/* Backward/Inverse Fast Fourier Transformation without normalization (division by 2); Decimation In Time; In place; Normal Order Output (with bit reversed input)
 *
 * F - array of K numbers of N+1 digits
 * digits - N+1 digits
 * K - represents # of numbers in array F
 * N - represents primitive root of unity 2^omega in ring 2^N + 1
 * temp - temporary space; allocated >= 2 * (N + 1)
 *
 * Algorithm:
 *   Algorithm 2.3 (BackwardFFT) by Brent & Zimmermann in Modern Computer Arithmetic
 *   http://www.loria.fr/~zimmerma/mca/mca-cup-0.5.9.pdf
 */
void bFFT(digit* F, index k, index digits, index omega, index N, digit* temp) {
	switch (k) {

		case 0:
			break;

		case 1:
			bfy__inplace(F, F + digits, INDEX_ZERO, N, temp);
			break;

		default:
			k--;

			// G := F + K * digits
			digit* G = F + (digits << k);

			bFFT(F, k, digits, 2 * omega, N, temp);
			bFFT(G, k, digits, 2 * omega, N, temp);

			k = INDEX_ONE << k;
			for (index j = INDEX_ZERO; j < k; ++j, F += digits, G += digits) {
				bfy__inplace(F, G, 2 * N - omega * j, N, temp); /* Division by k can be reduced to multiplication by 2N - k modulo 2^N + 1 */
			}
			break;
	}
}
