#include "precognito.h"
#include "math/numberutils.h"

index precognito_digits(digit radix, digit number) {

	if (!number) {
		return INDEX_ZERO;
	}

	return logREDUCE(radix, &number) + INDEX_ONE;
}
