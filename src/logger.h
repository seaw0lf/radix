#ifndef LOGGER_H_
#define LOGGER_H_

#include <stdio.h>

/* Available loggin levels */
#define LEVEL_NONE 0
#define LEVEL_INFO 1
#define LEVEL_DEBUG 2
#define LEVEL_TRACE 3
#define LEVEL_CTRACE 4
#define LEVEL_ALL 9

/* Current loggin level */
#define LOG_LEVEL LEVEL_ALL

/* ERROR */
#define error(msg) fprintf(stderr, msg)
#define errorf(msg, ...) fprintf(stderr, msg, __VA_ARGS__)

/* INFO */
#if LOG_LEVEL >= LEVEL_INFO
#define info(msg) printf(msg)
#define infof(msg, ...) printf(msg, __VA_ARGS__)
#else
#define info(msg) (void)NULL
#define infof(msg, ...) (void)NULL
#endif

#define infoln(msg) info(msg "\n")
#define infofln(msg, ...) infof(msg "\n", __VA_ARGS__)

/* DEBUG */
#if LOG_LEVEL >= LEVEL_DEBUG
#define debug(msg) printf(msg)
#define debugf(msg, ...) printf(msg, __VA_ARGS__)
#else
#define debug(msg) (void)NULL
#define debugf(msg, ...) (void)NULL
#endif

#define debugln(msg) debug(msg "\n")
#define debugfln(msg, ...) debugf(msg "\n", __VA_ARGS__)

/* TRACE */
#if LOG_LEVEL >= LEVEL_TRACE
#define trace(msg) printf(msg)
#define tracef(msg, ...) printf(msg, __VA_ARGS__)
#else
#define trace(msg) (void)NULL
#define tracef(msg, ...) (void)NULL
#endif

#define traceln(msg) trace(msg "\n")
#define tracefln(msg, ...) tracef(msg "\n", __VA_ARGS__)

/* TRACE with caller */
#if LOG_LEVEL >= LEVEL_CTRACE
#define ctrace(msg) printf("%s: " msg, __func__)
#define ctracef(msg, ...) printf("%s: " msg, __func__, __VA_ARGS__)
#else
#define ctrace(msg) (void)NULL
#define ctracef(msg, ...) (void)NULL
#endif

#define ctraceln(msg) ctrace(msg "\n")
#define ctracefln(msg, ...) ctracef(msg "\n", __VA_ARGS__)

/* STOPWATCH */
#if LOG_LEVEL >= LEVEL_DEBUG
#include <time.h>
#define stopwatch_start(id) time_t start_##id, stop_##id; time(&start_##id)
#define stopwatch_reset(id) clock(&start_##id)
#define stopwatch_stop(id) (time(&stop_##id) - start_##id)
#else
#define stopwatch_start(id) (void)NULL
#define stopwatch_reset(id) (void)NULL
#define stopwatch_stop(id) 0
#endif

#endif /* LOGGER_H_ */
