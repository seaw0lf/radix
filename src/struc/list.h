#ifndef LIST_H_
#define LIST_H_

struct listitem {
	void* data;
	struct listitem* next;
};

typedef struct listitem ListItem;

ListItem* allocListItem(void* data, ListItem* next);
ListItem* freeListItem(ListItem* anItem, void (*freeData)(void*));

void freeList(ListItem* firstItem, void (*freeData)(void*));

void printList(ListItem* firstItem, void (*printData)(void*));

#endif /* LIST_H_ */
