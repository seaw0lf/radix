#include <stdlib.h>
#include "list.h"
#include "../logger.h"

/***** List item management *****/

/* Allocate a list item */
/* Returns pointer to the new list item */
ListItem* allocListItem(void* data, ListItem* next) {

	/* Return nothing on error */
	if (data == NULL)
		return NULL;

	/* Allocate an item */
	ListItem* anItem = malloc(sizeof(ListItem));

	/* Return nothing on error */
	if (anItem == NULL)
		return NULL;

	/* Initialize members */
	anItem->data = data;
	anItem->next = next;

	return anItem;
}

/* Free a list item */
/* Returns pointer to next list item */
ListItem* freeListItem(ListItem* anItem, void (*freeData)(void*)) {

	/* If we have nothing, we are done */
	if (anItem == NULL)
		return NULL;

	/* Store pointer to next item */
	ListItem* aNextItem = anItem->next;

	/* Free data member */
	if (freeData != NULL) {
		freeData(anItem->data);
	}
	/* Free item */
	free(anItem);

	/* Return pointer to next item */
	return aNextItem;
}

/* Free list */
void freeList(ListItem* firstItem, void (*freeData)(void*)) {

	/* Free first list item and continue with next one */
	while (firstItem != NULL) {
		firstItem = freeListItem(firstItem, freeData);
	}
}

/* Print list using printData function for data */
void printList(ListItem* firstItem, void (*printData)(void*)) {

	while (firstItem != NULL) {

		printData(firstItem->data);
		trace("\n");

		/* Continue with next list item */
		firstItem = firstItem->next;
	}
	trace("\n");
}
