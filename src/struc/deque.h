#ifndef DEQUE_H_
#define DEQUE_H_

/* Significantly modified version:
 * 1. push - required immediately after construction
 * 2. pop - after construction & push
 */

typedef struct {
	/* First nonempty container chunk */
	size_t ixFront;
	/* First unread char of first nonempty container chunk */
	size_t ixFrontLocal;
	/* Last nonempty container chunk */
	size_t ixBack;
	/* Last unread char of last nonempty container chunk */
	size_t ixBackLocal;

	/* Number of unread characters of container chunks */
	size_t charCount;
	/* Number of allocated container chunks */
	size_t containerSize;
	/* The chunk container; chunk contains chars i.e. chunk is a string (part of input) */
	char** containter;
} Deque;

Deque* allocDeque(size_t containerSize, size_t bufferSize);
void freeDeque(Deque* dq);

//void pushFront(char c);
//void pushBack(char c);
size_t pushString(Deque* dq, char* buffer, size_t size);

char front(Deque* dq);
char back(Deque* dq);

void popFront(Deque* dq);
void popBack(Deque* dq);

#endif /* DEQUE_H_ */
