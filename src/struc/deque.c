#include <stdlib.h>
#include "deque.h"
#include "../io/input.h"
#include "../logger.h"

static size_t bufferSize;

/***** Deque management *****/

static void freeContainerElement(char* element) {
	free(element);
}

static void freeContainerContent(char** container, size_t length) {
	while(length--) {
		freeContainerElement(container[length]);
	}
}

static char** allocContainer(const size_t size) {
	return malloc(size * sizeof(char*));
}

static char** reallocContainer(Deque* dq, const size_t containerSize) {

	/* Allocate new container */
	char** theNewContainer = allocContainer(containerSize);

	/* Return nothing on error */
	if (theNewContainer == NULL)
		return NULL;

	/* Copy pointer into the new container */
	for (size_t ix = dq->ixFront; ix < dq->ixBack; ++ix) {
		theNewContainer[ix] = dq->containter[ix];
	}

	/* Remember original container */
	char** theContainer = dq->containter;

	/* Put the new container into deque */
	dq->containter = theNewContainer;
	dq->containerSize = containerSize;

	/* Return original container */
	return theContainer;
}

static void freeContainer(char** container) {
	if (container) {
		free(container);
	}
}

Deque* allocDeque(size_t contSize, size_t bufSize) {
	bufferSize = bufSize;

	Deque* dq = malloc(sizeof(Deque));
	if (!dq) {
		return NULL;
	}

	dq->charCount = 0;
	dq->ixBack = dq->ixFront = 0;
	dq->ixBackLocal = dq->ixFrontLocal = 0;

	dq->containter = allocContainer(dq->containerSize = contSize);
	if (!dq->containter) {
		freeDeque(dq);
		return NULL;
	}

	return dq;
}

void freeDeque(Deque* dq) {
	freeContainerContent(dq->containter + dq->ixFront,
			dq->ixBack - dq->ixFront);
	freeContainer(dq->containter);
	free(dq);
}

/***** Deque operations *****/

size_t pushString(Deque* dq, char* buffer, size_t size) {

	if (dq->ixBack == dq->containerSize) {
		freeContainer(reallocContainer(dq, dq->containerSize * 2));
		if (!dq->containter) {
			return 0;
		}
	}

	dq->containter[dq->ixBack++] = buffer;
	dq->ixBackLocal = size;
	dq->charCount += size;
	return size;
}

inline char front(Deque* dq) {

	if (!dq->charCount) {
		return ASCII_EOT;
	}

	return ((char*) dq->containter[dq->ixFront])[dq->ixFrontLocal];
}

inline char back(Deque* dq) {

	if (!dq->charCount) {
		return ASCII_EOT;
	}

	return ((char*) dq->containter[dq->ixBack - 1])[dq->ixBackLocal - 1];
}

inline void popFront(Deque* dq) {

	if (!dq->charCount) {
		return;
	}

	dq->charCount--;
	dq->ixFrontLocal++;
	if (dq->ixFrontLocal == bufferSize) {
		freeContainerElement(dq->containter[dq->ixFront]);
		dq->ixFrontLocal = 0;
		dq->ixFront++;
	}

}

inline void popBack(Deque* dq) {

	if (!dq->charCount) {
		return;
	}

	dq->charCount--;
	dq->ixBackLocal--;
	if (dq->ixBackLocal == 0) {
		dq->ixBackLocal = bufferSize;
		dq->ixBack--;
		freeContainerElement(dq->containter[dq->ixBack]);
	}
}
