#include <inttypes.h>
#include <unistd.h>
#include <stdio.h>
#include "math/numberops.h"
#include "testutils.h"
#include "logger.h"

static const char siPrefix[] = { ' ', 'k', 'M', 'G' };
static char sizeofmemBuff[6 * 32];

char* sizeofmem(short i, index bytes) {

	index xB = bytes;
	short siIndex = 0;
	for (; xB > 1024; siIndex++)
		xB >>= 10;

	sprintf(sizeofmemBuff + 32 * i, "%"PRIuFAST64"B ~ %"PRIuFAST64" %cB", bytes, xB, siPrefix[siIndex]);
	return sizeofmemBuff + 32 * i;
}

/* FILE <--> Number */

void digits2file(char* filename, digit* digits, index used, index allocated) {

	FILE* file = fopen(filename, "w");

	if (!file) {
		infofln("Cannot open file %s (w)", filename);
		return;
	}

	fprintf(file, "%"PRIuFAST64"/%"PRIuFAST64"\n", used, allocated);
	while (used--) {
		fprintf(file, "%"PRIuFAST64"\n", *(digits++));
	}
	fclose(file);
}

digit* file2digits(char* filename, index* used, index* allocated) {

	FILE* file = fopen(filename, "r");

	if (!file) {
		infofln("Cannot open file %s (r)", filename);
		return NULL;
	}

	fscanf(file, "%"PRIuFAST64"/%"PRIuFAST64, used, allocated);
	digit* result = digits_alloc(*allocated);

	if (result) {
		curta_zeroise(result + *used, *allocated - *used);
		for (digit* r = result; fscanf(file, "%"PRIuFAST64, r) != EOF; r++)
			;
	}

	fclose(file);

	return result;
}
