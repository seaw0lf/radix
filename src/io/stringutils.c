#include "stringutils.h"
#include "input.h"
#include "output.h"

inline digit char2digit(char* input, digit radix, size_t length) {

	/* Value of currently parsed number */
	register digit number = DIGIT_ZERO;
	/* Value of currently parsed digit */
	register digitp digitParsed;

	while (length-- && (digitParsed = input_digit(*(input++))) >= 0) {
		/* Add parsed value to the resulting number value */
		number *= radix;
		number += digitParsed;
	}
	return number;
}

size_t digit2char(digit input, digit radix, bool leadingZeros, size_t length, char* output) {

	/* Prerequisites: */
	/* output has at least length allocated characters */

	for (output += length; length && (input || leadingZeros); --length, input /= radix) {
		*(--output) = output_char(input % radix);
	}

	/* Number of unused output characters, i.e. number of omitted leading zeros. */
	return length;
}
