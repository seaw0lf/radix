#include <stdlib.h>
#include "output.h"
#include "stringutils.h"
#include "../logger.h"
#include "../precognito.h"
#include "../math/numberutils.h"

static const char toString[RADIX_MAX] = {
/* 00-0F */	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
/* 10-1F */	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
/* 20-23 */	'w', 'x', 'y', 'z',
};

static const char OUTPUT_BEGIN = '[';
static const char OUTPUT_END = ']';

static digit radix;
static digit exponent;

/* Buffer */
static char* buffer;
static char* buffTerminator;
static char* buffPtr;

/* Character count of digits */
static index count;

#define returnOnIOError(X) if ((X) == -1) { return -1; }
#define returnOnAllocError(X) if ((X) == NULL) { return -1; }


/*** Helpers ***/

inline char output_char(const digit number) {
	return toString[number];
}

inline static bool leadingZeros() {
	/* Leading zeros are required if there is at least one character after OUTPUT_BEGIN character. */
	return count > INDEX_ONE;
}

inline static ssize_t flush() {

	// Write the buffer to output.
	ssize_t written = output_write(buffer, buffPtr - buffer);
	returnOnIOError(written);

	// Reset buffer pointers and counters.
	buffPtr = buffer;

	return written;
}

inline static ssize_t buffered(const ssize_t amount) {

	buffPtr += amount;
	count += amount;

	return amount;
}

inline static ssize_t writeChar(const char c) {

	if (buffPtr == buffTerminator) {
		returnOnIOError(flush());
	}

	*buffPtr = c;
	return buffered(1);
}

inline ssize_t output_digit_(const digit input, const digit radix, const bool leadingZeros, size_t length) {

	/* Recompute the length if leading zeros are omitted. */
	if (!leadingZeros) {
		length = precognito_digits(radix, input);
	}

	/* If buffer is to small to contain length characters, flush is required to empty buffer. */
	if (buffPtr + length > buffTerminator) {
		returnOnIOError(flush());
	}

	digit2char(input, radix, leadingZeros, length, buffPtr);
	return buffered(length);
}


/*** Output management ***/

/* Begins the output sequence
 *
 * Input:
 *   targetRadix - radix in which the digits should be
 *   bufferSize - flush write operation threshold; bufferSize > 0
 * Output:
 *   destination - number of written characters; negative in case of error
 *
 * The flush threshold is computed as bufferSize * DIGIT_BITS.
 *
 * Observation: The minimum reasonable static allocated buffer would be DIGIT_BITS
 *   since this is the number of characters required to write the greatest number
 *   that can fit into digit type expressed in smallest radix.
 */
ssize_t output_init(const digit targetRadix, const index targetExponent, const index bufferSize) {

	/* Remember important information */
	radix = targetRadix;
	exponent = targetExponent;

	index buffAllocated = bufferSize * DIGIT_BITS;

	/* Initialize buffer */
	count = INDEX_ZERO;
	buffPtr = buffer = malloc(buffAllocated * sizeof(char));
	returnOnAllocError(buffer);

	buffTerminator = buffer + buffAllocated;

	/* Write opening bracket */
	return writeChar(OUTPUT_BEGIN);
}

/* Writes an digit to the output */
inline ssize_t output_digit(const digit input) {

	/* Write digit to the output in specific radix */
	return output_digit_(input, radix, leadingZeros(), exponent);
}

/* Writes zeros to the output. */
inline ssize_t output_zero(const digit zeroCount) {

	/* If zeroes are ignored there's nothing to do */
	if (!leadingZeros()) {
		return 0;
	}

	/* Flush output to clean the buffer. */
	returnOnIOError(flush());

	/* Use buffer to write zeros. */
	/* 1. If zeroCount >= buffAllocated write zeros, flush, loop 1 */
	/* 2. If zeroCount < buffAllocated only write zeros to buffer and exit */

	/* Fill buffer with zeroCount zero character (but do not overflow). */
	char zero = output_char(0);
	digit zeros = zeroCount;

	while (zeros && buffPtr < buffTerminator) {
		*(buffPtr++) = zero;
		zeros--;
	}
	buffered(buffPtr - buffer);

	/* If the number of zeros exceeds buffer size, the flush has to be executed ~ zeroCount/buffAllocited times */
	while (zeros) {

		returnOnIOError(flush());

		zeros -= buffered(MIN(zeros, buffTerminator - buffer));
	}

	return zeroCount;
}

#define freeOnIOError(X) if ((X) == -1) { free(buffer); return -1; }

/* Ends the output sequence */
ssize_t output_free() {

	ssize_t writtenTotal = 0;
	ssize_t written;

	/* Write closing bracket */
	freeOnIOError(written = writeChar(OUTPUT_END));
	writtenTotal += written;

	/* Write radix in decimal base to buffer */
	freeOnIOError(written = output_digit_(radix, 10, false, 2));
	writtenTotal += written;

	freeOnIOError(flush());

	return writtenTotal;
}
