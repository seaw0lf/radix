#include <inttypes.h>
#include <stdlib.h>
#include <unistd.h>
#include "input.h"
#include "output.h"
#include "stringutils.h"
#include "../testutils.h"
#include "../logger.h"
#include "../struc/deque.h"

/* Each character that represents value of a digit has positive number.
 * All other characters have special negative value.
 */

#define INPUT_FILE "test/ntest_100_000.txt" // "test/ntest_32_.txt"

#define INPUT_VALID(C) ((C) >= 0)

// TODO prekodovat... citame uz od zaciatku
#define EON -1 /* End Of Number - the "[" character */
#define ESR -2 /* End of Source Radix - the "]" character */
#define ETR -3 /* End of Target Radix - the "=" character */
#define EOT -4 /* End Of Transmission - the end of input */
#define WHS -5 /* WHiteSpace */
#define XXX -6 /* Illegal */

static const digitp valueOf[] = {
/* 00-0F */	XXX,XXX,XXX,XXX,EOT,XXX,XXX,XXX,XXX,WHS,WHS,WHS,WHS,WHS,XXX,XXX,
/* 10-1F */	XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,
/* 20-2F */	WHS,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,
/* 30-3F */	  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,XXX,XXX,XXX,ETR,XXX,XXX,
/* 40-4F */	XXX, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
/* 50-5F */	 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,EON,XXX,ESR,XXX,XXX,
/* 60-6F */	XXX, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
/* 70-7F */	 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,XXX,XXX,XXX,XXX,XXX,
/* 80-8F */	XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,
/* 90-9F */	XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,
/* A0-AF */	XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,
/* B0-BF */	XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,
/* C0-CF */	XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,
/* D0-DF */	XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,
/* E0-EF */	XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,
/* F0-FF */	XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,XXX,WHS,
};


/* Pointer to the top of the resizing array  */
static Deque* input = NULL;
static size_t bufferSize;


/*** Input -> Internal input holder (stack) ***/

/* Initialize input */
void input_init(size_t bufSize) {
	/* Size of datachunk in deque */
	bufferSize = bufSize;
	/* Init deque with specific container and datachunk size*/
	input = allocDeque(4, bufferSize);
}

/* Reads standard input into input stack (so the end of input will be on the top of the stack) */
void input_read() {

#ifdef INPUT_FILE
	FILE* file;
	if ((file = fopen(INPUT_FILE, "r")) == NULL) {
		infoln("Cannot open file");
		exit(EXIT_FAILURE);
	}
#endif

	char* buf;
	ssize_t size;

	do {

		/* Allocate buffer */
		buf = malloc(bufferSize * sizeof(char));

		/* If error... */
		if (buf == NULL) {
			/* ... indicate error... */
			size = -1;
			/* ... and stop reading. */
			break;
		}

		/* Read data. */
#ifdef INPUT_FILE
		size = fread(buf, sizeof(char), bufferSize, file);
#else
		size = read(STDIN_FILENO, buf, bufferSize);
#endif

		/* If nothing read or error... */
		if (size <= 0) {
			/* ... free the allocated buffer... */
			free(buf);
			/* ... and stop reading. */
			break;
		}

		/* Push data to deque. */
		pushString(input, buf, size);

		/* Continue only if there is more to be read (when buffer was filled entirely). */
	} while (size == bufferSize);

	/* If error... */
	if (size < 0) {
		/* ... free internal input structure. */
		input_free();
	}

	debugfln(" $ Imported %"PRIuFAST64" characters (%s)...", input_unprocessed(), sizeofchars(0, input_unprocessed()));

#ifdef INPUT_FILE
	fclose(file);
#endif
}

/* Free input */
void input_free() {
	/* Free deque */
	freeDeque(input);
}


/*** Helpers ***/

/* Number value of a digit represented by character; Negative value has special meaning (see valueOf table) */
inline digitp input_digit(char digit) {
	return valueOf[(digitp) digit];
}

void input_flush(digit base) {
	// TODO toto tu bude takto? neda sa nejak inteligentnejsie spravit? napr. cez output?

	if (input->ixFront + 1 == input->ixBack) {
		/* The one array of input chars. */
		output_write(input->containter[input->ixFront] + input->ixFrontLocal, input->ixBackLocal - input->ixFrontLocal);
	} else {
		/* The first array of input chars. */
		output_write(input->containter[input->ixFront] + input->ixFrontLocal, bufferSize - input->ixFrontLocal);
		/* Write all but the last array of input chars. */
		for (index i = input->ixFront + 1; i < input->ixBack - 1; ++i) {
			output_write(input->containter[i], bufferSize);
		}
		/* The last array of input chars. */
		output_write(input->containter[input->ixBack - 1], input->ixBackLocal);
	}
	/* The base. */
	char b[2];
	output_write(b, 2 - digit2char(base, 10, false, 2, b));
}

/* Number of unprocessed characters */
index input_unprocessed() {
	/* Number of unread chars in container chunks */
	return input->charCount;
}


/*** Parsers ***/

/* Parse current character in given radix */
inline digitp input_parseChar(digit radix, char (*get)(Deque*), char (*pop)(Deque*)) {

	/* Parse character */
	digitp value = input_digit(get(input));
	/* If it has meaningful value, move on to the next character (otherwise consider to be illegal value) */
	if (INPUT_VALID(value)) {
		if (value < radix) {
			pop(input);
		} else {
			value = XXX;
		}
	}
	/* Return parsed value */
	return value;
}

/* Check current character against expected special character */
inline bool input_isChar(char expected, char (*get)(Deque*), char (*pop)(Deque*)) {

	/* If expected special character doesn't match parsed special character (therefore radix = 0) we are done */
	if (get(input) != expected)
		return false;

	/* Move on to the next character */
	pop(input);
	/* Parsed character is the expected character */
	return true;
}

/* Parse number of limited length using given radix exponential from back */
inline digit input_parseNumberBack(digit* radixExp, index length) {

	// TODO toto je trosku grcne... da sa s tym nieco spravit?
	digit radix = *(radixExp + 1);

	/* Value of currently parsed number */
	digit number = DIGIT_ZERO;

	/* Char is processed only when
	 * - it represents a meaningful value in given radix
	 * - it is within requested length
	 */
	digitp char2number;
	while (length-- && INPUT_VALID(char2number = parseCharBack(radix))) {
		/* Add parsed value to the resulting number value */
		/* Raise the exponent by one for next iteration */
		number += char2number * *(radixExp++);
	}
	return number;
}

/* Parse number of limited length using given radix exponential from front */
inline digit input_parseNumberFront(digit* radixExp, index length) {

	// TODO toto je trosku grcne... da sa s tym nieco spravit? da sa pouzit char2digit zo stringutils?
	digit radix = *(radixExp + 1);
	radixExp += length;

	/* Value of currently parsed number */
	digit number = DIGIT_ZERO;

	/* Char is processed only when
	 * - it represents a meaningful value in given radix
	 * - it is within requested length
	 */
	digitp char2number;
	while (length-- && INPUT_VALID(char2number = parseCharFront(radix))) {
		/* Add parsed value to the resulting number value */
		/* Raise the exponent by one for next iteration */
		number += char2number * *(--radixExp);
	}
	return number;
}
