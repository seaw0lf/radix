#ifndef OUTPUT_H_
#define OUTPUT_H_

#include <unistd.h>
#include <stdbool.h>
#include "../math/number.h"

#define RADIX_MIN 2
#define RADIX_MAX 36

#define output_write(BUF, LEN) write(STDOUT_FILENO, (BUF), (LEN));

/* Begins the output sequence */
ssize_t output_init(digit radix, index exponent, index bufferSize);
/* Writes a digit in specific radix of given length to the output */
ssize_t output_digit_(digit input, digit radix, bool leadingZeros, size_t length);
/* Writes a digit to the output */
ssize_t output_digit(digit input);
/* Writes a zero to the output */
ssize_t output_zero(digit count);
/* Ends the output sequence */
ssize_t output_free();

char output_char(digit number);

#endif /* OUTPUT_H_ */
