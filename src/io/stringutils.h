#ifndef STRINGUTILS_H_
#define STRINGUTILS_H_

#include <stddef.h>
#include <stdbool.h>
#include "../math/number.h"

digit char2digit(char* input, digit radix, size_t length);
size_t digit2char(digit input, digit radix, bool leadingZeros, size_t length, char* output);

#endif /* STRINGUTILS_H_ */
