#ifndef INPUT_H_
#define INPUT_H_

#include <stddef.h>
#include <stdbool.h>
#include "../struc/deque.h"
#include "../math/number.h"

#define ASCII_EOT 0x04
#define ASCII_WHS 0x0A

/* Initialize internal structure */
void input_init(size_t bufferSize);
/* Load input (from stdin) into internal structure */
void input_read();
/* Free internal structure */
void input_free();

/* Parse actual character in given radix */
#define parseCharFront(X) input_parseChar(X, (void*) &front, (void*) &popFront)
/* Check actual character against expected character */
#define isCharFront(C) input_isChar(C, (void*) &front, (void*) &popFront)
/* Parse actual character in given radix */
#define parseCharBack(X) input_parseChar(X, (void*) &back, (void*) &popBack)
/* Check actual character against expected character */
#define isCharBack(C) input_isChar(C, (void*) &back, (void*) &popBack)
/* Parse number of limited length in given radix */
digit input_parseNumberBack(digit* radix, index length);
digit input_parseNumberFront(digit* radix, index length);

digitp input_parseChar(digit radix, char (*get)(Deque*), char (*pop)(Deque*));
bool input_isChar(char expected, char (*get)(Deque*), char (*pop)(Deque*));

index input_unprocessed();
void input_flush(digit base);

digitp input_digit(char digit);

#endif /* INPUT_H_ */
