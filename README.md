# RAdiX

## Purpose

A Free-time Project

* Learn ANSI C basics
* Get used to GIT
* Think about algorithmical problems
* Dive deeper into classic computer science algorithms

## Problem Statement

Convert given number from source to target base.

### Example

#### Input File
```text
[11100101011111110]2=16
```

###### Notes
- detect and report if the input file does not comply with the format `[{number_in_source_base}]{source_base_in_base_10}={target_base_in_base_10}`
- focus on scenarios where source and target base are prime numbers

#### Output File
```text
1cafe
```

### Constraints
- ANSI C
- no external libraries allowed
- max 8 GB RAM allowed
- single thread allowed
